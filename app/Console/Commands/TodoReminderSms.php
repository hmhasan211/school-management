<?php

namespace App\Console\Commands;

use App\Models\Backend\Attendance;
use App\Models\Backend\Todo;
use Carbon\Carbon;
use Illuminate\Console\Command;

class TodoReminderSms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CronJob:TodoReminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily Sms from Todo list';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $todos = Todo::query()->with('staff:id,name,mobile')->where('date',date('Y-m-d'))
            ->whereNotNull('staff_id')
            ->where('notify',1)
            ->where('status',1)
            ->get();

        foreach ($todos as $todo){
            $msg = 'Dear '.$todo->staff->name.',</br> your today task is '.$todo->title .'</br> Thank You!!';
            $this->send($todo,$msg);
        }
        $this->info('Sms Send successfully!!');
//        return 0;
    }

    public function send($student,$msg)
    {
        $api_key = smsConfig('api_key');
        $contacts = $student->mobile;
        $senderid = smsConfig('sender_id');
        $sms = $msg;
        $URL = "http://masking.mdlsms.com/smsapi?api_key=".urlencode($api_key)."&type=text&contacts=".$contacts."&senderid=".urlencode($senderid)."&msg=".urlencode($sms);

        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_HTTPHEADER, ['Content-Type: text/html; charset=UTF-8']);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ($ch, CURLOPT_URL, $URL);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
    }
}
