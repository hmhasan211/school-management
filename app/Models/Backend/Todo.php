<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'description', 'start_date','end_date', 'time', 'venue', 'staff_id', 'notify', 'status'];

    public function staff()
    {
        return $this->belongsTo(Staff::class);
    }
}
