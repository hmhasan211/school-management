<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    protected $fillable = ['name','description'];
}
