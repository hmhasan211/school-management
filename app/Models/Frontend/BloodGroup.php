<?php

namespace App\Models\Frontend;

use Illuminate\Database\Eloquent\Model;

class BloodGroup extends Model
{
    protected $fillable = ['name'];
}
