<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Backend\AcademicClass;
use App\Models\Backend\Attendance;
use App\Models\Backend\Staff;
use App\Models\Backend\Subject;
use App\Models\Diary;
use Illuminate\Http\Request;

class DiaryController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $date = $request->get('date');
        $ac = $request->get('academic_class_id');
        $query = Diary::query()->with(['academicClass:id,session_id,class_id,section_id,section_id','academicClass.classes:id,name','academicClass.section:id,name','academicClass.group:id,name']);
//        $query = Diary::query();
        $academicClass = AcademicClass::active()->get();

        if($date && $ac){
          $data =   $query->where('date', $date)->where('academic_class_id',$ac )->get()->groupBy('academic_class_id');
        }elseif ($date){
          $data =   $query->where('date', $date)->get()->groupBy('academic_class_id');
        } elseif($ac){
           $data =  $query->where('academic_class_id', $ac)->get()->groupBy('academic_class_id');
        }else{
          $data =   $query->where('date', date('Y-m-d'))->get()->groupBy('academic_class_id');
        }

        if ($query){
            $diaries =  $data;
        }else{
            $diaries = null;
        }
//        return $diaries;
        return view('admin.diary.index', compact('academicClass','diaries'));
    }

    public function customPrint(Request $request,$ac = null,$date = null)
    {
        $customPrint = $request->print_item ;
        $today = date('Y-m-d');
        $todayAttn = Attendance::query()
//           ->whereDate('date',$today)
          ->whereDate('date', '2022-11-01')
          ->whereIn('attendance_status_id', [1, 3]) //1 for present , 3 for late
          ->whereHas('studentAcademic', function ($q) use ($ac) {
              $q->where('academic_class_id', $ac);
          })
          ->count();

        $prints = $customPrint ??  $todayAttn;

        //query for getting diary
        $query = Diary::query()->with(['academicClass', 'subject:id,name','teacher:id,name']);
          $diary =   $query->where('academic_class_id',$ac)->whereDate('date', $date)->get();

        return view('admin.diary.print-diary',compact('prints','diary'));
    }

    public function destroy($id){
        $diary = Diary::find($id)->delete();
      return back();
    }

    public function create()
    {
        $academicClass = AcademicClass::active()->get(); // active() means is show all active sessions
        $subjects = Subject::all();
        $teachers = Staff::where('staff_type_id', 2)->get();
        return view('admin.diary.create', compact('academicClass','subjects','teachers'));
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'academic_class_id' => 'required',
            'date' => 'required',
            'teacher_id' => 'required',
            'subject_id' => 'required',
            'description' => 'required',
        ]);

        Diary::create($validated);
        return back()->with('status', 'Your Diary Create Successfully');
    }
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'academic_class_id' => 'required',
            'date' => 'required',
            'teacher_id' => 'required',
            'subject_id' => 'required',
            'description' => 'required',
        ]);

        $diary = Diary::find($id);
        $diary->update($validated);

        return redirect()->route('diary.index')->with('status', 'Your Diary Create Successfully');
    }

    public function edit($id)
    {
        $diary = Diary::find($id);
        return redirect()->route('diary.create')->with(['data' => $diary]);
    }
}
