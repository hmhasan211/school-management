<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Backend\School;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SchoolController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $schools = School::query()->latest()->get();
        return view('admin.admissionReg.schoolList', compact('schools'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:schools',
            'phone' => 'numeric',
            'email' => 'email',
        ], []);

        School::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'p_name' => $request->p_name,
        ]);
        $request->session()->flash('success', 'School Created successfully!');
        return redirect()->back();
    }

    public function edit(Request $request)
    {
        $data = School::query()->findOrFail($request->id);
        return $data;
    }

    public function jsonData(Request $request)
    {
        $data = School::query()->findOrFail($request->id);
        return $data;
    }

    public function sendSms(Request $request)
    {
        $api_key = smsConfig('api_key');
        $mobile =  $request->number;
        $senderid = smsConfig('sender_id');
        $sms = $request->get('message');
        $URL = "http://bangladeshsms.com/smsapi?api_key=".urlencode($api_key)."&type=text&contacts=".urlencode($mobile)."&senderid=".urlencode($senderid)."&msg=".urlencode($sms);

        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_HTTPHEADER, ['Content-Type: text/html; charset=UTF-8']);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ($ch, CURLOPT_URL, $URL);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);

        $data['type'] = 'School';
        $data['user_id'] = auth()->id();
        $data['destination_count'] = 1;
        $data['sms_count'] = Str::of($sms)->wordCount() ?? 0;
        $data['numbers'] = $mobile;
        $data['message'] = $sms;
        $data['status'] = '1';
        CommunicationHistory::query()->create($data);
        //dd($output);

        Session::flash('success','SMS sent successfully!');

        return redirect()->back();
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'phone' => 'numeric',
            'email' => 'email',
            'p_name' => $request->p_name,
        ], []);

        $editData = School::query()->findOrFail($request->id);
        $editData->update($request->all());
        $request->session()->flash('success', 'School Updated successfully!');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $class = School::query()->findOrFail($id);
        $class->delete();
        Session::flash('success', 'School Deleted successfully');
        return redirect()->back();
    }
}
