<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Backend\AcademicClass;
use App\Models\Backend\Group;
use App\Models\Backend\Religion;
use App\Models\Backend\School;
use App\Models\Backend\Section;
use App\Models\Backend\Student;
use App\Models\Backend\StudentAcademic;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class StudentReportController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function studentCustomReport(Request $request)
    {
        $field =   $request->field;
        $reqGroup = $request->group_id;
        $reqReligion = $request->religion_id;
        $reqSection = $request->section_id;
        $reqAge = $request->age;
        $reqTribal = $request->is_tribal;
        $reqfighter = $request->is_freedomfighter;
        $reqDisablity = $request->disability;
        $reqField = $request->field;
        $field[] ="id";

        $religions = Religion::query()->get(['id', 'name']);
        $groups = Group::query()->get(['id', 'name']);
        $section = Section::query()->get(['id', 'name']);
        $t_columns = DB::getSchemaBuilder()->getColumnListing("students");
        $age = $request->input('age');
        $startDate = Carbon::today()->subYears($age)->format('Y-m-d');
        $endDate = date('Y-m-d');


          $query = StudentAcademic::query()->with(['student:id,name,studentId,mobile,address,area,email,religion_id',
              'student.father','student.mother',
           'group:id,name','section:id,name','student.religion:id,name'
          ]) ->select(['student_id', 'group_id','section_id']);

        if( $reqGroup) {
            $query ->where('group_id', $request->group_id);
        }

        if( $reqSection) {
            $query->where('section_id',$reqSection);
        }

        if( $reqReligion) {
            $query ->whereHas('student',function ($q) use($reqReligion){
                    $q->where('religion_id',$reqReligion);
                });
        }

        if( $reqAge) {
            $query ->whereHas('student',function ($q) use($endDate,$startDate){
                    $q->whereBetween('dob',[$startDate ,$endDate]);
                });
        }

        if( $reqTribal) {
            $query ->whereHas('student',function ($q) {
                    $q->where('is_tribal',1);
                });
        }

        if( $reqfighter) {
            $query->whereHas('student',function ($q) {
                    $q->where('freedom_fighter',1);
                });
        }

        if( $reqDisablity) {
            $query->whereHas('student',function ($q) {
                    $q->where('disability',1);
                });
        }

//         $students =  $query->get();


        if ($reqGroup ||$reqReligion  || $reqSection || $reqAge ||$reqTribal || $reqfighter || $reqDisablity ){
            $students = $query->get();
        }else{
            $students = null;
        }

        return view('admin.admissionReg.custom_report', compact('religions','reqField','groups','section', 't_columns', 'students'));


//return $students->count()  ;


//               ->get(['student_id','group_id','section_id']);

//        $query = StudentAcademic::query()->with(['student: id, name,studentId, mobile,address,area,email','student.father:id,f_name','student.mother:id,m_name',
//            'student'=> function($q) use($field) {
//                $q->addSelect($field);
//            },
////            'student.father'=> function($q) use($field) {
////                $q->addSelect($field);
////            },'student.mother'=> function($q) use($field) {
////                $q->addSelect($field);
////            },
//            'group:id,name','section:id,name','student.religion:id,name']);


//        return $field;

//        if ($reqReligion && $reqGroup && $reqSection && $age  ) {
//            return '4';
//            $students   = $query
////                ->with([
////                'student'=> function($q) use($field) {
////                $q->addSelect($field);
////            },'student.father'=> function($q) use($field) {
////                    $q->addSelect($field);
////                },'student.mother'=> function($q) use($field) {
////                    $q->addSelect($field);
////                },
////            'group:id,name','section:id,name','student.religion:id,name'])
//                ->select(['student_id', 'group_id','section_id'])
//                ->where('group_id', $reqGroup)
//                ->where('section_id', $reqSection)
//                ->whereHas('student',function ($q) use($reqReligion,$endDate,$startDate){
//                    $q->where('religion_id',$reqReligion)
//                        ->whereYear('dob', '<= ',$startDate)->whereYear('dob', '>= ',$endDate);
//                })->get();
//        }
//        elseif ($reqReligion && $reqGroup && $reqSection){
//            return '3';
//            $students   = $query
////                ->with(['student'=> function($q) use($field) {
////                $q->addSelect($field);
////            },'group:id,name','section:id,name','student.religion:id,name'])
//                ->select(['student_id', 'group_id','section_id'])
//                ->where('group_id', $reqGroup)
//                ->where('section_id', $reqSection)
//                ->whereHas('student',function ($q) use($reqReligion){
//                    $q->where('religion_id',$reqReligion);
//                })->get();
//        }elseif( $reqReligion) {
//            return 'rel';
//            $students   = $query
////                ->with(['student'=> function($q) use($field) {
////                $q->addSelect($field);
////            },'student.religion:id,name'])
//                ->select('student_id')
//                ->whereHas('student',function ($q) use($request,$field){
//                    $q->where('religion_id',$request->religion_id);
//                })->get();
//        }elseif( $reqGroup) {
////            return 'grp';
//            $students   = $query
////                ->with(['student'=> function($q) use($field) {
////                $q->addSelect($field);
////            },'group:id,name'])
//                ->where('group_id', $request->group_id)
//                ->select(['student_id', 'group_id'])->get();
//        }elseif( $reqSection) {
//            return 'sec';
//            $students   = $query
////                ->with(['student'=> function($q) use($field) {
////                $q->addSelect($field);
////            },'section:id,name'])
//                ->where('section_id',$reqSection)
//                ->select(['student_id', 'section_id'])->get();
//        } elseif( $reqAge) {
//            return 'age';
//            $students   = $query
////                ->with(['student'=> function($q) use($field) {
////                $q->addSelect($field);
////            }])
//                ->select('student_id')
//                ->whereHas('student',function ($q) use($endDate,$startDate){
//                         $q->whereBetween('dob',[$startDate ,$endDate]);
//                })->get();
//        } elseif( $reqTribal) {
//            return 'tribal';
//            $students   = $query
////                ->with(['student'=> function($q) use($field) {
////                $q->addSelect($field);
////            }])
//                ->select('student_id')
//                ->whereHas('student',function ($q) {
//                         $q->where('is_tribal',1);
//                })->get();
//        }
//        elseif( $reqfighter) {
//            return 'f';
//            $students   = $query
////                ->with(['student'=> function($q) use($field) {
////                $q->addSelect($field);
////            }])
//                ->select('student_id')
//                ->whereHas('student',function ($q) {
//                         $q->where('freedom_fighter',1);
//                })->get();
//        } elseif( $reqDisablity) {
//            return 'dis';
//            $students   = $query
////                ->with(['student'=> function($q) use($field) {
////                $q->addSelect($field);
////            }])
//                ->select('student_id')
//                ->whereHas('student',function ($q) {
//                         $q->where('disability',1);
//                })->get();
//        }else {
//            $students = null;
//        }
//        return $students;

    }

    public function tokenGenerate(Request $request)
    {
        $reqAc = $request->ac_class_id;
        $classes = AcademicClass::query()->get();
        $query = StudentAcademic::query();

        if ($reqAc) {
            $students = $query->with('student:id,name,studentId,mobile,address')
                ->where('academic_class_id',$reqAc)
                ->select(['student_id','academic_class_id'])
                ->get();
        } else {
            $students = null;
        }
//return $students;
        return view('admin.admissionReg.token', compact('classes', 'students'));
    }

    public function customTable(Request $request)
    {
        $classes = AcademicClass::query()->get();

        $columns = $request->column;
        $arrayCol = explode(',', $columns);

        if ($request->ac_class_id) {
            $students = StudentAcademic::query()
                         ->with('student:id,name')->where('academic_class_id', $request->ac_class_id)
                         ->get();
        } else {
            $students = null;
        }
//        return $students;
        return view('admin.admissionReg.custom-table', compact('students','classes', 'arrayCol' ));
    }
}
