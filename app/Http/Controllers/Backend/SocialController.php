<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Backend\Social;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SocialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

        public function index()
        {
            $socials = Social::query()->get();
            return view('admin.settings.socials',compact('socials'));
        }

        public function status($id){
            $todo = Social::query()->findOrFail($id);
            if ($todo->status == 1){
                $todo->status = 0;
            }else{
                $todo->status = 1;
            }
            $todo->save();
          Session::flash('success','Status has been updated successfully!');
            return back();
        }

        public function store(Request $request)
        {
            $this->validate($request, [
                'provider' => 'required|string|unique:socials',
                'address' => 'required',
            ], []);

            Social::create([
                'provider' => $request->provider,
                'address' => $request->address,
                'icon' => $request->icon,
            ]);
            $request->session()->flash('success', 'Data Created successfully!');
            return redirect()->back();
        }

    public function edit(Request $request)
    {
        $data = Social::query()->findOrFail($request->id);
        return $data;
    }


    public function update(Request $request)
    {
        $this->validate($request, [
            'provider' => 'required|string|unique:socials,provider,'.$request->id,
            'address' => 'required',
        ], []);

        $editData = Social::query()->findOrFail($request->id);
        $editData->update($request->all());
        $request->session()->flash('success', 'Data Updated successfully!');
        return redirect()->back();
    }

    public function destroy($id)
    {
        $class = Social::query()->findOrFail($id);
        $class->delete();
        Session::flash('success', 'Data Deleted successfully');
        return redirect()->back();
    }
}
