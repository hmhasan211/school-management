<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    public function setPrice()
    {
        return view('set_price');
    }

    public function getPrice(Request $request){
//        return $request->all();
        $date =  $request->date;
        $mobile =  $request->mobile;
        $greeting =  $request->greeting;
        $b_price =  $request->basic_price;
        $st_price =  $request->standard_price;
        $pr_price =  $request->premium_price;
        return view('pricing',compact('date','mobile','greeting'));
    }
}
