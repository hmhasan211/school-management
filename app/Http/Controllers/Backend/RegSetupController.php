<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Backend\AcademicClass;
use App\Models\Backend\StudentAcademic;
use Illuminate\Http\Request;

class RegSetupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request  $req){
        $classes = AcademicClass::query()->get();
//        return $req->all();
        $ac = $req->ac_class_id;
        $exam = $req->exam_name;

        $students = StudentAcademic::query()->with('student')

            ->where('academic_class_id',$ac)
//            ->count();
                ->latest()
            ->get(['id','student_id','school_id','hsc','ssc','jsc','pec']);

        return view('admin.admissionReg.reg-number-setup',compact('classes','students'));
    }


    public function updateData(Request  $request){

        $recordId = $request->id;

        if ($request->hsc) {
            $reqHscRoll = $request->hsc["roll"];
            $reqHscReg = $request->hsc["reg"];
            $reqHscGpa = $request->hsc["gpa"];
        }
        if ($request->ssc){
            $reqSscRoll = $request->ssc["roll"];
            $reqSscReg = $request->ssc["reg"];
            $reqSscGpa = $request->ssc["gpa"];
        }
        if ($request->jsc) {
            $reqJscRoll = $request->jsc["roll"];
            $reqJscReg = $request->jsc["reg"];
            $reqJscGpa = $request->jsc["gpa"];
        }
        if ($request->pec) {
            $reqPecRoll = $request->pec["roll"];
            $reqPecReg = $request->pec["reg"];
            $reqPecGpa = $request->pec["gpa"];
        }
        foreach ($recordId as $key=>$r){

            $editData =  StudentAcademic::query()->whereId($r)->first();

            //decode json data
            $hscInfo = json_decode($editData->hsc);
            $sscInfo = json_decode($editData->ssc);
            $jscInfo = json_decode($editData->jsc);
            $pecInfo = json_decode($editData->pec);
            //end decode json data

            $jsonDataHsc = [
                "gpa"=>  $reqHscGpa[$key] ?? "" ,
                "reg" => $reqHscReg[$key] ?? "",
                "roll" => $reqHscRoll[$key] ?? '',
                "inst" => $hscInfo->inst ?? "",
                "year" => $hscInfo->year ?? "",
                "group" => $hscInfo->group ?? "",
                "board" => $hscInfo->board ?? "",
            ];

            $jsonDataSsc = [
                "gpa"=>  $reqSscGpa[$key] ?? "" ,
                "reg" => $reqSscReg[$key] ?? "",
                "roll" => $reqSscRoll[$key] ?? '',
                "inst" => $sscInfo->inst ?? "",
                "year" => $sscInfo->year ?? "",
                "group" => $sscInfo->group ?? "",
                "board" => $sscInfo->board ?? "",
            ];
            $jsonDataJsc = [
                "gpa"=>  $reqJscGpa[$key] ?? "" ,
                "reg" => $reqJscReg[$key] ?? "",
                "roll" => $reqJscRoll[$key] ?? '',
                "inst" => $jscInfo->inst ?? "",
                "year" => $jscInfo->year ?? "",
                "group" => $jscInfo->group ?? "",
                "board" => $jscInfo->board ?? "",
            ];

            $jsonDataPec = [
                "gpa"=>  $reqPecGpa[$key] ?? "" ,
                "reg" => $reqPecReg[$key] ?? "",
                "roll" => $reqPecRoll[$key] ?? '',
                "inst" => $pecInfo->inst ?? "",
                "year" => $pecInfo->year ?? "",
                "group" => $pecInfo->group ?? "",
                "board" => $pecInfo->board ?? "",
            ];


            if ($request->hsc) {
                $editData->update(["hsc" => json_encode($jsonDataHsc)]);
            }
            if ($request->ssc) {
                $editData->update(["ssc" => json_encode($jsonDataSsc)]);
            }
            if ($request->jsc) {
                $editData->update(["jsc" => json_encode($jsonDataJsc)]);
            }
            if ($request->pec) {
                $editData->update(["pec" => json_encode($jsonDataPec)]);
            }

        }
        return back();
    }

}
