<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Backend\Staff;
use App\Models\Backend\Todo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class TodoController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function exportCSV($from = null,$to = null)
    {
        $fileName = 'tasks.csv';

//        $tasks = Todo::all();

        $qry = Todo::query();

        if ($from) {
            $qry->whereDate('start_date','>=',$from);
        }
        if ($to) {
            $qry->whereDate('end_date','<=',$to);
        }

        $tasks =  $qry->orderBy('id', 'DESC')->get();

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('Title', 'Assign', 'Description', 'From','To', 'Time','Venue','Status','Notify');

        $callback = function() use($tasks, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($tasks as $task) {
                $row['Title']  = $task->title;
                $row['Assign']    = $task->staff->name ?? '';
                $row['Description']    = $task->description ?? '';
                $row['From']  = $task->start_date ?? '';
                $row['To']  = $task->end_date ?? '';
                $row['Time']  = $task->time ?? '';
                $row['Venue']  = $task->venue ?? '';
                $row['Status']  = $task->status ? 'yes':'no';
                $row['Notify']  = $task->notification ? 'yes':'no';

                fputcsv($file, array($row['Title'], $row['Assign'], $row['Description'],  $row['From'], $row['To'],  $row['Time'], $row['Venue'],  $row['Status'],$row['Notify']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    public function details($id){
        $data = Todo::query()->findOrFail($id);
        return view('admin.institution.todo-details',compact('data'));
    }

    public function search(){

        $search = request('search');
        $from = request('from');
        $to = request('to');


        $qry = Todo::query();

        if ($search) {
            $qry->where('title', 'Like', '%' . $search . '%')
            ->orWhere('description', 'Like', '%' . $search . '%');
        }
        if ($from){
            $qry->whereDate('start_date','>=',$from);
        }
        if ($to){
            $qry->whereDate('end_date','<=',$to);
        }

        $todos =  $qry->orderBy('id', 'DESC')->paginate(20);
        $staff = Staff::query()->get(['id','name','mobile']);
        return view('admin.institution.todo-list', compact('todos','staff'));
    }

    public function index()
    {
        $staff = Staff::query()->get(['id','name','mobile']);
        $todos = Todo::query()->latest()->paginate(15);
        return view('admin.institution.todo-list', compact('todos','staff'));
    }


    public function send($todo,$msg)
    {
        $api_key = smsConfig('api_key');
        $contacts = $todo->staff->mobile;
        $senderid = smsConfig('sender_id');
        $sms = $msg;
        $URL = "http://masking.mdlsms.com/smsapi?api_key=".urlencode($api_key)."&type=text&contacts=".$contacts."&senderid=".urlencode($senderid)."&msg=".urlencode($sms);

        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_HTTPHEADER, ['Content-Type: text/html; charset=UTF-8']);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ($ch, CURLOPT_URL, $URL);
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);

    }

    public function store(Request $request)
    {
//        return $request->all();
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $holiday = Todo::query()->create($request->all());


        return redirect('todo/list')->with(['status' => 'ToDo has been created successfully!']);
    }

    public function edit($id)
    {
        $staff = Staff::query()->get(['id','name','mobile']);
        $todo = Todo::query()->findOrFail($id);
        $todos = Todo::query()->paginate(20);
        return view('admin.institution.todo-list-edit', compact('todo', 'todos','staff'));
    }

    public function update($id, Request $request)
    {
        $holiday = Todo::query()->findOrFail($id);

        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }


        $holiday->update($request->all());


        return redirect('todo/list')->with(['status' => 'Data has been updated!']);
    }

    public function status($id){
        $todo = Todo::query()->findOrFail($id);
      if ($todo->status == 1){
          $todo->status = 0;
      }else{
          $todo->status = 1;
      }
        $todo->save();
        \Illuminate\Support\Facades\Session::flash('success','Status has been updated successfully!');
      return back();
    }

    public function destroy($id)
    {
        $todo = Todo::query()->findOrFail($id);
        $todo->delete();
        Session::flash('success', $todo->title . ' is deleted successfully!');
        return redirect('todo/list')->with(['status' => 'Data has been Deleted successfully!']);
    }
}