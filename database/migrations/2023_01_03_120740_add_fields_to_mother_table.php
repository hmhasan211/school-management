<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToMotherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mothers', function (Blueprint $table) {
            $table->string('m_wrk_place')->nullable()->after('m_occupation');
            $table->string('m_image')->nullable()->after('m_email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mothers', function (Blueprint $table) {
            $table->dropColumn('m_wrk_place');
            $table->dropColumn('m_image');
        });
    }
}
