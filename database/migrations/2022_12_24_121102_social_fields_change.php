<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SocialFieldsChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('socials', function($table) {
            $table->dropColumn([ 'facebook', 'youtube', 'twitter', 'linkendin']);
            $table->string('provider')->unique()->after('id');
            $table->string('icon')->nullable()->after('provider');
            $table->string('address')->unique()->after('icon');
            $table->boolean('status')->default(1)->after('address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('socials', function($table) {
            $table->dropColumn([ 'facebook', 'youtube', 'twitter', 'linkendin']);
        });
    }
}
