<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveOrderFromPage extends Migration
{
    public function up()
    {
        Schema::table('pages', function($table) {
            $table->dropColumn('order');
        });
    }

    public function down()
    {
        Schema::table('pages', function($table) {
            $table->integer('order');
        });
    }
}
