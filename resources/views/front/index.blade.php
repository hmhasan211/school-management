@extends('layouts.front')

@section('content')

    @include('front.carousel')

    @include('front.features')

    @include('front.notice')

    @include('front.chairman')

    <hr>

    @include('front.message')

    @include('front.events')

    {{--@include('front.gallery')--}}

    @include('front.progress-bar')

    @include('front.galleryCorner')

    @include('front.news')

    @include('front.subscibe')

    @include('front.teacher')

@stop
