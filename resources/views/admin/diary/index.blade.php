@extends('layouts.fixed')

@section('title','Dairy List')
@section('style')
{{--    <style>--}}
{{--        @media print {--}}
{{--            .no_print {--}}
{{--                display: none;--}}
{{--            }--}}
{{--            collapsed{--}}
{{--                display: none;--}}
{{--            }--}}
{{--            /*.accordion-group .accordion-body.collapse {*/--}}
{{--            /*    height: auto;*/--}}
{{--            /*}*/--}}
{{--        }--}}
{{--    </style>--}}
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ __('Dairy ')}}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{ __('Home')}}</a></li>
                        <li class="breadcrumb-item active">{{ __('All Dairy List')}}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

{{--    search content--}}
    <section class="content no_print ">
        <div class="container-fluid">
            <div class="col-lg-12 col-sm-8 col-md-8 col-xs-12 ">
                <div class="card card-primary card-outline">
                    <div class="card-body" style="padding-bottom:0">
                        <form action="{{ route('diary.index') }}" method="get">
                            <div class="form-row">
                                <div class="form-group col">
                                    <label>Date</label>
                                    <input type="date" name="date"  class="form-control datePicker" >
                                </div>

                                <div class="form-group col">
                                    <label>Academic Class</label>
                                    <select name="academic_class_id" id="" class="form-control select2">
                                      <option disabled selected>{{ __('--Select Class--')}}</option>
                                      @foreach($academicClass as $ac)
                                      <option value="{{ $ac->id }}">{{ $ac->classes->name ?? '' }} {{ $ac->group->name ?? '' }}</option>
                                      @endforeach
                                  </select>
                                </div>
                                <div class="form-group col" style="margin-top: 30px">
                                    <button type="submit" class="btn btn-info"><i class="fa fa-search"></i></button>
                                    <a href="{{ route('diary.create') }}" class="btn  btn-success ml-2"><i class="fa fa-plus-circle"></i></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </section>

    <section class="content mt-4">
        <div class="container-fluid">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="text-center">
                            <h5 class="mb-4">
{{--                                    {{  $diaries[$key][0]->date->format('Y-m-d')  }}--}}
{{--                                    {{  $diaries[$key][0]->date->format('l')  }}--}}
                            </h5>
                        </div>
                        @forelse($diaries as $key=>$diary)
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header no_print" id="headingThree">
                                    <h1 class="mb-0">
                                        <button class="btn  btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapse{{ $key }}" aria-expanded="false" aria-controls="collapse{{ $key }}">
                                           <strong >
                                               {{$diary[0]->academicClass->classes->name ?? '' }}
                                               {{$diary[0]->academicClass->section->name ?? '' }}
                                               {{ $diary[0]->academicClass->group->name ?? '' }} &nbsp;
                                           </strong>
                                            <small>
                                                {{  $diaries[$key][0]->date->format('Y-m-d')  }},
                                                {{  $diaries[$key][0]->date->format('l')  }}
                                            </small>
                                            <div class="float-right no_print">

                                               Subject: <span class="badge badge-success"> {{ count($diary) }}</span> &nbsp;&nbsp;
                                            </div>
                                        </button>

                                    </h1>
                                </div>
                                <div id="collapse{{ $key }}" class="collapse  @if($loop->first) show @endif" aria-labelledby="headingThree" data-parent="#accordionExample">
                                    <div class="card-body">

                                        <table class="table table-bordered ">

                                            <thead class="no_print">
                                            <tr>
                                                <td>
                                                    <div class="btn-group">
                                                        <button type="button" class="badge badge-warning  dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fa fa-print"></i>
                                                        </button>
                                                        <div class="dropdown-menu text-center">
                                                            <a type="button" href="" title="Print" class="badge badge-info " data-toggle="modal" target="_blank" data-target="#printModal{{$key}}" data-whatever="@mdo"  >custom number</a>
                                                            <div class="dropdown-divider"></div>
                                                            <a href="{{ route('diary.custom-print',['ac'=>$key,'date'=>$diary[0]['date']]) }}" title="Print" target="_blank" class="badge badge-info" >as per attendance</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($diary as $value)
                                            <tr>
                                                    <th>{{ $value->subject->name ?? '-' }}:</th>
                                                    <td>{!!  $value->description ?? '-' !!}</td>
                                                    <td class="no_print">
                                                        <a href="{{ route('diary.edit', $value->id) }}" role="button"
                                                           class="text-primary"><i
                                                                    class="fas fa-edit"></i></a>

                                                        <form style="display: inline" action="{{ route('diary.destroy', $value->id) }}" method="post">
                                                            @csrf
                                                            @method('DELETE')
                                                            <a href="#" class="text-danger confirmation" title="Delete" data-toggle="tooltip" onclick="this.closest('form').submit();return false;">
                                                                <i class="fas fa-trash"></i>
                                                            </a>
                                                        </form>

                                                    </td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <!-- ***/ Pop Up Model for button -->
                            <div class="modal fade" id="printModal{{$key}}" tabindex="-1" role="dialog" aria-labelledby="printModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="printModalLabel">Number of Print</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                            <form action="{{ route('diary.custom-print',['ac'=>$key,'date'=>$diary[0]['date']]) }}" method="get">
                                                <div class="row">
                                                    <div class="col-8">
                                                        <input type="text" name="print_item" class="form-control">
                                                    </div>
                                                    <div class="col-4">
                                                        <button type="submit" class="btn btn-success btn-block">
                                                            Submit
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ***/ Pop Up Model for button End-->
                        @empty
                            <td>
                                <h6 class="text-center text-danger text-bold">  No Diary Found ! 😒</h6>
                            </td>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>

    </section>
@stop

@section('script')
    <script type="text/javascript">
        $('.confirmation').on('click', function () {
            return confirm('Are you sure?');
        });
    </script>
@stop
