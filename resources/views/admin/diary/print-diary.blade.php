@extends('layouts.fixed')

@section('title','Print-Diary')
@section('style')
{{--    <style>--}}
{{--        @media print {--}}
{{--            .no_print {--}}
{{--                display: none;--}}
{{--            }--}}
{{--            collapsed{--}}
{{--                display: none;--}}
{{--            }--}}
{{--        }--}}
{{--    </style>--}}
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ __('Print-Diary')}}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{ __('Home')}}</a></li>
                        <li class="breadcrumb-item active">{{ __('Print Dairy')}}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    {{--    search content--}}
    <section class="content no_print ">
        <div class="container-fluid">
            <div class="col-lg-12 col-sm-8 col-md-8 col-xs-12 ">
                <button class="btn btn-warning btn-block"  onclick="window.print(); return false;"><i
                            class="fa fa-print"></i> Print</button>
            </div>
        </div>
    </section>

    <section class="content mt-4">
        <div class="container-fluid">
                <div class="card">
                    <div class="card-body">
                       <div class="row">
                           @if($prints > 0)
                           @for( $i =1; $i<= $prints; $i++ )

                           <div class="col-6 mb-3">
                               <div class="card">
                                   <div class="card-header">
                                       <h5 class="card-title">
                                           {{$diary[0]->academicClass->classes->name ?? '' }}
                                           {{$diary[0]->academicClass->section->name ?? '' }}
                                           {{ $diary[0]->academicClass->group->name ?? '' }}
                                       </h5>
                                   </div>
                                   <div class="card-body">
                                       <table class="table table-borderless table-responsive">
                                           <tbody>
                                           @foreach($diary as $value)
                                               <tr>
                                                   <th>{{ $value->subject->name ?? '-' }}:</th>
                                                   <td>{!!  $value->description ?? '-' !!}</td>
                                               </tr>
                                           @endforeach
                                           </tbody>
                                       </table>
                                   </div>
                               </div>
                           </div>
                           @endfor
                           @else
                                <h6 class="text-center text-danger text-bold"> Check Attendance !!</h6>
                           @endif
                       </div>
                    </div>
                </div>
        </div>

    </section>
@stop

@section('script')

@stop
