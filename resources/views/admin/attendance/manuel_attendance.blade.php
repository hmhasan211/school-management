@extends('layouts.fixed')
@section('title', 'Manuel Attendance')
@section('style')

@stop

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Student Manuel Attendance') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{ __('Attendance') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('Manuel Attendance') }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- /.Search-panel -->
    <section class="content no_print ">
        <div class="container-fluid">
            {{-- start --}}
            <div class="col-lg-12 col-sm-8 col-md-8 col-xs-12 ">
                <div class="card card-primary card-outline">
                    <div class="card-body" style="padding-bottom:0">
                        <form method="get" action="{{ route('student.manuel-attendance') }}">
                            <div class="form-row" >
                                <div class="form-group col-md-2">
                                    <label>Academic Class </label>
                                    <select name="academic_class" id="" class=" form-control select2" >
                                        <option value="">Select class</option>
                                            @foreach ($academic_class as $class)
                                                <option value="{{ $class->id }}" {{ ( request()->academic_class ==  $class->id ? 'selected' : '') }}>
                                                    {{ $class->classes->name }} {{ $class->section->name ?? '' }} {{ $class->group->name ?? '' }}
                                                </option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-2">
                                    <label>Date</label>
                                    {{ Form::date('date', request()->date ?? date('Y-m-d'), ['class' => 'form-control', 'id'=>'datetimepicker']) }}
                                </div>

                                <div class="form-group col-md-3">
                                    <label>Student Id</label>
                                    {{ Form::text('s_id', request()->s_id, ['class' => 'form-control','placeholder'=>'Comma separate for multiple Id']) }}
                                </div>

                                <div class="form-group col-md-1" style="margin-top: 30px">
                                    <button type="submit" class="btn btn-info btn-md btn-block"><i
                                                class="fa fa-search"></i>&nbsp
                                    </button>
                                </div>
                                <div class="form-group col-md-1" style="margin-top: 30px">
                                    <button class="btn btn-warning btn-md btn-block"
                                            onclick="window.print(); return false;"><i
                                                class="fa fa-print"></i>&nbsp
                                    </button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.card -->
            </div>

        </div>{{-- end --}}
    </section>


    @if (isset($attendances))
        @if(count($attendances) > 0)
        <section class="content mt-4">
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="card">
                        <form id="form" method="post" action="{{ route('student.manuel-attendance-status') }}">
                            @csrf
                            <div class="card-body">
                                <div class="text-center">
                                    <h3>Student Manuel Attendance</h3>
                                    <h5 class="mb-4">
                                    </h5>
                                </div>
                                <table class="table table-bordered  table-sm ">
                                    <thead class="text-center">
                                    <tr>
                                        <th>Student ID</th>
                                        <th>Name</th>
                                        <th>Class</th>
                                        <th>Date</th>
                                        <th>Attendance </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($attendances as $key=>$attn)
                                        <tr>
                                            <td>{{$attn->studentAcademic->student->studentid ?? ''}}</td>
                                            <td>{{$attn->studentAcademic->student->name ?? ''}}</td>
                                            <td>{{$attn->studentAcademic->classes->name ?? ''}} {{$attn->studentAcademic->section->name ?? ''}} {{$attn->studentAcademic->group->name ?? ''}}  </td>
                                            <td>{{$attn->date->format('d/m/Y') ?? ''}}</td>
                                            <td class="text-center">
                                                <input type="checkbox" name="check[{{ $attn->id }}]" value="1" {{ $attn->attendance_status_id == 1 ? 'checked' : '' }} />
                                            </td>
                                        </tr>
                                    @empty
                                        <td colspan="8" class="text-center text-bold text-danger">No data found! 😒 </td>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                            @if(count($attendances) > 0)
                            <button type="submit" class="btn btn-warning btn-sm btn-block">  Save</button>
                            @endif
                        </form>
                    </div>
                </div>
            </div>

        </section>
      @endif
    @endif

@stop

@section('plugin')
@stop
@section('script')

 <script>
        $(function() {

            // on form submit
            $("#form").on('submit', function() {
                // to each unchecked checkbox
                $(this).find('input[type=checkbox]:not(:checked)').prop('checked', true).val(2);
            })
        })

            // $("#checkAll").click(function(){
            //     $('input:checkbox').not(this).prop('checked', this.checked);
            // });
    </script>
@stop
