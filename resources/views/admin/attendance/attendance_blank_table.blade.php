@extends('layouts.fixed')
@section('title', 'Attendance  table')
@section('style')
    <style>
        @media print {
            .no_print {
                display: none;
            }
        }
        @page {
            /*size: A4;*/
            margin: 0;
            border: 1px solid black;
        }
    </style>
@stop

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Attendance Blank Table') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{ __('Attendance') }}</a></li>
                        <li class="breadcrumb-item active">{{ __(' Attendance-table') }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- /.Search-panel -->
    <section class="content no_print ">
        <div class="container-fluid">
            {{-- start --}}
            <div class="col-lg-12 col-sm-8 col-md-8 col-xs-12 ">
                <div class="card card-primary card-outline">
                    <div class="card-body">
                        <form method="get" action="{{ route('student.attendance.blank-table') }}">
                            <div class="form-row" >
                                <div class="form-group col">
                                    <label>Academic Class </label>
                                    <select name="academic_class" id="" class=" form-control select2" >
                                        <option value="">Select class</option>
                                            @foreach ($academic_class as $class)
                                                <option value="{{ $class->id }}" {{ ( request()->academic_class ==  $class->id ? 'selected' : '') }}>
                                                    {{ $class->classes->name }} {{ $class->section->name ?? '' }} {{ $class->group->name ?? '' }}
                                                </option>
                                            @endforeach
                                    </select>
                                </div>
                                
                                <div class="form-group col">
                                    <label>Month </label>
                                    <select name="month" id="" class=" form-control select2" >
                                        <option value="">Select Month</option>
                                            @foreach ($months as $mnt)
                                                <option value="{{ $mnt->id }}" {{ ( request()->month ==  $mnt->id ? 'selected' : '') }}>
                                                    {{ $mnt->name }}
                                                </option>
                                            @endforeach
                                    </select>
                                </div>

                                <div class="form-group col">
                                    <label>Year </label>
                                    <select name="year" id="" class=" form-control select2" >
                                        <option value="">Select Year</option>
                                     @php $year = 2020 @endphp
                                        @for ($year = $year; $year <= 2030; $year++)
                                            <option value="{{ $year }}">{{ $year }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="form-group  col mt-4" >
                                    <button type="submit" class="btn btn-info btn-md "><i
                                                class="fa fa-search"></i>
                                    </button>  
                                    <button class="btn btn-warning btn-md"
                                            onclick="window.print(); return false;"><i
                                                class="fa fa-print"></i>
                                    </button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.card -->
            </div>

        </div>{{-- end --}}
    </section>


    @if (isset($students))
        @if( $students->count() > 0)
        <section class="content mt-4">
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="card" >
                        <div class="card-body">
                            <div class="text-center">
                                <h4> Oxform Moder School & College</h4>
                             Motierpool, Pathantoli Road,
                                Doublemooring, Chattogram.
                                <h6>Class:
                                    {{ $students[0]->classes->name ?? '' }}
                                    {{ $students[0]->section->name ?? '' }}
                                    {{ $students[0]->group->name ?? '' }}
                                </h6>
                                <h6> @if(request()->month != null) Month: {{  $monthName->name . ','  .$reqYear }}@endif</h6>
                            </div>
                            <table class="table table-bordered  table-sm"  id="resizeMe">
                                <thead>
                                <tr>
                                    <th>Sl.</th>
                                    <th>Name</th>

                                        @for($i = 1;$i<=cal_days_in_month(CAL_GREGORIAN, request()->month,date('Y'));$i++)
                                            <th>{{ $i }}</th>
                                        @endfor
                                        


                                </tr>
                                </thead>
                                <tbody>
                                @foreach($students as $key=> $stu)
{{--                                    {{ $students }}--}}
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $stu->student->name }}</td>

                                        @for($i = 1;$i<=cal_days_in_month(CAL_GREGORIAN, request()->month,date('Y'));$i++)
                                            <th></th>
                                        @endfor

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <strong class=" mt-5 ">
                               <span> Total student : {{ $students->count() }}</span>
                            </strong>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        @else
            <h5 class="text-bold text-danger text-center"> Sorry!! Somethig went wrong!</h5>
        @endif
    @endif

@stop

@section('plugin')
@stop
@section('script')

<script type="text/javascript">
    $('.select2').select2({

    })
</script>


@stop
