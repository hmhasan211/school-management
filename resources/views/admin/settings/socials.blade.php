@extends('layouts.fixed')

@section('title','Socials')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Socials</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Settings</a></li>
                        <li class="breadcrumb-item active">social</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-10 offset-1" >
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card border shadow-lg">
                        <div class="card-header" >
                            <div class="row">
                                <div class="col-md-12">
                                    <div style="float: left;">
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"  style="margin-top: 10px; margin-left: 10px;"> <i class="fas fa-plus-circle"></i> New</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Provider</th>
                                    <th>Icon</th>
                                    <th>Address</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($i = 0)
                                @forelse($socials as $key=>$social)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $social->provider ?? '' }}</td>
                                        <td>{{ $social->icon ?? '' }}</td>
                                        <td>{{ $social->address ?? '' }}</td>
                                        <td class="text-center">
                                            @if($social->status == 1 )
                                                <a href="{{ route("social.status",$social->id )}}" title="Chage Status" ><i class="fa fa-circle  text-success " aria-hidden="true"></i></a>
                                            @else
                                                <a href="{{ route("social.status",$social->id) }}" title="Chage Status" ><i class="fa fa-circle  text-danger " aria-hidden="true"></i></a>
                                            @endif
                                        </td>
                                        <td>
{{--                                            {{ Form::model($social,['action'=>['Backend\SocialController@destroy',$social->id], 'method'=>'delete','id'=>'delete-form-'.{{$post->id}}]) }}--}}
{{--                                            <a type="button" class="btn btn-warning btn-sm edit" value='{{$social->id}}' title="Edit"> <i class="fas fa-edit"></i></a>--}}
{{--                                            {{ Form::submit('X',['class'=>'btn btn-danger btn-sm']) }}--}}
{{--                                            {{ Form::close() }}--}}

                                            <a type="button" class="btn btn-warning btn-sm edit" value='{{$social->id}}' title="Edit"> <i class="fas fa-edit"></i></a>
                                            <button class="btn btn-danger btn-sm" type="button" onclick="deleteData({{$social->id}})">
                                               <i class="fa fa-trash"></i>
                                            </button>
                                            <form id="delete-form-{{$social->id}}" method="post"  action="{{route('social.delete',$social->id)}}" style="display: none">
                                                @csrf
                                                @method('DELETE')
                                            </form>

                                        </td>
                                    </tr>
                                @empty
                                    <td colspan="10" class="text-danger text-bold text-center">  No Data Found!! 😒</td>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ***/ Pop Up Model for button -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Social</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    {!! Form::open(['action'=>'Backend\SocialController@store', 'method'=>'post']) !!}

                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label" style="font-weight: 500; text-align: right"> Name*</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {!! Form::text('provider', null, ['class'=>'form-control', 'placeholder'=>'Provider Name']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label" style="font-weight: 500; text-align: right">Icon</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {!! Form::text('icon', null, ['class'=>'form-control', 'placeholder'=>'Icon']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label" style="font-weight: 500; text-align: right">Address</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {!! Form::text('address', null, ['class'=>'form-control', 'placeholder'=>'Social address']) !!}
                            </div>
                        </div>
                    </div>

                    <div>
                        <button type="submit" class="btn btn-success  btn-block" > <i class="fas fa-plus-circle"></i> Add</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- ***/ Pop Up Model for button End-->


    <!-- ***/ Pop Up Model for Edit school -->
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['route'=>'social.update', 'method'=>'post']) !!}
                    {!! Form::hidden('id', null, ['id'=>'id']) !!}
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label" style="font-weight: 500; text-align: right"> Provider*</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {!! Form::text('provider', null, ['class'=>'form-control provider', 'placeholder'=>'Provider Name']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label" style="font-weight: 500; text-align: right">Icon</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {!! Form::text('icon', null, ['class'=>'form-control icon', 'placeholder'=>'Icon']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label" style="font-weight: 500; text-align: right">Social Address</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {!! Form::text('address', null, ['class'=>'form-control address', 'placeholder'=>'Social address']) !!}
                            </div>
                        </div>
                    </div>


                    <div>
                        <button type="submit" class="btn btn-warning btn-sm btn-block float-right">
                            <i class="fas fa-plus-circle"></i> Update
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- ***/ Pop Up Model for button End-->
@stop

@section('script')

    <script>
        $(document).on('click', '.edit', function () {
            $("#edit").modal("show");
            var id = $(this).attr('value');

            $.ajax({
                method:"get",
                url:"{{ url('social-edit')}}",
                data:{id:id,"_token":"{{ csrf_token() }}"},
                dataType:"json",
                success:function(response){
                    console.log(response);
                    $("#id").val(response.id);
                    $(".provider").val(response.provider);
                    $(".icon").val(response.icon);
                    $(".address").val(response.address);
                    $(".status").val(response.status);

                },
                error:function(err){
                    console.log(err);
                }
            });
        });

    </script>

@stop
