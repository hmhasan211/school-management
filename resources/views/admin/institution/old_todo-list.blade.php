@extends('layouts.fixed')

@section('title','ToDo')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>ToDo</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Institute</a></li>
                        <li class="breadcrumb-item active">Todo</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-8 offset-1">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card card-info">
                        <div class="card-header" >
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="float-left">
                                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"  style="margin-top: 10px; margin-left: 10px;"> <i class="fas fa-plus-circle"></i> New</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($i = 0)
                                @forelse($todos as $key=>$todo)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $todo->title ?? '' }}</td>
                                        <td> {{ $todo->description ?? '' }} </td>
                                        <td>
                                            {{ Form::model($todo,['action'=>['Backend\TodoController@destroy',$todo->id],'method'=>'delete','onsubmit'=>'return confirmDelete()']) }}
                                            <a type="button" class="btn btn-warning btn-sm edit" value='{{$todo->id}}' title="Edit"> <i class="fas fa-edit"></i></a>
                                            {{ Form::submit('X',['class'=>'btn btn-danger btn-sm']) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @empty
                                    <td colspan="10" class="text-danger text-bold text-center">  No Data Found!! 😒</td>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ***/ Pop Up Model for button -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add School</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    {!! Form::open(['action'=>'Backend\TodoController@store', 'method'=>'post']) !!}

                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label" > Title*</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {!! Form::text('title', null, ['class'=>'form-control ', 'placeholder'=>'Title']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label">Description</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {!! Form::text('description', null, ['class'=>'form-control ', 'placeholder'=>'Description']) !!}
                            </div>
                        </div>
                    </div>

                    <div style="float: right">
                        <button type="submit" class="btn btn-success  btn-sm" > <i class="fas fa-plus-circle"></i> Add</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <!-- ***/ Pop Up Model for Edit school -->
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update School</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['action'=>'Backend\TodoController@update', 'method'=>'post']) !!}
                    {!! Form::hidden('id', null, ['id'=>'id']) !!}
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label" > Title</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {!! Form::text('title', null, ['class'=>'form-control title', 'placeholder'=>'Title']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label" >Description</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {!! Form::text('description', null, ['class'=>'form-control description', 'placeholder'=>'Description']) !!}
                            </div>
                        </div>
                    </div>
                    <div style="float: right">
                        <button type="submit" class="btn btn-warning btn-sm">
                            <i class="fas fa-plus-circle"></i> Update
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- ***/ Pop Up Model for button End-->
@stop

@section('script')
    <script>
        $(document).on('click', '.edit', function () {
            $("#edit").modal("show");
            var id = $(this).attr('value');
            $.ajax({
                method:"get",
                url:"{{ url('todo-edit')}}",
                data:{id:id,"_token":"{{ csrf_token() }}"},
                dataType:"json",
                success:function(response){
                    console.log(response);
                    $("#id").val(response.id);
                    $(".title").val(response.title);
                    $(".description").val(response.description);
                },
                error:function(err){
                    console.log(err);
                }
            });
        });

        function confirmDelete(){
            let x = confirm('Are you sure you want to delete this School?');
            return !!x;
        }
    </script>
@stop
