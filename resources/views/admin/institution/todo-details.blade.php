@extends('layouts.fixed')

@section('title','ToDo-Details ')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{__('ToDo ')}} </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active">{{__('ToDo-Details ')}}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- /.Search-panel -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
               <div class="col-8 offset-2" >
                   <div class="card">
                       <div class="card-header">
                           <h2 class="card-title"> To-Do Details Info</h2>
                       </div>
                       <div class="card-body">

                           <table class=" table table-bordered" >
                               <tbody class="text-center">
                               <tr>
                                   <th>Title:</th>
                                   <td> {{ $data->title ?? '-' }} </td>
                               </tr>
                               <tr>
                                   <th>Description:</th>
                                   <td> {{ $data->description ?? '-' }} </td>
                               </tr>
                               <tr>
                                   <th>From:</th>
                                   <td>  {{ $data->start_date ?? '-'}} </td>
                               </tr>
                               <tr>
                                   <th>To:</th>
                                   <td>  {{ $data->end_date ?? '-'}} </td>
                               </tr>
                               <tr>
                                   <th>Time:</th>
                                   <td>  {{ $data->time?? '-' }} </td>
                               </tr>
                               <tr>
                                   <th>Venue:</th>
                                   <td>  {{ $data->venue ?? '-'}} </td>
                               </tr>
                               <tr>
                                   <th>Assign To:</th>
                                   <td>
                                       {{ $data->staff->name ?? '-' }}
                                   </td>
                               </tr>
                               <tr>
                                   <th>Status:</th>
                                   <td class=" text-bold @if($data->status == 1)  text-danger  @endif  text-success">  {{ $data->status == 1 ? ' Due Task ': 'Done' }} </td>
                               </tr>
                               <tr>
                                   <th>Notify:</th>
                                   <td class=" text-bold ">  {{ $data->notify == 1 ? ' Yes ': 'No' }} </td>
                               </tr>
                               </tbody>
                           </table>

                       </div>

{{--                       <ul class="list-group list-group-flush">--}}
{{--                           <tr>  <th>Description </th>  <td>  {{ $data->description }} </td> </tr>--}}
{{--                           <tr>  <th>Description </th>  <td>  {{ $data->description }} </td> </tr>--}}
{{--                           <li class="list-group-item">:  </li>--}}
{{--                           <li class="list-group-item">Date:  {{ $data->date }}</li>--}}
{{--                           <li class="list-group-item">Time:  {{ $data->time }}</li>--}}
{{--                           <li class="list-group-item">Vestibulum at eros</li>--}}
{{--                       </ul>--}}
                   </div>
               </div>
            </div>
        </div>
    </section>
@stop



@section('plugin')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@stop
@section('script')
    <script type="text/javascript">
        $('.select2').select2({})
    </script>
    <script>
        function deleteConfirm() {
            var x = confirm('Are you sure you wan to delete this Todo?');
            return !!x;
        }
    </script>
@stop
