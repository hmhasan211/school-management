@extends('layouts.fixed')

@section('title','Todo ')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{__('Todo ')}} </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active">{{__('Todo ')}}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-4">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col">
                            <div class="card card-warning">
                                <div class="card-header">
                                    <h5>Update ToDo</h5>
                                </div>
                                <div class="card-body">
                                    {{ Form::model($todo,['action'=>['Backend\TodoController@update',$todo->id],'method'=>'patch']) }}
                                    <div class="form-group">
                                        <label for="email">{{ __('Title')}}</label>
                                        <input class="form-control @error('title') is-invalid @enderror"  name="title" type="text" value="{{ $todo->title }}">
                                        <div class="invalid-feedback">
                                            @error('title') {{ $message }} @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">{{ __('Description')}}</label>
                                        <input class="form-control @error('description') is-invalid @enderror"  name="description" type="text" value="{{ $todo->description }}">
                                        <div class="invalid-feedback">
                                            @error('description') {{ $message }} @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="email">AssignTo</label>
                                        <select name="staff_id" class="form-control select2" id="">
                                            <option value="" >Select</option>
                                            @foreach($staff as $data)
                                                <option value="{{ $data->id }}" {{ $todo->staff_id == $data->id  ? 'selected' : '' }}>
                                                    {{ $data->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="collapse show" id="collapseExample">

                                        <div class="form-group">
                                            <label  for="email">Venue</label>
                                            <input class="form-control  "  value="{{ $todo->venue ?? '' }}" name="venue" type="text">
                                            <div class="invalid-feedback">
                                                @error('venue') {{ $message }} @enderror
                                            </div>
                                        </div>

                                        <div class="form-row ">

                                            <div class="form-group col-6">
                                                <label for="email">Start Date</label>
                                                <input class="form-control" value="{{$todo->start_date ?? '' }}" name="start_date"  type="date">
                                            </div>


                                            <div class="form-group col-6">
                                                <label for="email">End Date</label>
                                                <input class="form-control" value="{{$todo->end_date ?? '' }}" name="end_date"  type="date">
                                            </div>

                                        </div>
                                        <div class="form-group ">
                                            <label for="email">Time</label>
                                            <input class="form-control " name="time" type="time" value="{{ $todo->time ?? '' }}">
                                            <div class="invalid-feedback">
                                                @error('time') {{ $message }} @enderror
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label for="email">Notifiable</label>
                                            <input  name="notify" type="checkbox" value="1" {{  ($todo->notify == 1 ? 'checked' : '') }}>
                                            <div class="invalid-feedback">
                                                @error('notify') {{ $message }} @enderror
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group ">
                                        <div class="row">
                                            <div class="col-6">
                                                <a class="btn btn-outline-danger btn-block" href="{{ route("todo.list") }}">Back</a>
{{--                                                <a class="btn btn-outline-danger btn-block" type="submit" value="Back">--}}
                                            </div>
                                            <div class="col-6">
                                                <input class="btn btn-outline-warning btn-block" type="submit" value="Update">
                                            </div>

                                        </div>
                                    </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-8">
                    <div class="row">
                        <div class="col">
                            <div class="card card-success">
                                <div class="card-header ">
                                    <h5> ToDo List</h5>
                                </div>
                                <div class="card-body">
                                    <table id="example2" class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>{{ __('Sl.')}}</th>
                                            <th>{{ __('Title')}}</th>
                                            <th>{{ __('Description')}}</th>
                                            <th>{{__('Status')}}</th>
                                            <th>{{ __('Action')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($todos as $key=>$h)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>{{ $h->title }}</td>
                                                <td>
                                                    {{ $h->description}}
                                                </td>
                                                <td>
                                                    @if($h->status == 1)
                                                        <span class="badge badge-danger">{{ __('Due Task')}}</span>
                                                    @else
                                                        <span class="badge badge-success">{{ __('Done')}}</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ Form::open(['action'=>['Backend\TodoController@destroy',$h->id],'method'=>'delete','onsubmit'=>'return deleteConfirm()']) }}
                                                    <a href="{{ action('Backend\TodoController@edit',$h->id) }}" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop



@section('plugin')
@stop
@section('script')
    <script type="text/javascript">
        $('.select2').select2({})
    </script>
    <script>
        function deleteConfirm() {
            var x = confirm('Are you sure you wan to delete this holiday?');
            return !!x;
        }
    </script>
@stop