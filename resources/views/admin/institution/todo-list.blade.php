@extends('layouts.fixed')

@section('title','ToDo')
@section('style')
    <style>
        @media print {
            .no_print {
                display: none;
            }
            .for_print{
                align-content: center;
            }
        }
    </style>
@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{__('To-Do ')}} </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{__('Home')}}</a></li>
                        <li class="breadcrumb-item active">{{__('To-Do ')}}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- /.Search-panel -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-4 no_print">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col">
                            <div class="card card-success">
                                <div class="card-header">
                                    <div class="row">
                                        <h5>Create To-Do</h5>

                                    </div>

                                </div>
                                <div class="card-body">
                                    {{ Form::open(['action'=>'Backend\TodoController@store','method'=>'post']) }}
                                    <div class="form-group">
                                        <label  for="email">Title</label> <i class="text-danger text-bold">*</i>
                                        <input class="form-control @error('title') is-invalid @enderror" placeholder="Todo Name" name="title" type="text">
                                        <div class="invalid-feedback">
                                            @error('title') {{ $message }} @enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Description</label>
                                        <input class="form-control @error('description') is-invalid @enderror" placeholder="Todo Name" name="description" type="text">
                                        <div class="invalid-feedback">
                                            @error('description') {{ $message }} @enderror
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-10">
                                            <label for="email">AssignTo</label>
                                            <select name="staff_id" class="form-control select2" id="">
                                                <option value="" >Select</option>
                                                @foreach($staff as $data)
                                                    <option value="{{ $data->id }}">{{ $data->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group mt-2 col-2">
                                            <label class=""></label>
                                        <button class=" btn btn-outline-warning btn-sm form-control" type="button" data-toggle="collapse"
                                                data-target="#expandPanel" aria-expanded="false"
                                                aria-controls="expandPanel">
                                            <i class="fa fa-plus-circle"></i>
                                        </button>
                                        </div>


                                    </div>


                                    <div class="collapse " id="expandPanel">

                                        <div class="form-group">
                                            <label  for="email">Venue</label>
                                            <input class="form-control @error('venue') is-invalid @enderror" placeholder=" add a venue " name="venue" type="text">
                                            <div class="invalid-feedback">
                                                @error('venue') {{ $message }} @enderror
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-6">
                                                <label for="email">Start Date</label>
                                                <input class="form-control" value="{{date('Y-m-d')}}" name="start_date"
                                                       type="date">
                                                <div class="invalid-feedback">
                                                    @error('description') {{ $message }} @enderror
                                                </div>
                                            </div>

                                            <div class="form-group col-6">
                                                <label for="email">End Date</label>
                                                <input class="form-control" value="{{date('Y-m-d')}}" name="end_date"
                                                       type="date">
                                                <div class="invalid-feedback">
                                                    @error('description') {{ $message }} @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group ml-auto mr-3">
                                            <label for="email">Time</label>
                                            <input class="form-control " name="time" type="time">
                                            <div class="invalid-feedback">
                                                @error('time') {{ $message }} @enderror
                                            </div>
                                        </div>

                                        <div class="form-group ">
                                            <label for="email">Notifiable</label>
                                            <input  name="notify" type="checkbox" value="1">
                                            <div class="invalid-feedback">
                                                @error('notify') {{ $message }} @enderror
                                            </div>
                                        </div>

                                    </div>
                                    <input class="btn btn-outline-info btn-block" type="submit" value="Create">
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-8 for_print">
                    <div class="row">
                        <div class="col">
                            <div class="card card-info">
                                <div class="card-header  ">
                                    <div class="row">
                                        <h5 class="ml-3"> ToDo List</h5>
                                        <div class="ml-auto">
                                            <form method="get" action="{{ route("todo.search") }}">
                                            <div class="input-group  no_print">
                                                <input type="text" class="form-control" name="search"
                                                       placeholder="search"
                                                       aria-label="search"
                                                       aria-describedby="button-addon2">
                                                <div class="input-group-append">
                                                    <button class="btn btn-secondary mr-2" type="submit" id="button-addon2">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                                        <i class="fa fa-calendar-check"></i>
                                                    </button>

                                                    <a href="{{ route('todo.list') }}">
                                                            <button class="btn btn-danger" type="button" title="Refresh page">
                                                                <span class="fas fa-sync-alt"></span>
                                                            </button>
                                                    </a>
                                                    <button class="btn btn-warning btn-sm ml-2 "
                                                            onclick="window.print(); return false;"><i
                                                                class="fa fa-print"></i>
                                                    </button>

                                                    <a  class="btn btn-success btn-sm" href="{{ route('todo.csv',['from'=>request()->from,'to'=> request()->to]) }}">Excel</a>

{{--                                                    <span data-href="/export-csv" id="export" class="btn btn-success btn-sm" onclick ="exportTasks (event.target);">Excel</span>--}}

                                                </div>
                                            </div>

                                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" >
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                                <div class="form-row">
                                                                    <div class="form-group col-6">
                                                                        <label class="text-bold text-dark">From</label>
                                                                        <input class="form-control"  name="from"
                                                                               type="date">
                                                                    </div>

                                                                    <div class="form-group col-6">
                                                                        <label class="text-bold text-dark">To </label>
                                                                        <input class="form-control"  name="to"
                                                                               type="date">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <table id="example2" class="table table-bordered table-hover ">
                                        <thead>
                                        <tr class="">
                                            <th>{{__('Date')}}</th>
                                            <th>{{__('Title')}}</th>
{{--                                            <th>{{__('Description')}}</th>--}}
                                            <th>{{__('Status')}}</th>
                                            <th>{{__('Assign')}}</th>
                                            <th>{{__('Action')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($todos as $key=>$h)
                                            <tr>
                                                <td>{{  date('d M,y', strtotime( $h->created_at)) }}</td>
                                                <td>{{ Str::words( $h->title, 10,'...')  }}</td>
{{--                                                <td>--}}
{{--                                                    {!! Str::words( $h->description, 5,'...')  !!}--}}
{{--                                                </td>--}}
                                                <td>
                                                    @if($h->status == 1)
                                                        <a href="{{ route("todo.status",$h->id) }}" class="badge badge-danger">{{__('Due Task')}}</a>
                                                    @else
                                                        <a href="{{ route("todo.status",$h->id) }}" class="badge badge-success">{{__('Done')}}</a>
                                                    @endif
                                                </td>
                                                <td>
                                                    <span class="badge badge-warning">{{ $h->staff->name ?? '' }}</span>
                                                </td>
                                                <td>

                                                    <!-- Example single danger button -->
                                                    <div class="btn-group no_print">
                                                        <button type="button" class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fa fa-tools"></i>
                                                        </button>
                                                        <div class="dropdown-menu text-center">

                                                            <a href="{{ action('Backend\TodoController@edit',$h->id) }}" class="  btn btn-warning btn-sm"><i class="fas fa-edit"></i> </a>
                                                            <a href="{{ action('Backend\TodoController@details',$h->id) }}" target="_blank" title="Details" role="button" class="  btn btn-success btn-sm"><i class="fas fa-eye"></i> </a>

                                                            <button class="btn btn-danger btn-sm" type="button" onclick="deleteData({{$h->id}})">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                            <form id="delete-form-{{$h->id}}" method="post"  action="{{route('todo.destroy',$h->id)}}" style="display: none">
                                                                @csrf
                                                                @method('DELETE')
                                                            </form>

                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                             @empty
                                            <td colspan="5">
                                                <h6 class="text-danger text-center"> Sorry No Data Found!!</h6>
                                            </td>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop



@section('plugin')
@stop
@section('script')

    <script type="text/javascript">
        $('.select2').select2({})
    </script>
    <script>
        function deleteConfirm() {
            var x = confirm('Are you sure you wan to delete this Todo?');
            return !!x;
        }
        // function exportTasks(_this) {
        //     let _url = $(_this).data('href');
        //     window.location.href = _url;
        // }
    </script>

@stop
