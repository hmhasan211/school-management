@extends('layouts.fixed')

@section('title','Board-Exam Form')
@section('style')
    <link rel="stylesheet" href="{{ asset('assets/custom-form/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/custom-form/style_tooltip.css') }}">
@stop
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    {{-- <h1 class="m-0 text-dark">Student</h1> --}}
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{ __('Home') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('Add New Student') }}</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-light">
                        @if($errors->any())
                            <div class="alert alert-danger" role="alert">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <!-- /.card-header -->
                        <!-- form start -->
                        {{--                        {!!  Form::open(['action'=>'Backend\StudentController@store', 'method'=>'post', 'enctype'=>'multipart/form-data']) !!}--}}
                        {{--                        @include('admin.student.form')--}}
                        {{--                        {!! Form::close() !!}--}}
                        <table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tbody><tr>
                                <td><form id="app_form" name="app_form" method="post" action="afeq.php" onsubmit="return app_form_validator(this)">
                                        <input type="hidden" name="track_code" id="track_code" value="TFQDK2KX405796S30851">
                                        <input type="hidden" name="cadre_type" id="cadre_type" value="1">
                                        <input type="hidden" name="pid" id="pid" value="431">
                                        <table width="1000" border="0" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                <td width="19"><img src="images/main_01.png" width="19" height="22"></td>
                                                <td width="961" class="main-top">&nbsp;</td>
                                                <td width="20"><img src="images/main_03.png" width="20" height="22"></td>
                                            </tr>
                                            <tr>
                                                <td class="main-left">&nbsp;</td>
                                                <td bgcolor="#E7E7E7"><table width="961" border="0" cellpadding="0" cellspacing="0" class="gap01">
                                                        <tbody><tr>
                                                            <td align="left" valign="top" bgcolor="#B3CBE6" class="bdr-body"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td width="10%" height="95" align="center"
                                                                            valign="middle"><img
                                                                                    src="{{  asset('assets/custom-form/oxford_logo.jpg')  }}"
                                                                                    width="75" height="75"></td>
                                                                        <td width="65%" height="95" align="left"
                                                                            valign="middle">
                                                                            <span class="GrayBlue20bold">Oxford Moder School & College </span>
                                                                            <br>
                                                                            <span class="GrayBlue14b">  Motierpool, Pathantoli Road,
                                                                                Doublemooring, Chattogram.</span><br>
                                                                        </td>
                                                                        <td width="25%" height="95" align="left"
                                                                            valign="middle"><span class="XamName">  </span><br>
                                                                    </tr>
                                                                    </tbody></table></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bdr-toplink"><tbody><tr><td bgcolor="#025A8A">
                                                                            <table border="0" cellpadding="5" cellspacing="0" class="cyan11">
                                                                                <tbody><tr>
                                                                                    <td width="80" height="25" align="left" valign="middle"><a href="home.php" class="link-home">Home</a></td>
                                                                                    <td align="left" valign="middle"><a href="imsize.php" class="link02">Photo/Signature Validator</a></td>
                                                                                    <td width="30" height="25" align="center" valign="middle">•</td>
                                                                                    <td align="left" valign="middle"><a href="options/plog.php" class="link02">Download Applicant's Copy</a></td>
                                                                                    <td width="30" height="25" align="center" valign="middle">•</td>
                                                                                    <td height="30" align="left" valign="middle"><a href="options/pay.php" class="link02">Payment Status</a></td>
                                                                                    <td width="30" height="25" align="center" valign="middle">•</td>
                                                                                    <td height="30" align="left" valign="middle"><a href="admitcard/index.php" class="link02">Admit Card</a></td>
                                                                                    <td width="30" height="25" align="center" valign="middle">•</td>
                                                                                    <td height="30" align="left" valign="middle"><a href="options/help.php" class="link02">Help</a></td>
                                                                                    <td width="30" height="25" align="center" valign="middle">•</td>
                                                                                    <td height="30" align="left" valign="middle"><a href="https://youtu.be/Ma1Y4BcCrBY" class="link02">Video Tutorial</a></td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td></tr></tbody></table></td>
                                                        </tr>

                                                        <tr>
                                                            <td align="center" valign="top" bgcolor="#C9DAED" class="bdr-body"><table width="99%" border="0" cellpadding="0" cellspacing="0">
                                                                    <tbody><tr>
                                                                        <td height="40" align="center" valign="middle" class="GrayBlue14b">
                                                                            Part - 1 : Personal Information<br><hr></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="GrayBlue12">
                                                                                <tbody><tr>
                                                                                    <td width="14%" height="35" align="left" valign="middle" class="bdr01">Applicant's Name</td>
                                                                                    <td width="1%" height="35" align="left" valign="middle" class="bdr01">:</td>
                                                                                    <td width="34%" height="35" align="left" valign="middle" class="bdr01"><input name="name" type="text" class="field-100" id="name" onkeypress="return alpha(event,letters)" onblur="javascript:{this.value = this.value.toUpperCase(); }"></td>
                                                                                    <td width="2%" height="35" align="left" valign="middle" class="bdr01">&nbsp;</td>
                                                                                    <td width="14%" height="35" align="left" valign="middle" class="bdr01">Father's Name</td>
                                                                                    <td width="1%" height="35" align="left" valign="middle" class="bdr01">:</td>
                                                                                    <td width="34%" height="35" align="left" valign="middle" class="bdr01"><input name="fathername" type="text" class="field-100" id="fathername" onkeypress="return alpha(event,letters)" onblur="javascript:{this.value = this.value.toUpperCase(); }"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">Date of Birth</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">:</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">
                                                                                        <select name="b_day" class="field-dropdown-M" id="b_day">
                                                                                            <option selected="selected" value="0">Day</option>
                                                                                            <option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option>
                                                                                        </select>
                                                                                        <select name="b_month" class="field-dropdown-M" id="b_month">
                                                                                            <option selected="selected" value="0">Month</option>
                                                                                            <option value="01">January</option>
                                                                                            <option value="02">February</option>
                                                                                            <option value="03">March</option>
                                                                                            <option value="04">April</option>
                                                                                            <option value="05">May</option>
                                                                                            <option value="06">June</option>
                                                                                            <option value="07">July</option>
                                                                                            <option value="08">August</option>
                                                                                            <option value="09">September</option>
                                                                                            <option value="10">October</option>
                                                                                            <option value="11">November</option>
                                                                                            <option value="12">December</option>
                                                                                        </select>
                                                                                        <select name="b_year" class="field-dropdown-M" id="b_year">
                                                                                            <option selected="selected" value="0">Year</option>
                                                                                            <option value="1989">1989</option><option value="1990">1990</option><option value="1991">1991</option><option value="1992">1992</option><option value="1993">1993</option><option value="1994">1994</option><option value="1995">1995</option><option value="1996">1996</option><option value="1997">1997</option><option value="1998">1998</option><option value="1999">1999</option><option value="2000">2000</option><option value="2001">2001</option>
                                                                                        </select>
                                                                                    </td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">&nbsp;</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">Mother's Name</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">:</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01"><input name="mothername" type="text" class="field-100" id="mothername" onkeypress="return alpha(event,letters)" onblur="javascript:{this.value = this.value.toUpperCase(); }"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">Gender</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">:</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01-A">
                                                                                        <label><input type="radio" name="sex" value="1" id="sex_01">Male</label>
                                                                                        <label><input type="radio" name="sex" value="2" id="sex_02">Female</label>
                                                                                        <label><input type="radio" name="sex" value="3" id="sex_03">Third Gender</label></td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">&nbsp;</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">Employment Status</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">:</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">
                                                                                        <select name="employed" class="field-dropdown-L" id="employed">
                                                                                            <option selected="selected" value="0">Select One</option>
                                                                                            <option value="1">Not Employed</option>
                                                                                            <option value="2">Regular basis under revenue budget</option>
                                                                                            <option value="3"> Ad-hoc basis in non cadre post under revenue budget</option>
                                                                                            <option value="4">Temporary basis under revenue budget</option>
                                                                                            <option value="5">Work Charged basis under revenue budget</option>
                                                                                            <option value="6">Temporary basis under development project</option>
                                                                                            <option value="7">Work Charged basis under development project</option>
                                                                                            <option value="8">Autonomous/Semi Autonomous Organisation</option>
                                                                                            <option value="9">Private Organisation</option>
                                                                                        </select></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">Ethnic Minority</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">:</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01-A">
                                                                                        <label><input type="radio" name="tribal" value="1" id="tribal1"> Yes</label>
                                                                                        <label><input type="radio" name="tribal" value="2" id="tribal2"> No</label></td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">&nbsp;</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">Freedom Fighter Status</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">:</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01"><select name="ffq" class="field-dropdown-L" id="ffq">
                                                                                            <option selected="selected" value="0">Select One</option>
                                                                                            <option value="2">Child of Freedom Fighter</option>
                                                                                            <option value="3">Grand Child of Freedom Fighter</option>
                                                                                            <option value="4">Non Freedom Fighter</option>
                                                                                        </select></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01"><span>Marital Status</span></td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">:</td>
                                                                                    <td height="35" colspan="5" align="left" valign="middle" class="bdr01-A"><label>
                                                                                            <input type="radio" name="mstatus" value="1" id="mstatus_01">
                                                                                            Married</label>
                                                                                        <label style="display: none;"><span class="black11">[Spouse Name]</span>
                                                                                            <input name="s_name" id="s_name" type="text" class="DEPENDS ON mstatus BEING 1" size="40" onkeypress="return alpha(event,letters)" onblur="javascript:{this.value = this.value.toUpperCase(); }" style="display: none;" hidden="">
                                                                                        </label>
                                                                                        <label>
                                                                                            <input type="radio" name="mstatus" value="2" id="mstatus_02">
                                                                                            Single</label></td>
                                                                                </tr>
                                                                                </tbody></table></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="GrayBlue12">
                                                                                <tbody><tr>
                                                                                    <td width="14%" height="35" align="left" valign="middle" class="bdr01">Nationality</td>
                                                                                    <td width="1%" height="35" align="left" valign="middle" class="bdr01">:</td>
                                                                                    <td width="34%" height="35" align="left" valign="middle" class="bdr01"><select name="nationality" class="field-dropdown-M" id="nationality">
                                                                                            <option selected="selected" value="Bangladeshi">Bangladeshi</option>
                                                                                        </select></td>
                                                                                    <td width="2%" height="35" align="left" valign="middle" class="bdr01">&nbsp;</td>
                                                                                    <td width="14%" height="35" align="left" valign="middle" class="bdr01">National ID</td>
                                                                                    <td width="1%" height="35" align="left" valign="middle" class="bdr01">:</td>
                                                                                    <td width="34%" height="35" align="left" valign="middle" class="bdr01-A"><label>
                                                                                            <input type="radio" name="nid" value="1" id="nid_1">
                                                                                            Yes</label>
                                                                                        <label style="">
                                                                                            <input name="nid_no" type="text" id="nid_no" class="DEPENDS ON nid BEING 1" maxlength="20" onkeypress="return alpha(event,numbers)" style="display: none;" hidden="">
                                                                                            <input type="radio" name="nid" value="2" id="nid_2">
                                                                                            No</label></td>
                                                                                </tr>
                                                                                </tbody></table></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="GrayBlue12">
                                                                                <tbody><tr>
                                                                                    <td width="14%" height="35" align="left" valign="middle" class="bdr01">Disability</td>
                                                                                    <td width="1%" height="35" align="left" valign="middle" class="bdr01">:</td>
                                                                                    <td width="85%" align="left" valign="middle" class="bdr01-A">
                                                                                        <label>
                                                                                            <input type="radio" name="phc" value="0" id="phc_0">
                                                                                            None</label>
                                                                                        <label>
                                                                                            <input type="radio" name="phc" value="1" id="phc_1">
                                                                                            Visually Disabled <img src="images/phc_v.png" width="20" height="20" align="absmiddle"></label>
                                                                                        <label>
                                                                                            <input type="radio" name="phc" value="2" id="phc_2">
                                                                                            Physically Disabled <img src="images/phc_p.png" width="20" height="20" align="absmiddle"></label></td>
                                                                                </tr>
                                                                                </tbody></table></td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="GrayBlue12">
                                                                                <tbody><tr>
                                                                                    <td width="98%" height="35" align="left" valign="middle" class="bdr01">
                                                                                        Height [cm]: <select name="ph_height" class="field-dropdown-M" id="ph_height">
                                                                                            <option selected="selected" value="0">Height</option><option value="147">147</option><option value="148">148</option><option value="149">149</option><option value="150">150</option><option value="151">151</option><option value="152">152</option><option value="153">153</option><option value="154">154</option><option value="155">155</option><option value="156">156</option><option value="157">157</option><option value="158">158</option><option value="159">159</option><option value="160">160</option><option value="161">161</option><option value="162">162</option><option value="163">163</option><option value="164">164</option><option value="165">165</option><option value="166">166</option><option value="167">167</option><option value="168">168</option><option value="169">169</option><option value="170">170</option><option value="171">171</option><option value="172">172</option><option value="173">173</option><option value="174">174</option><option value="175">175</option><option value="176">176</option><option value="177">177</option><option value="178">178</option><option value="179">179</option><option value="180">180</option><option value="181">181</option><option value="182">182</option><option value="183">183</option><option value="184">184</option><option value="185">185</option><option value="186">186</option><option value="187">187</option><option value="188">188</option><option value="189">189</option><option value="190">190</option><option value="191">191</option><option value="192">192</option><option value="193">193</option><option value="194">194</option><option value="195">195</option><option value="196">196</option><option value="197">197</option><option value="198">198</option><option value="199">199</option><option value="200">200</option><option value="201">201</option><option value="202">202</option><option value="203">203</option><option value="204">204</option><option value="205">205</option><option value="206">206</option><option value="207">207</option><option value="208">208</option><option value="209">209</option><option value="210">210</option></select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        Weight [kg]: <select name="ph_weight" class="field-dropdown-M" id="ph_weight">
                                                                                            <option selected="selected" value="0">Weight</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option><option value="61">61</option><option value="62">62</option><option value="63">63</option><option value="64">64</option><option value="65">65</option><option value="66">66</option><option value="67">67</option><option value="68">68</option><option value="69">69</option><option value="70">70</option><option value="71">71</option><option value="72">72</option><option value="73">73</option><option value="74">74</option><option value="75">75</option><option value="76">76</option><option value="77">77</option><option value="78">78</option><option value="79">79</option><option value="80">80</option><option value="81">81</option><option value="82">82</option><option value="83">83</option><option value="84">84</option><option value="85">85</option><option value="86">86</option><option value="87">87</option><option value="88">88</option><option value="89">89</option><option value="90">90</option><option value="91">91</option><option value="92">92</option><option value="93">93</option><option value="94">94</option><option value="95">95</option><option value="96">96</option><option value="97">97</option><option value="98">98</option><option value="99">99</option><option value="100">100</option><option value="101">101</option><option value="102">102</option><option value="103">103</option><option value="104">104</option><option value="105">105</option><option value="106">106</option><option value="107">107</option><option value="108">108</option><option value="109">109</option><option value="110">110</option><option value="111">111</option><option value="112">112</option><option value="113">113</option><option value="114">114</option><option value="115">115</option><option value="116">116</option><option value="117">117</option><option value="118">118</option><option value="119">119</option><option value="120">120</option></select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        Chest [cm]: <select name="ph_chest" class="field-dropdown-M" id="ph_chest">
                                                                                            <option selected="selected" value="0">Chest</option><option value="70">70</option><option value="71">71</option><option value="72">72</option><option value="73">73</option><option value="74">74</option><option value="75">75</option><option value="76">76</option><option value="77">77</option><option value="78">78</option><option value="79">79</option><option value="80">80</option><option value="81">81</option><option value="82">82</option><option value="83">83</option><option value="84">84</option><option value="85">85</option><option value="86">86</option><option value="87">87</option><option value="88">88</option><option value="89">89</option><option value="90">90</option><option value="91">91</option><option value="92">92</option><option value="93">93</option><option value="94">94</option><option value="95">95</option><option value="96">96</option><option value="97">97</option><option value="98">98</option><option value="99">99</option><option value="100">100</option><option value="101">101</option><option value="102">102</option><option value="103">103</option><option value="104">104</option><option value="105">105</option><option value="106">106</option><option value="107">107</option><option value="108">108</option><option value="109">109</option><option value="110">110</option><option value="111">111</option><option value="112">112</option><option value="113">113</option><option value="114">114</option><option value="115">115</option><option value="116">116</option><option value="117">117</option><option value="118">118</option><option value="119">119</option><option value="120">120</option><option value="121">121</option><option value="122">122</option><option value="123">123</option><option value="124">124</option><option value="125">125</option><option value="126">126</option><option value="127">127</option><option value="128">128</option><option value="129">129</option><option value="130">130</option></select>
                                                                                    </td>
                                                                                    <td width="2%" height="35" align="left" valign="middle" class="bdr01-A">&nbsp;</td>
                                                                                </tr>
                                                                                </tbody></table></td>
                                                                    </tr>

                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" border="0"
                                                                                   cellpadding="0" cellspacing="0">
                                                                                <tbody>
                                                                                <tr>
                                                                                    <td width="49%" height="270"
                                                                                        align="left" valign="middle"
                                                                                        class="bdr01-A">
                                                                                        <table width="100%" border="0"
                                                                                               cellpadding="3"
                                                                                               cellspacing="0"
                                                                                               class="bdr-body">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td height="25"
                                                                                                    colspan="3"
                                                                                                    align="left"
                                                                                                    valign="middle"
                                                                                                    bgcolor="#89AED8"
                                                                                                    class="GrayBlue12">
                                                                                                    Present Address
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td width="30%"
                                                                                                    bgcolor="#B3CBE6">
                                                                                                    Care of <span
                                                                                                            class="black10">[optional]</span>
                                                                                                </td>
                                                                                                <td width="70%"
                                                                                                    bgcolor="#B3CBE6">
                                                                                                    <input name="present_care"
                                                                                                           type="text"
                                                                                                           class="field-100"
                                                                                                           id="present_care"
                                                                                                           onkeypress="return alpha(event,letters)">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td bgcolor="#B3CBE6">
                                                                                                    Village/Town/<br>
                                                                                                    Road/House/Flat
                                                                                                </td>
                                                                                                <td bgcolor="#B3CBE6">
                                                                                                    <textarea
                                                                                                            name="present_vill"
                                                                                                            rows="2"
                                                                                                            class="field-100"
                                                                                                            id="present_vill"
                                                                                                            onkeypress="return alpha(event,letters+numbers+custom)"></textarea>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td bgcolor="#B3CBE6">
                                                                                                    District
                                                                                                </td>
                                                                                                <td bgcolor="#B3CBE6">
                                                                                                    <select name="menu_one"
                                                                                                            class="field-dropdown-L"
                                                                                                            id="menu_one"
                                                                                                            onchange="get_one_list(this); changeVisibility1(this)">
                                                                                                        <option selected="selected"
                                                                                                                value="0">
                                                                                                            Select One
                                                                                                        </option>
                                                                                                        <option value="26">
                                                                                                            Bagerhat
                                                                                                        </option>
                                                                                                        <option value="64">
                                                                                                            Bandarban
                                                                                                        </option>
                                                                                                        <option value="32">
                                                                                                            Barguna
                                                                                                        </option>
                                                                                                        <option value="29">
                                                                                                            Barishal
                                                                                                        </option>
                                                                                                        <option value="30">
                                                                                                            Bhola
                                                                                                        </option>
                                                                                                        <option value="10">
                                                                                                            Bogura
                                                                                                        </option>
                                                                                                        <option value="54">
                                                                                                            Brahmanbaria
                                                                                                        </option>
                                                                                                        <option value="56">
                                                                                                            Chandpur
                                                                                                        </option>
                                                                                                        <option value="13">
                                                                                                            Chapainawabganj
                                                                                                        </option>
                                                                                                        <option value="60">
                                                                                                            Chattogram
                                                                                                        </option>
                                                                                                        <option value="19">
                                                                                                            Chuadanga
                                                                                                        </option>
                                                                                                        <option value="61">
                                                                                                            Coxsbazar
                                                                                                        </option>
                                                                                                        <option value="55">
                                                                                                            Cumilla
                                                                                                        </option>
                                                                                                        <option value="40">
                                                                                                            Dhaka
                                                                                                        </option>
                                                                                                        <option value="03">
                                                                                                            Dinajpur
                                                                                                        </option>
                                                                                                        <option value="45">
                                                                                                            Faridpur
                                                                                                        </option>
                                                                                                        <option value="59">
                                                                                                            Feni
                                                                                                        </option>
                                                                                                        <option value="08">
                                                                                                            Gaibandha
                                                                                                        </option>
                                                                                                        <option value="41">
                                                                                                            Gazipur
                                                                                                        </option>
                                                                                                        <option value="47">
                                                                                                            Gopalganj
                                                                                                        </option>
                                                                                                        <option value="53">
                                                                                                            Habiganj
                                                                                                        </option>
                                                                                                        <option value="36">
                                                                                                            Jamalpur
                                                                                                        </option>
                                                                                                        <option value="23">
                                                                                                            Jashore
                                                                                                        </option>
                                                                                                        <option value="28">
                                                                                                            Jhalakathi
                                                                                                        </option>
                                                                                                        <option value="20">
                                                                                                            Jhenaidah
                                                                                                        </option>
                                                                                                        <option value="09">
                                                                                                            Joypurhat
                                                                                                        </option>
                                                                                                        <option value="62">
                                                                                                            Khagrachhari
                                                                                                        </option>
                                                                                                        <option value="25">
                                                                                                            Khulna
                                                                                                        </option>
                                                                                                        <option value="38">
                                                                                                            Kishoreganj
                                                                                                        </option>
                                                                                                        <option value="07">
                                                                                                            Kurigram
                                                                                                        </option>
                                                                                                        <option value="17">
                                                                                                            Kushtia
                                                                                                        </option>
                                                                                                        <option value="57">
                                                                                                            Lakshmipur
                                                                                                        </option>
                                                                                                        <option value="05">
                                                                                                            Lalmonirhat
                                                                                                        </option>
                                                                                                        <option value="48">
                                                                                                            Madaripur
                                                                                                        </option>
                                                                                                        <option value="21">
                                                                                                            Magura
                                                                                                        </option>
                                                                                                        <option value="39">
                                                                                                            Manikganj
                                                                                                        </option>
                                                                                                        <option value="18">
                                                                                                            Meherpur
                                                                                                        </option>
                                                                                                        <option value="52">
                                                                                                            Moulvibazar
                                                                                                        </option>
                                                                                                        <option value="44">
                                                                                                            Munshiganj
                                                                                                        </option>
                                                                                                        <option value="34">
                                                                                                            Mymensingh
                                                                                                        </option>
                                                                                                        <option value="11">
                                                                                                            Naogaon
                                                                                                        </option>
                                                                                                        <option value="22">
                                                                                                            Narail
                                                                                                        </option>
                                                                                                        <option value="43">
                                                                                                            Narayanganj
                                                                                                        </option>
                                                                                                        <option value="42">
                                                                                                            Narsingdi
                                                                                                        </option>
                                                                                                        <option value="12">
                                                                                                            Natore
                                                                                                        </option>
                                                                                                        <option value="33">
                                                                                                            Netrokona
                                                                                                        </option>
                                                                                                        <option value="04">
                                                                                                            Nilphamari
                                                                                                        </option>
                                                                                                        <option value="58">
                                                                                                            Noakhali
                                                                                                        </option>
                                                                                                        <option value="16">
                                                                                                            Pabna
                                                                                                        </option>
                                                                                                        <option value="01">
                                                                                                            Panchagarh
                                                                                                        </option>
                                                                                                        <option value="31">
                                                                                                            Patuakhali
                                                                                                        </option>
                                                                                                        <option value="27">
                                                                                                            Pirojpur
                                                                                                        </option>
                                                                                                        <option value="46">
                                                                                                            Rajbari
                                                                                                        </option>
                                                                                                        <option value="14">
                                                                                                            Rajshahi
                                                                                                        </option>
                                                                                                        <option value="63">
                                                                                                            Rangamati
                                                                                                        </option>
                                                                                                        <option value="06">
                                                                                                            Rangpur
                                                                                                        </option>
                                                                                                        <option value="24">
                                                                                                            Satkhira
                                                                                                        </option>
                                                                                                        <option value="49">
                                                                                                            Shariatpur
                                                                                                        </option>
                                                                                                        <option value="35">
                                                                                                            Sherpur
                                                                                                        </option>
                                                                                                        <option value="15">
                                                                                                            Sirajganj
                                                                                                        </option>
                                                                                                        <option value="50">
                                                                                                            Sunamganj
                                                                                                        </option>
                                                                                                        <option value="51">
                                                                                                            Sylhet
                                                                                                        </option>
                                                                                                        <option value="37">
                                                                                                            Tangail
                                                                                                        </option>
                                                                                                        <option value="02">
                                                                                                            Thakurgaon
                                                                                                        </option>
                                                                                                    </select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td bgcolor="#B3CBE6">
                                                                                                    Upazilla/P.S <img
                                                                                                            src="images/loader.gif"
                                                                                                            border="0"
                                                                                                            align="absmiddle"
                                                                                                            name="loading1"
                                                                                                            style="visibility: hidden;">
                                                                                                </td>
                                                                                                <td bgcolor="#B3CBE6">
                                                                                                    <select name="menu_one_list"
                                                                                                            class="field-dropdown-L"
                                                                                                            id="menu_one_list"
                                                                                                            onchange="Show_ops1TextBox(this.id,'ops1');">

                                                                                                        <option value="">
                                                                                                            Select One
                                                                                                        </option>
                                                                                                        <option value="Alfadanga">
                                                                                                            Alfadanga
                                                                                                        </option>
                                                                                                        <option value="Bhanga">
                                                                                                            Bhanga
                                                                                                        </option>
                                                                                                        <option value="Boalmari">
                                                                                                            Boalmari
                                                                                                        </option>
                                                                                                        <option value="Charbhadrasan">
                                                                                                            Charbhadrasan
                                                                                                        </option>
                                                                                                        <option value="Faridpur Sadar">
                                                                                                            Faridpur
                                                                                                            Sadar
                                                                                                        </option>
                                                                                                        <option value="Madhukhali">
                                                                                                            Madhukhali
                                                                                                        </option>
                                                                                                        <option value="Nagarkanda">
                                                                                                            Nagarkanda
                                                                                                        </option>
                                                                                                        <option value="Sadarpur">
                                                                                                            Sadarpur
                                                                                                        </option>
                                                                                                        <option value="Saltha">
                                                                                                            Saltha
                                                                                                        </option>
                                                                                                    </select><br>
                                                                                                    <input name="ops1"
                                                                                                           type="text"
                                                                                                           class="field-100"
                                                                                                           id="ops1"
                                                                                                           style="VISIBILITY: hidden">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td bgcolor="#B3CBE6">
                                                                                                    Post Office
                                                                                                </td>
                                                                                                <td bgcolor="#B3CBE6">
                                                                                                    <input name="present_post"
                                                                                                           type="text"
                                                                                                           maxlength="20"
                                                                                                           class="field-100"
                                                                                                           id="present_post"
                                                                                                           onkeypress="return alpha(event,letters)">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td bgcolor="#B3CBE6">
                                                                                                    Post Code <span
                                                                                                            class="black10">[optional]</span>
                                                                                                </td>
                                                                                                <td bgcolor="#B3CBE6">
                                                                                                    <input name="present_pcode"
                                                                                                           type="text"
                                                                                                           maxlength="6"
                                                                                                           class="field-100"
                                                                                                           id="present_pcode"
                                                                                                           onkeypress="return alpha(event,numbers)">
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td width="2%" height="270"
                                                                                        align="left" valign="middle"
                                                                                        class="bdr01-A">&nbsp;
                                                                                    </td>
                                                                                    <td width="49%" height="270"
                                                                                        align="left" valign="middle"
                                                                                        class="bdr01-A">
                                                                                        <table width="100%" border="0"
                                                                                               cellpadding="3"
                                                                                               cellspacing="0"
                                                                                               class="bdr-body">
                                                                                            <tbody>
                                                                                            <tr>
                                                                                                <td height="25"
                                                                                                    colspan="3"
                                                                                                    align="left"
                                                                                                    valign="middle"
                                                                                                    bgcolor="#89AED8"
                                                                                                    class="GrayBlue12">
                                                                                                    Permanent Address
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td width="30%"
                                                                                                    height="20"
                                                                                                    bgcolor="#B3CBE6">
                                                                                                    Care of <span
                                                                                                            class="black10">[optional]</span>
                                                                                                </td>
                                                                                                <td width="70%"
                                                                                                    height="20"
                                                                                                    bgcolor="#B3CBE6">
                                                                                                    <input name="permanent_care"
                                                                                                           type="text"
                                                                                                           class="field-100"
                                                                                                           id="permanent_care"
                                                                                                           onkeypress="return alpha(event,letters)">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td height="20"
                                                                                                    bgcolor="#B3CBE6">
                                                                                                    Village/Town/<br>
                                                                                                    Road/House/Flat
                                                                                                </td>
                                                                                                <td height="20"
                                                                                                    bgcolor="#B3CBE6">
                                                                                                    <textarea
                                                                                                            name="permanent_vill"
                                                                                                            rows="2"
                                                                                                            class="field-100"
                                                                                                            id="permanent_vill"
                                                                                                            onkeypress="return alpha(event,letters+numbers+custom)"></textarea>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td height="20"
                                                                                                    bgcolor="#B3CBE6">
                                                                                                    Home District
                                                                                                </td>
                                                                                                <td height="20"
                                                                                                    bgcolor="#B3CBE6">
                                                                                                    <select name="menu_two"
                                                                                                            class="field-dropdown-L"
                                                                                                            id="menu_two"
                                                                                                            onchange="get_two_list(this); changeVisibility2(this)">
                                                                                                        <option selected="selected"
                                                                                                                value="0">
                                                                                                            Select One
                                                                                                        </option>
                                                                                                        <option value="26">
                                                                                                            Bagerhat
                                                                                                        </option>
                                                                                                        <option value="64">
                                                                                                            Bandarban
                                                                                                        </option>
                                                                                                        <option value="32">
                                                                                                            Barguna
                                                                                                        </option>
                                                                                                        <option value="29">
                                                                                                            Barishal
                                                                                                        </option>
                                                                                                        <option value="30">
                                                                                                            Bhola
                                                                                                        </option>
                                                                                                        <option value="10">
                                                                                                            Bogura
                                                                                                        </option>
                                                                                                        <option value="54">
                                                                                                            Brahmanbaria
                                                                                                        </option>
                                                                                                        <option value="56">
                                                                                                            Chandpur
                                                                                                        </option>
                                                                                                        <option value="13">
                                                                                                            Chapainawabganj
                                                                                                        </option>
                                                                                                        <option value="60">
                                                                                                            Chattogram
                                                                                                        </option>
                                                                                                        <option value="19">
                                                                                                            Chuadanga
                                                                                                        </option>
                                                                                                        <option value="61">
                                                                                                            Coxsbazar
                                                                                                        </option>
                                                                                                        <option value="55">
                                                                                                            Cumilla
                                                                                                        </option>
                                                                                                        <option value="40">
                                                                                                            Dhaka
                                                                                                        </option>
                                                                                                        <option value="03">
                                                                                                            Dinajpur
                                                                                                        </option>
                                                                                                        <option value="45">
                                                                                                            Faridpur
                                                                                                        </option>
                                                                                                        <option value="59">
                                                                                                            Feni
                                                                                                        </option>
                                                                                                        <option value="08">
                                                                                                            Gaibandha
                                                                                                        </option>
                                                                                                        <option value="41">
                                                                                                            Gazipur
                                                                                                        </option>
                                                                                                        <option value="47">
                                                                                                            Gopalganj
                                                                                                        </option>
                                                                                                        <option value="53">
                                                                                                            Habiganj
                                                                                                        </option>
                                                                                                        <option value="36">
                                                                                                            Jamalpur
                                                                                                        </option>
                                                                                                        <option value="23">
                                                                                                            Jashore
                                                                                                        </option>
                                                                                                        <option value="28">
                                                                                                            Jhalakathi
                                                                                                        </option>
                                                                                                        <option value="20">
                                                                                                            Jhenaidah
                                                                                                        </option>
                                                                                                        <option value="09">
                                                                                                            Joypurhat
                                                                                                        </option>
                                                                                                        <option value="62">
                                                                                                            Khagrachhari
                                                                                                        </option>
                                                                                                        <option value="25">
                                                                                                            Khulna
                                                                                                        </option>
                                                                                                        <option value="38">
                                                                                                            Kishoreganj
                                                                                                        </option>
                                                                                                        <option value="07">
                                                                                                            Kurigram
                                                                                                        </option>
                                                                                                        <option value="17">
                                                                                                            Kushtia
                                                                                                        </option>
                                                                                                        <option value="57">
                                                                                                            Lakshmipur
                                                                                                        </option>
                                                                                                        <option value="05">
                                                                                                            Lalmonirhat
                                                                                                        </option>
                                                                                                        <option value="48">
                                                                                                            Madaripur
                                                                                                        </option>
                                                                                                        <option value="21">
                                                                                                            Magura
                                                                                                        </option>
                                                                                                        <option value="39">
                                                                                                            Manikganj
                                                                                                        </option>
                                                                                                        <option value="18">
                                                                                                            Meherpur
                                                                                                        </option>
                                                                                                        <option value="52">
                                                                                                            Moulvibazar
                                                                                                        </option>
                                                                                                        <option value="44">
                                                                                                            Munshiganj
                                                                                                        </option>
                                                                                                        <option value="34">
                                                                                                            Mymensingh
                                                                                                        </option>
                                                                                                        <option value="11">
                                                                                                            Naogaon
                                                                                                        </option>
                                                                                                        <option value="22">
                                                                                                            Narail
                                                                                                        </option>
                                                                                                        <option value="43">
                                                                                                            Narayanganj
                                                                                                        </option>
                                                                                                        <option value="42">
                                                                                                            Narsingdi
                                                                                                        </option>
                                                                                                        <option value="12">
                                                                                                            Natore
                                                                                                        </option>
                                                                                                        <option value="33">
                                                                                                            Netrokona
                                                                                                        </option>
                                                                                                        <option value="04">
                                                                                                            Nilphamari
                                                                                                        </option>
                                                                                                        <option value="58">
                                                                                                            Noakhali
                                                                                                        </option>
                                                                                                        <option value="16">
                                                                                                            Pabna
                                                                                                        </option>
                                                                                                        <option value="01">
                                                                                                            Panchagarh
                                                                                                        </option>
                                                                                                        <option value="31">
                                                                                                            Patuakhali
                                                                                                        </option>
                                                                                                        <option value="27">
                                                                                                            Pirojpur
                                                                                                        </option>
                                                                                                        <option value="46">
                                                                                                            Rajbari
                                                                                                        </option>
                                                                                                        <option value="14">
                                                                                                            Rajshahi
                                                                                                        </option>
                                                                                                        <option value="63">
                                                                                                            Rangamati
                                                                                                        </option>
                                                                                                        <option value="06">
                                                                                                            Rangpur
                                                                                                        </option>
                                                                                                        <option value="24">
                                                                                                            Satkhira
                                                                                                        </option>
                                                                                                        <option value="49">
                                                                                                            Shariatpur
                                                                                                        </option>
                                                                                                        <option value="35">
                                                                                                            Sherpur
                                                                                                        </option>
                                                                                                        <option value="15">
                                                                                                            Sirajganj
                                                                                                        </option>
                                                                                                        <option value="50">
                                                                                                            Sunamganj
                                                                                                        </option>
                                                                                                        <option value="51">
                                                                                                            Sylhet
                                                                                                        </option>
                                                                                                        <option value="37">
                                                                                                            Tangail
                                                                                                        </option>
                                                                                                        <option value="02">
                                                                                                            Thakurgaon
                                                                                                        </option>
                                                                                                    </select></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td height="20"
                                                                                                    bgcolor="#B3CBE6">
                                                                                                    Upazilla/P.S <img
                                                                                                            src="images/loader.gif"
                                                                                                            border="0"
                                                                                                            align="absmiddle"
                                                                                                            name="loading2"
                                                                                                            style="visibility:hidden">
                                                                                                </td>
                                                                                                <td height="20"
                                                                                                    bgcolor="#B3CBE6">
                                                                                                    <select name="menu_two_list"
                                                                                                            class="field-dropdown-L"
                                                                                                            id="menu_two_list"
                                                                                                            onchange="Show_ops1TextBox(this.id,'ops2');">
                                                                                                        <option selected="selected"
                                                                                                                value="0">
                                                                                                            Select One
                                                                                                        </option>
                                                                                                    </select><br>
                                                                                                    <input name="ops2"
                                                                                                           type="text"
                                                                                                           class="field-100"
                                                                                                           id="ops2"
                                                                                                           style="VISIBILITY: hidden">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td height="20"
                                                                                                    bgcolor="#B3CBE6">
                                                                                                    Post Office
                                                                                                </td>
                                                                                                <td height="20"
                                                                                                    bgcolor="#B3CBE6">
                                                                                                    <input name="permanent_post"
                                                                                                           type="text"
                                                                                                           maxlength="20"
                                                                                                           class="field-100"
                                                                                                           id="permanent_post"
                                                                                                           onkeypress="return alpha(event,letters)">
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td bgcolor="#B3CBE6">
                                                                                                    Post Code <span
                                                                                                            class="black10">[optional]</span>
                                                                                                </td>
                                                                                                <td bgcolor="#B3CBE6">
                                                                                                    <input name="permanent_pcode"
                                                                                                           type="text"
                                                                                                           maxlength="6"
                                                                                                           class="field-100"
                                                                                                           id="permanent_pcode"
                                                                                                           onkeypress="return alpha(event,numbers)">
                                                                                                </td>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="GrayBlue12">
                                                                                <tbody><tr>
                                                                                    <td width="13%" height="35" align="left" valign="middle" class="bdr01">Contact Mobile</td>
                                                                                    <td width="1%" height="35" align="left" valign="middle" class="bdr01">:</td>
                                                                                    <td width="37%" height="35" align="left" valign="middle" class="bdr01"><input name="mobileno" type="text" class="field-100" id="mobileno" maxlength="11" onkeypress="return alpha(event,numbers)"></td>
                                                                                    <td width="49%" height="35" colspan="3" align="left" valign="middle" class="bdr01-A"><span class="red11">
					                                                                Please mention a Mobile Number of any operator. Relevant information will be sent by SMS to that number.</span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">Re-Type Mobile</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">:</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01"><input name="confirmmobile" type="text" class="field-100" id="confirmmobile" maxlength="11" onkeypress="return onlyNumbers()"></td>
                                                                                    <td height="35" colspan="3" align="left" valign="middle" class="bdr01-A"><span class="black11">Confirm the Mobile Number.</span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">Exam Centre</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">:</td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01"><select name="exam_centre" class="field-dropdown-L" id="exam_centre">
                                                                                            <option selected="selected" value="0">Select One</option>

                                                                                            <option value="5">BARISAL</option>
                                                                                            <option value="3">CHATTOGRAM</option>
                                                                                            <option value="1">DHAKA</option>
                                                                                            <option value="4">KHULNA</option>
                                                                                            <option value="2">RAJSHAHI</option>
                                                                                            <option value="6">SYLHET</option>
                                                                                            <option value="7">RANGPUR</option>
                                                                                            <option value="8">MYMENSINGH</option>
                                                                                        </select></td>
                                                                                    <td height="35" align="left" valign="middle" class="bdr01">&nbsp;</td>
                                                                                </tr>
                                                                                </tbody></table></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="60" align="center" valign="middle"><table width="100%" border="0" cellpadding="3" cellspacing="0" class="bdr-toplink">
                                                                                <tbody><tr>
                                                                                    <td height="40" align="center" valign="middle" bgcolor="#025A8A"><span class="OffWhite12b">Question Version: Tick the box if you want english version question for the preliminary test</span> <input name="qlang" type="checkbox" id="qlang" value="1" onclick="qagr(this)"></td>
                                                                                </tr>
                                                                                </tbody></table></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="30" align="center" valign="middle" class="black14"><input type="checkbox" name="info_yes" id="info_yes" onclick="agreesubmit(this)">
                                                                            The above information is correct and I would like to go to the next step.</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center" valign="middle">
                                                                            <input type="submit" name="next" id="button01" value="Next"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="40" align="center" valign="middle"><img name="im" src="images/blank.png"></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td align="left" valign="top" bgcolor="#B3CBE6" class="bdr-body"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tbody><tr>
                                                                        <td width="0%" height="50" align="center" valign="middle" class="black10">&nbsp;</td>
                                                                        <td width="49%" height="50" align="left" valign="middle">
                                                                            <table width="100%" border="0" cellpadding="5" cellspacing="0" class="black10">
                                                                                <tbody><tr>
                                                                                    <td width="85%" align="left" valign="middle"></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="left" valign="middle"></td>
                                                                                </tr>
                                                                                </tbody></table></td>
                                                                        <td width="40%" height="50" align="left" valign="middle"><table width="100%" border="0" cellpadding="5" cellspacing="0" class="black10">
                                                                                <tbody><tr>
                                                                                    <td width="85%" align="right" valign="middle">&nbsp;</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td align="right" valign="middle"></td>
                                                                                </tr>
                                                                                </tbody></table></td>
                                                                        <td width="10%" height="50" align="center" valign="middle"></td>
                                                                    </tr>
                                                                    <td align="center" valign="top" bgcolor="#C9DAED" class="bdr-body"><table width="99%" border="0" cellpadding="0" cellspacing="0">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td height="40" align="center" valign="middle" class="GrayBlue14b">
                                                                                    Part - 2 : Educational Qualifications<br><hr></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                                        <tbody><tr>
                                                                                            <td width="49%" align="left" valign="middle"><table width="100%" border="0" cellpadding="3" cellspacing="0" class="bdr-body">
                                                                                                    <tbody><tr>
                                                                                                        <td height="20" colspan="3" align="left" valign="middle" bgcolor="#89AED8" class="GrayBlue12">S S C or Equivalent</td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td width="34%" align="left" valign="middle" bgcolor="#B3CBE6">Examination</td>
                                                                                                        <td width="4%" align="left" valign="middle" bgcolor="#B3CBE6">&nbsp;</td>
                                                                                                        <td width="62%" align="left" valign="middle" bgcolor="#B3CBE6"><select name="exam1" class="field-dropdown-E" id="exam1">
                                                                                                                <option selected="selected" value="0">Select One</option>
                                                                                                                <option value="1">S.S.C</option><option value="2">Dakhil</option><option value="3">S.S.C Vocational</option><option value="4">O Level/Cambridge</option><option value="5">S.S.C Equivalent</option>
                                                                                                            </select></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">Board</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">&nbsp;</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6"><select name="institute1" class="field-dropdown-E" id="institute1">
                                                                                                                <option selected="selected" value="0">Select One</option>
                                                                                                                <option value="1">Dhaka</option><option value="2">Comilla</option><option value="3">Rajshahi</option><option value="4">Jessore</option><option value="5">Chittagong</option><option value="6">Barisal</option><option value="7">Sylhet</option><option value="8">Dinajpur</option><option value="9">Madrasah</option><option value="10">Technical</option><option value="15">Cambridge International - IGCE</option><option value="16">Edexcel International</option><option value="20">Others</option><option value="11">Mymensingh</option>
                                                                                                            </select></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">Roll</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">&nbsp;</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6"><input name="roll1" type="text" maxlength="10" class="field-E" id="roll1" onkeypress="return alpha(event,numbers)"></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">Result</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">&nbsp;</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="black12">
                                                                                                                <tbody><tr>
                                                                                                                    <td width="60%" align="left" valign="middle"><select name="result1" class="field-dropdown-E" id="result1" onchange="Show_GpaTextBox(this.id,'result_gpa1');">
                                                                                                                            <option value="0" selected="selected">Select</option>
                                                                                                                            <option value="1">1st Division</option>
                                                                                                                            <option value="2">2nd Division</option>
                                                                                                                            <option value="3">3rd Division</option>
                                                                                                                            <option value="4">GPA/CGPA in scale 4</option>
                                                                                                                            <option value="5">GPA/CGPA in scale 5</option>
                                                                                                                        </select></td>
                                                                                                                    <td width="25%" align="left" valign="middle"><input name="result_gpa1" class="field-G" id="result_gpa1" style="VISIBILITY: hidden" maxlength="4" onkeypress="return alpha(event,numbers)"></td>
                                                                                                                    <td width="15%" align="left" valign="middle" id="Caption_Marks1">&nbsp;</td>
                                                                                                                </tr>
                                                                                                                </tbody></table></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">Group</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">&nbsp;</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6"><select name="subject1" class="field-dropdown-E" id="subject1">
                                                                                                                <option selected="selected" value="0">Select One</option>
                                                                                                                <option value="1">Science</option><option value="2">Humanities</option><option value="3">Commerce</option><option value="9">Others</option>
                                                                                                            </select></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">Passing Year</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">&nbsp;</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6"><select name="year1" class="field-dropdown-E" id="year1">
                                                                                                                <option selected="selected" value="0">Select One</option>
                                                                                                                <option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option>
                                                                                                            </select></td>
                                                                                                    </tr>
                                                                                                    </tbody></table></td>
                                                                                            <td width="2%" align="left" valign="middle">&nbsp;</td>
                                                                                            <td width="49%" align="left" valign="middle"><table width="100%" border="0" cellpadding="3" cellspacing="0" class="bdr-body">
                                                                                                    <tbody><tr>
                                                                                                        <td height="20" colspan="3" align="left" valign="middle" bgcolor="#89AED8" class="GrayBlue12">H S C or Equivalent</td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td width="34%" align="left" valign="middle" bgcolor="#B3CBE6">Examination</td>
                                                                                                        <td width="4%" align="left" valign="middle" bgcolor="#B3CBE6">&nbsp;</td>
                                                                                                        <td width="62%" align="left" valign="middle" bgcolor="#B3CBE6"><select name="exam2" class="field-dropdown-E" id="exam2">
                                                                                                                <option selected="selected" value="0">Select One</option>
                                                                                                                <option value="1">H.S.C</option><option value="2">Alim</option><option value="3">Business Management</option><option value="4">Diploma</option><option value="5">A Level/Sr. Cambridge</option><option value="6">H.S.C Equivalent</option>
                                                                                                            </select></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">Board</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">&nbsp;</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6"><select name="institute2" class="field-dropdown-E" id="institute2">
                                                                                                                <option selected="selected" value="0">Select One</option>
                                                                                                                <option value="1">Dhaka</option><option value="2">Comilla</option><option value="3">Rajshahi</option><option value="4">Jessore</option><option value="5">Chittagong</option><option value="6">Barisal</option><option value="7">Sylhet</option><option value="8">Dinajpur</option><option value="9">Madrasah</option><option value="10">Technical</option><option value="15">Cambridge International - IGCE</option><option value="16">Edexcel International</option><option value="20">Others</option><option value="11">Mymensingh</option>
                                                                                                            </select></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">Roll</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">&nbsp;</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6"><input name="roll2" type="text" maxlength="10" class="field-E" id="roll2" onkeypress="return alpha(event,numbers)"></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">Result</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">&nbsp;</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="black12">
                                                                                                                <tbody><tr>
                                                                                                                    <td width="60%" align="left" valign="middle"><select name="result2" class="field-dropdown-E" id="result2" onchange="Show_GpaTextBox(this.id,'result_gpa2');">
                                                                                                                            <option value="0" selected="selected">Select</option>
                                                                                                                            <option value="1">1st Division</option>
                                                                                                                            <option value="2">2nd Division</option>
                                                                                                                            <option value="3">3rd Division</option>
                                                                                                                            <option value="4">GPA/CGPA in scale 4</option>
                                                                                                                            <option value="5">GPA/CGPA in scale 5</option>
                                                                                                                        </select></td>
                                                                                                                    <td width="25%" align="left" valign="middle"><input name="result_gpa2" class="field-G" id="result_gpa2" style="VISIBILITY: hidden" size="6" maxlength="4" onkeypress="return alpha(event,numbers)"></td>
                                                                                                                    <td width="15%" align="left" valign="middle" id="Caption_Marks2">&nbsp;</td>
                                                                                                                </tr>
                                                                                                                </tbody></table></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">Group</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">&nbsp;</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6"><select name="subject2" class="field-dropdown-E" id="subject2">
                                                                                                                <option selected="selected" value="0">Select One</option>
                                                                                                                <option value="1">Science</option><option value="2">Humanities</option><option value="3">Commerce</option><option value="9">Others</option>
                                                                                                            </select></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">Passing Year</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6">&nbsp;</td>
                                                                                                        <td align="left" valign="middle" bgcolor="#B3CBE6"><select name="year2" class="field-dropdown-E" id="year2">
                                                                                                                <option selected="selected" value="0">Select One</option>
                                                                                                                <option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option>
                                                                                                            </select></td>
                                                                                                    </tr>
                                                                                                    </tbody></table></td>
                                                                                        </tr>
                                                                                        </tbody></table></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><table width="100%" border="0" cellpadding="3" cellspacing="0" class="bdr-body">
                                                                                        <tbody><tr>
                                                                                            <td height="20" colspan="3" align="left" valign="middle" bgcolor="#89AED8" class="GrayBlue12">Graduation</td>
                                                                                            <td align="left" valign="middle" bgcolor="#89AED8" class="subblack12">&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="18%" align="left" valign="middle" bgcolor="#B3CBE6">Examination</td>
                                                                                            <td width="37%" align="left" valign="middle" bgcolor="#B3CBE6"><select name="exam3" class="field-dropdown-L" id="exam3" onchange="get_sub_gra(this), Show_ExamTextBox(this.id,'other_exam3'), changeVisibility1(this);">
                                                                                                    <option value="0" selected="selected">Select One</option>
                                                                                                    <option value="1">B.Sc (Engineering/Architecture)</option><option value="2">B.Sc (Agricultural Science)</option><option value="3">M.B.B.S/B.D.S</option><option value="4">Honours</option><option value="5">Pass Course</option><option value="6">A &amp; B Section of A.M.I.E</option><option value="7">BAMS/BHMS/BUMS</option><option value="9">Others</option>
                                                                                                </select>
                                                                                                <input name="other_exam3" type="text" class="field-100" id="other_exam3" style="VISIBILITY: hidden"></td>
                                                                                            <td width="15%" align="left" valign="middle" bgcolor="#B3CBE6">Result</td>
                                                                                            <td width="30%" align="left" valign="middle" bgcolor="#B3CBE6"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="black12">
                                                                                                    <tbody><tr>
                                                                                                        <td width="60%" height="25" align="left" valign="middle"><select name="result3" class="field-dropdown-E" id="result3" onchange="Show_GpaTextBox_G(this.id,'result_gpa3','esd3','eed3');">
                                                                                                                <option value="0" selected="selected">Select One</option>
                                                                                                                <option value="1">1st Class</option>
                                                                                                                <option value="2">2nd Class</option>
                                                                                                                <option value="3">3rd Class</option>
                                                                                                                <option value="4">GPA/CGPA in scale 4</option>
                                                                                                                <option value="5">GPA/CGPA in scale 5</option>
                                                                                                                <option value="7">Pass</option>
                                                                                                                <option value="6">Appeared</option>
                                                                                                                <option value="8">Others</option>
                                                                                                            </select></td>
                                                                                                        <td width="25%" height="25" align="left" valign="middle"><input name="result_gpa3" class="field-G" id="result_gpa3" style="VISIBILITY: hidden" size="6" maxlength="4" onkeypress="return alpha(event,numbers)"></td>
                                                                                                        <td width="15%" align="left" valign="middle" id="Caption_Marks3">&nbsp;</td>
                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                        <td height="25" colspan="3" align="left" valign="middle"><input name="esd3" class="field-Em" id="esd3" placeholder="exam start date" style="VISIBILITY: hidden" onkeypress="return alpha(event,numbers+custom)" size="6" maxlength="20" title="Starting Date of Exam like 20-12-2013">
                                                                                                            <input name="eed3" class="field-Em" id="eed3" placeholder="last date of exam" style="VISIBILITY: hidden" onkeypress="return alpha(event,numbers+custom)" size="6" maxlength="20" title="Last Date of Exam like 25-02-2014"></td>
                                                                                                    </tr>

                                                                                                    </tbody></table></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6">Subject/Degree <img src="images/loader.gif" border="0" align="absmiddle" name="loading1" style="visibility:hidden"></td>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6">
                                                                                                <select name="subject3" class="field-dropdown-L" id="subject3" onchange=" Show_SubTextBox(this.id,'other_subject3');">
                                                                                                    <option selected="selected" value="0">Select One</option>
                                                                                                </select>
                                                                                                <input name="other_subject3" type="text" class="field-100" id="other_subject3" style="VISIBILITY: hidden"></td>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6">Passing Year</td>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6"><select name="year3" class="field-dropdown-E" id="year3">
                                                                                                    <option selected="selected" value="0">Select One</option>
                                                                                                    <option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option>
                                                                                                </select></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6">University/Institute</td>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6">
                                                                                                <select name="institute3" class="field-dropdown-L" id="institute3" onchange="Show_UniTextBox(this.id,'other_institute3');">
                                                                                                    <option value="0" selected="selected">Select One</option>
                                                                                                    <option value="111">Bangabandhu Sheikh Mujib Medical University</option><option value="112">Bangabandhu Sheikh Mujibur Rahman Agricultural University</option><option value="113">Bangladesh Agricultural University,Mymensingh</option><option value="114">Bangladesh Open University</option><option value="115">Bangladesh University of Engineering &amp; Technology</option><option value="116">Bangladesh University of Professionals</option><option value="117">Chittagong University of Engineering &amp; Technology</option><option value="118">Chittagong Veterinary and Animal Sciences University</option><option value="119">Comilla University</option><option value="120">Dhaka University</option><option value="121">Dhaka University of Engineering &amp; Technology</option><option value="122">Hajee Mohammad Danesh Science &amp; Technology University</option><option value="123">Islamic University</option><option value="124">Jagannath University</option><option value="125">Jahangirnagar University</option><option value="126">Jatiya Kabi Kazi Nazrul Islam University</option><option value="127">Jessore Science &amp; Technology University</option><option value="128">Khulna University</option><option value="129">Khulna University of Engineering and Technology</option><option value="130">Mawlana Bhashani Science &amp; Technology University</option><option value="131">National University</option><option value="132">Noakhali Science &amp; Technology University</option><option value="133">Pabna University of Science and Technology</option><option value="134">Patuakhali Science And Technology University</option><option value="135">Rajshahi University</option><option value="136">Rajshahi University of Engineering &amp; Technology</option><option value="137">Rangpur University</option><option value="138">Shahjalal University of Science &amp; Technology</option><option value="139">Sher-e-Bangla Agricultural University</option><option value="140">Sylhet Agricultural University</option><option value="141">University of Chittagong</option><option value="142">Sonargaon University</option><option value="143">Bangladesh University of Textiles</option><option value="144">Islamic Arabic University</option><option value="145">Rabindra University</option><option value="222">Ahsanullah University of Science and Technology</option><option value="223">America Bangladesh University</option><option value="224">American International University Bangladesh</option><option value="225">ASA University Bangladesh</option><option value="226">Asian University of Bangladesh</option><option value="227">Atish Dipankar University of Science &amp; Technology</option><option value="228">Bangladesh Islami University</option><option value="229">Bangladesh University</option><option value="230">Bangladesh University of Business &amp; Technology (BUBT)</option><option value="231">BGC Trust University Bangladesh, Chittagong</option><option value="232">BRAC University</option><option value="233">Central Women's University</option><option value="234">City University</option><option value="235">Daffodil International University</option><option value="237">Dhaka International University</option><option value="238">East Delta University , Chittagong</option><option value="239">East West University</option><option value="240">Eastern University</option><option value="241">Gono Bishwabidyalay</option><option value="242">Green University of Bangladesh</option><option value="243">IBAIS University</option><option value="244">Independent University, Bangladesh</option><option value="245">International Islamic University, Chittagong</option><option value="246">International University of Business Agriculture &amp; Technology</option><option value="247">Leading University, Sylhet</option><option value="248">Manarat International University</option><option value="249">Metropolitan University, Sylhet</option><option value="250">North South University</option><option value="251">Northern University Bangladesh</option><option value="252">Premier University, Chittagong</option><option value="253">Presidency University</option><option value="254">Prime University</option><option value="255">Primeasia University</option><option value="256">Queens University</option><option value="257">Royal University of Dhaka</option><option value="258">Shanto Mariam University of Creative Technology</option><option value="259">Southeast University</option><option value="260">Southern University of Bangladesh , Chittagong</option><option value="261">Stamford University, Bangladesh</option><option value="262">State University Of Bangladesh</option><option value="263">Sylhet International University, Sylhet</option><option value="264">The Millenium University</option><option value="265">The Peoples University of Bangladesh</option><option value="266">The University of Asia Pacific</option><option value="267">United International University</option><option value="268">University of Development Alternative</option><option value="269">University of Information Technology &amp; Sciences</option><option value="270">University of Liberal Arts Bangladesh</option><option value="271">University of Science &amp; Technology, Chittagong</option><option value="272">University of South Asia</option><option value="273">Uttara University</option><option value="274">Victoria University of Bangladesh</option><option value="275">World University of Bangladesh</option><option value="276">European University of Bangladesh</option><option value="277">Britannia University</option><option value="278">Canadian University of Bangladesh</option><option value="279">CCN University of Science &amp; Technology</option><option value="280">Central University of Science and Technology</option><option value="281">Chittagong Independent University</option><option value="282">City University</option><option value="283">Cox's Bazar International University</option><option value="284">Exim Bank Agricultural University, Bangladesh</option><option value="285">Fareast International University</option><option value="286">Feni University</option><option value="287">First Capital University of Bangladesh</option><option value="288">German University Bangladesh</option><option value="289">Global University Bangladesh</option><option value="290">Hamdard University Bangladesh</option><option value="291">International Standard University</option><option value="292">Ishakha International University, Bangladesh</option><option value="293">Khulna Khan Bahadur Ahsanullah University</option><option value="294">Khwaja Yunus Ali University</option><option value="295">Leading University</option><option value="296">Microland University of Science and Technology</option><option value="297">N.P.I University of Bangladesh</option><option value="298">North Bengal International University</option><option value="299">North East University Bangladesh</option><option value="300">North South University</option><option value="301">North Western University</option><option value="302">Northern University of Business &amp; Technology, Khulna</option><option value="303">Notre Dame University Bangladesh</option><option value="304">Port City International University</option><option value="305">Pundra University of Science &amp; Technology</option><option value="306">R.T.M Al-Kobir Technical University</option><option value="307">Rabindra Maitree University, Kushtia</option><option value="308">Rajshahi Science &amp; Technology University (RSTU), Natore</option><option value="309">Ranada Prasad Shaha University</option><option value="310">Rupayan A.K.M Shamsuzzoha University</option><option value="311">Shah Makhdum Management University, Rajshahi</option><option value="312">Sheikh Fazilatunnesa Mujib University</option><option value="313">Sonargaon University</option><option value="314">Tagore University of Creative Arts, Keranigonj, Bangladesh</option><option value="315">The International University of Scholars</option><option value="316">The Millennium University</option><option value="317">The University of Comilla</option><option value="318">Times University, Bangladesh</option><option value="319">Trust University, Barishal</option><option value="320">Ahsania Mission University of Science and Technology</option><option value="321">University of Brahmanbaria</option><option value="322">University of Creative Technology, Chittagong</option><option value="323">University of Global Village</option><option value="324">University of Skill Enrichment and Technology</option><option value="325">University of South Asia</option><option value="326">Uttara University</option><option value="327">Varendra University</option><option value="328">Z.H Sikder University of Science &amp; Technology</option><option value="329">Z.N.R.F. University of Management Sciences</option><option value="330">Bangladesh Army International University of Science &amp; Technology(BAIUST) ,Comilla</option><option value="331">Bangladesh Army University of Engineering and Technology (BAUET), Qadirabad</option><option value="332">Bangladesh Army University of Science and Technology(BAUST), Saidpur</option><option value="333">Asian University for Women</option><option value="334">Islamic University of Technology</option><option value="335">South Asian University</option><option value="336">BGMEA University of Fashion &amp; Technology(BUFT)</option><option value="337">Bandarban University</option><option value="401">Dhaka Medical College</option><option value="402">Sir Salimullah Medical College</option><option value="403">Mymensingh Medical College</option><option value="404">Chittagong Medical College</option><option value="405">Rajshahi Medical College</option><option value="406">MAG Osmani Medical College</option><option value="407">Sher-E-Bangla Medical College</option><option value="408">Rangpur Medical College</option><option value="409">Comilla Medical College</option><option value="410">Khulna Medical College</option><option value="411">Shaheed Ziaur Rahman Medical College</option><option value="412">Dinajpur Medical College</option><option value="413">Faridpur Medical College</option><option value="414">Shaheed Suhrawardy Medical College</option><option value="415">Pabna Medical College</option><option value="416">Noakhali Medical College</option><option value="417">Cox's Bazar Medical College</option><option value="418">Jessore Medical College</option><option value="419">Shaheed Nazrul Islam Medical College</option><option value="420">Kushtia Medical College</option><option value="421">Satkhira Medical College</option><option value="422">Sheikh Sayera Khatun Medical College, Gopalganj</option><option value="501">Feni Medical College,Feni</option><option value="502">Gono Bishwabidyalay, Savar, Dhaka</option><option value="503">Ad-din Womens Medical College, Dhaka</option><option value="504">Anwer Khan Modern Medical College, Dhaka</option><option value="505">Bangladesh Medical College</option><option value="506">Jalalabad Rageb-Rabeya Medical College,Sylhet</option><option value="507">BGC Trust Medical College, Chittagong</option><option value="508">Central Medical College, Comilla</option><option value="509">Chottagram Ma-O-Shishu Hospital Medical College</option><option value="510">Community Based Medical College (cbmc), Mymensingh</option><option value="511">Community Medical College, Dhaka</option><option value="512">Delta Medical College, Dhaka</option><option value="513">Dhaka National Medical College</option><option value="514">Durra Samad Rahman Red Crescent Women's Medical College, Sylhet</option><option value="515">Eastern Medical College, Comilla</option><option value="516">Enam Medical College, Savar, Dhaka</option><option value="517">Sylhet Women`s Medical College, Sylhet</option><option value="518">Green Life Medical College,Dhaka</option><option value="519">Holy Family Red Crescent Medical College, Dhaka</option><option value="520">Ibrahim Medical College, Dhaka</option><option value="521">Ibn Sina Medical College, Dhaka</option><option value="522">International Medical College, Gazipur</option><option value="523">Islami Bank Medical College, Rajshahi</option><option value="524">Jahurul Islam Medical College, Kishoregonj</option><option value="525">Jalalabad Ragib-Rabeya Medical College Sylhet</option><option value="526">Khawja Yunus Ali Medical College, Sirajganj</option><option value="527">Kumudini Medical College, Tangail</option><option value="528">Labaid Medical College[6] Dhanmondi, Dhaka</option><option value="529">Maulana Bhasani Medical College</option><option value="530">Medical College for Women and Hospital, Dhaka</option><option value="531">Nightingale Medical College, Dhaka</option><option value="532">North Bengal Medical College, Sirajganj</option><option value="533">North East Medical College, Sylhet</option><option value="534">Northern International Medical College, Dhaka</option><option value="535">Northern Private Medical College, Rangpur</option><option value="536">Popular Medical College &amp; Hospital, Dhaka</option><option value="537">Prime Medical College, Rangpur</option><option value="538">Rangpur Community Hospital Medical College</option><option value="539">Sahabuddin Medical College and Hospital</option><option value="540">Samaj Vittik Medical College, Mirzanagar, Savar</option><option value="541">Shahabuddin Medical College, Dhaka</option><option value="542">Z. H. Sikder Women`s Medical College</option><option value="543">Southern Medical College, Chittagong</option><option value="544">Tairunnessa Memorial Medical College, Gazipur</option><option value="545">TMSS Medical College,Bogra</option><option value="546">University Of Science and Technology Chittagong. IAMS</option><option value="547">Uttara Adhunik Medical College,Dhaka</option><option value="548">Bangabandhu Sheikh Mujibur  Rahman Science and Technology University</option><option value="549">Begum Rokeya University, Rangpur</option><option value="550">Bangabandhu Sheikh Mujibur Rahman Science &amp; Technology University</option><option value="551">Port City International University</option><option value="552">Sylhet Engineering College</option><option value="553">Mymensingh Engineering College</option><option value="554">Faridpur Engineering College</option><option value="555">Barisal Engineering College</option><option value="556">University of Global Village</option><option value="557">Bangabandhu Sheikh Mujibur Rahman Science &amp; Technology University, Pirojpur</option><option value="999">Others</option>
                                                                                                </select>
                                                                                                <input name="other_institute3" type="text" class="field-100" id="other_institute3" style="VISIBILITY: hidden"></td>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6">Course Duration</td>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6"><select name="duration3" class="field-dropdown-E" id="duration3">
                                                                                                    <option>Select One</option>
                                                                                                    <option value="2">02 Years</option>
                                                                                                    <option value="3">03 Years</option>
                                                                                                    <option value="4">04 Years</option>
                                                                                                    <option value="5">05 Years</option>
                                                                                                </select></td>
                                                                                        </tr>
                                                                                        </tbody></table></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center" valign="middle">&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><table width="100%" border="0" cellpadding="3" cellspacing="0" class="bdr-body">
                                                                                        <tbody><tr>
                                                                                            <td height="20" colspan="3" align="left" valign="middle" bgcolor="#89AED8" class="GrayBlue12">Master's<input name="masters" type="checkbox" id="masters" value="1" onclick="masfd();">
                                                                                                <span class="black11">if applicable</span></td>
                                                                                            <td align="left" valign="middle" bgcolor="#89AED8" class="subblack12">&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="18%" align="left" valign="middle" bgcolor="#B3CBE6">Examination</td>
                                                                                            <td width="37%" align="left" valign="middle" bgcolor="#B3CBE6">
                                                                                                <select name="exam4" class="field-dropdown-L" id="exam4" onchange="Show_ExamTextBox(this.id,'other_exam4');" disabled="">
                                                                                                    <option selected="selected" value="0">Select One</option>
                                                                                                    <option value="1">M.A</option><option value="2">M.S.S</option><option value="3">M.Sc/M.S</option><option value="4">M.Com</option><option value="5">M.B.A</option><option value="6">L.L.M</option><option value="9">Others</option>
                                                                                                </select>
                                                                                                <input name="other_exam4" type="text" class="field-100" id="other_exam4" style="VISIBILITY: hidden"></td>
                                                                                            <td width="15%" align="left" valign="middle" bgcolor="#B3CBE6">Result</td>
                                                                                            <td width="30%" align="left" valign="middle" bgcolor="#B3CBE6"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="black12">
                                                                                                    <tbody><tr>
                                                                                                        <td width="60%" height="25" align="left" valign="middle"><select name="result4" class="field-dropdown-E" id="result4" onchange="Show_GpaTextBox_M(this.id,'result_gpa4','esd4','eed4');" disabled="">
                                                                                                                <option value="0" selected="selected">Select One</option>
                                                                                                                <option value="1">1st Class</option>
                                                                                                                <option value="2">2nd Class</option>
                                                                                                                <option value="3">3rd Class</option>
                                                                                                                <option value="4">GPA/CGPA in scale 4</option>
                                                                                                                <option value="5">GPA/CGPA in scale 5</option>
                                                                                                                <option value="6">Appeared</option>
                                                                                                            </select></td>
                                                                                                        <td width="25%" height="25" align="left" valign="middle"><input name="result_gpa4" class="field-G" id="result_gpa4" style="VISIBILITY: hidden" size="6" maxlength="4" onkeypress="return alpha(event,numbers)"></td>
                                                                                                        <td width="15%" align="left" valign="middle" id="Caption_Marks4">&nbsp;</td>
                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                        <td height="25" colspan="3" align="left" valign="middle"><input name="esd4" class="field-Em" id="esd4" placeholder="exam start date" style="VISIBILITY: hidden" onkeypress="return alpha(event,numbers+custom)" size="6" maxlength="20" title="Starting Date of Exam like 20-06-2014">
                                                                                                            <input name="eed4" class="field-Em" id="eed4" placeholder="last date of exam" style="VISIBILITY: hidden" onkeypress="return alpha(event,numbers+custom); checkdate(this)" size="6" maxlength="20" title="Last Date of Exam like 30-08-2014"></td>
                                                                                                    </tr>

                                                                                                    </tbody></table></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6">Subject</td>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6">
                                                                                                <select name="subject4" class="field-dropdown-L" id="subject4" onchange=" Show_SubTextBox(this.id,'other_subject4');" disabled="">
                                                                                                    <option selected="selected" value="0">Select One</option>
                                                                                                    <option value="101">Accounting [101]</option><option value="102">Anthropology [102]</option><option value="103">Applied Chemistry [103]</option><option value="104">Applied Physics [104]</option><option value="105">Applied Mathematics [105]</option><option value="106">Arabic [106]</option><option value="107">Archaeology [107]</option><option value="108">Bangla [108]</option><option value="109">Banking [109]</option><option value="110">Biochemistry [110]</option><option value="111">Botany [111]</option><option value="112">Business Administration [112]</option><option value="113">Chemistry [113]</option><option value="114">Computer Science [114]</option><option value="115">Clinical Psychology [115]</option><option value="116">Drama &amp; Music [116]</option><option value="117">Development Studies [117]</option><option value="118">Economics [118]</option><option value="119">Education [119]</option><option value="120">English [120]</option><option value="121">Finance [121]</option><option value="122">Fine Arts [122]</option><option value="123">Folklore [123]</option><option value="124">Geography [124]</option><option value="125">Geology [125]</option><option value="126">History [126]</option><option value="127">Home Economics [127]</option><option value="128">Hadith [128]</option><option value="129">International Relations [129]</option><option value="130">Islamic History and Culture [130]</option><option value="131">Islamic Studies [131]</option><option value="132">Information Com. Tech. (ICT) [132]</option><option value="133">Mass Comm. &amp; Journalism [133]</option><option value="134">Law/Jurisprudence [134]</option><option value="135">Library &amp; Information Science [135]</option><option value="136">Language/Linguistic [136]</option><option value="137">Management [137]</option><option value="138">Marketing [138]</option><option value="139">Mathematics [139]</option><option value="140">Microbiology [140]</option><option value="141">Marine Science [141]</option><option value="142">Medical Technology [142]</option><option value="143">Pali [143]</option><option value="144">Persian [144]</option><option value="145">Pharmacy [145]</option><option value="146">Philosophy [146]</option><option value="147">Physics [147]</option><option value="148">Political Science/Govt. and Politics [148]</option><option value="149">Psychology [149]</option><option value="150">Public Administration [150]</option><option value="151">Public Finance [151]</option><option value="152">Population Science [152]</option><option value="153">Peace &amp; Conflict [153]</option><option value="154">Pharmaceutical Chemistry [154]</option><option value="155">Sanskrit [155]</option><option value="156">Social Welfare/Social Work [156]</option><option value="157">Sociology [157]</option><option value="158">Soil Water and Environment Science/Soil Science [158]</option><option value="159">Statistics/Applied Statistics [159]</option><option value="160">Tafsir [160]</option><option value="161">Urdu [161]</option><option value="162">Urban Development [162]</option><option value="163">World Religion [163]</option><option value="164">Women Studies [164]</option><option value="165">Water &amp; Environment Science [165]</option><option value="166">Zoology [166]</option><option value="167">Genetic and Breeding [167]</option><option value="168">International Law [168]</option><option value="169">Akaid [169]</option><option value="170">Graphics [170]</option><option value="171">Fikha [171]</option><option value="172">Modern Arabic [172]</option><option value="173">History of Music [173]</option><option value="174">Drawing and Painting [174]</option><option value="175">Industrial Arts [175]</option><option value="176">Ethics [176]</option><option value="177">Forestry [177]</option><option value="178">Nursery School and Child Development [178]</option><option value="179">Child Development [179]</option><option value="180">Home Management and Housing [180]</option><option value="181">Food &amp; Nutrition/Nutrition Science [181]</option><option value="182">Related Art [182]</option><option value="183">Clothing &amp; Textile [183]</option><option value="184">Institutional Food Management [184]</option><option value="185">Environmental Science [185]</option><option value="186">Demography [186]</option><option value="187">Public Health [187]</option><option value="188">Museology [188]</option><option value="189">Physical Education [189]</option><option value="190">Fashion &amp; Design [190]</option><option value="191">Folk Song [191]</option><option value="192">Classic Song [192]</option><option value="193">Tagore Song [193]</option><option value="194">Nazrul Song [194]</option><option value="195">Instrumental Music [195]</option><option value="196">Geophysics [196]</option><option value="197">Pharmacology [197]</option><option value="198">Wood Science [198]</option><option value="199">Tourism &amp; Hospitality Management [199]</option><option value="201">Agriculture [201]</option><option value="202">Agriculture Chemistry [202]</option><option value="203">Agriculture Co-operatives [203]</option><option value="204">Agriculture Economics [204]</option><option value="205">Agriculture Engineering [205]</option><option value="206">Agriculture Finance [206]</option><option value="207">Agriculture Marketing [207]</option><option value="208">Agriculture Science [208]</option><option value="209">Agriculture Soil Science [209]</option><option value="210">Animal Husbandry [210]</option><option value="211">Agronomy &amp; Aquaculture [211]</option><option value="212">Agronomy &amp; Aquaculture Extension [212]</option><option value="213">Anatomy &amp; Histology [213]</option><option value="214">Agronnomy [214]</option><option value="215">Anatomology [215]</option><option value="216">Animal Breeding &amp; Genetic [216]</option><option value="217">Animal Science [217]</option><option value="218">Animal Nutrition [218]</option><option value="220">Agriculture Water Management [220]</option><option value="221">Agriculture Extension [221]</option><option value="223">Agro Forestry [223]</option><option value="225">Agriculture Statistics [225]</option><option value="226">Agr.Co-operative &amp; Marketing [226]</option><option value="227">Bio-Technology [227]</option><option value="228">Crop Botany [228]</option><option value="229">Diary Science [229]</option><option value="230">Doc.of Veterinary Science [230]</option><option value="231">Fisheries [231]</option><option value="232">Fisheries &amp; Aquaculture [232]</option><option value="233">Fisheries Biology [233]</option><option value="234">Fisheries Management [234]</option><option value="235">Fisheries Technology [235]</option><option value="236">Forestry [236]</option><option value="237">Farm Power &amp; Machinery [237]</option><option value="238">Food Tech. &amp; Rural Industry [238]</option><option value="239">Farm Structure [239]</option><option value="240">Genetic and Breeding [240]</option><option value="241">Horticulture [241]</option><option value="242">Livestock [242]</option><option value="243">Microbiology &amp; Hygienic [243]</option><option value="244">Production Economics [244]</option><option value="245">Plant Pathology [245]</option><option value="246">Paratrology [246]</option><option value="247">Poultry Science [247]</option><option value="248">Rural Sociology [248]</option><option value="249">Surgery &amp; Obstate [249]</option><option value="250">Agriculture Business [250]</option><option value="251">Agriculture Business Administration [251]</option><option value="252">Agriculture Marketing Related Subject [252]</option><option value="301">Architecture [301]</option><option value="302">Chemical [302]</option><option value="303">Civil [303]</option><option value="304">Computer [304]</option><option value="305">Electrical [305]</option><option value="306">Electrical &amp; Electronics [306]</option><option value="307">Electronic [307]</option><option value="308">Genetic Engineering [308]</option><option value="309">Industrial [309]</option><option value="310">Leather Technology [310]</option><option value="311">Marine [311]</option><option value="312">Mechanical [312]</option><option value="313">Metallurgy [313]</option><option value="314">Mineral [314]</option><option value="315">Mining [315]</option><option value="316">Naval Architecture [316]</option><option value="317">Physical Planning [317]</option><option value="318">Regional Planning [318]</option><option value="319">Structural [319]</option><option value="320">Textile Technology [320]</option><option value="321">Town Planning [321]</option><option value="322">Urban Planning [322]</option><option value="323">Tele-Comunication Engineering [323]</option><option value="324">Computer Science &amp; Engineering [324]</option><option value="325">Microwave Engineering [325]</option><option value="326">A &amp; B Section of A.M.I.E [326]</option><option value="327">Maritime Radio Communication (General) [327]</option><option value="328">Radio Electronics [328]</option><option value="329">Power [329]</option><option value="330">Petroleum [330]</option><option value="331">Wood Technology [331]</option><option value="332">Computer Software Engineer [332]</option><option value="333">Genetic Engineering and Biotechnology/Biotechnology and Genetic Engineering [333]</option><option value="334">Industrial Production Engineering [334]</option><option value="335">Electrical &amp; Computer  Engineering [335]</option><option value="336">Building Engineering &amp;  Construction Management [336]</option><option value="337">Food Engineering [337]</option><option value="338">Bio-Medical Engineering       [338]</option><option value="339">Mechatronics  [339]</option><option value="340">Petroleum &amp; Mining Engineering [340]</option><option value="341">Glass &amp; Ceramic Engineering [341]</option><option value="342">Environmental Engineering [342]</option><option value="343">Food Engineering and Technology [343]</option><option value="344">Food Technology and Food Nutritional Science [344]</option><option value="345">Food Science and Engineering [345]</option><option value="346">Chemical &amp; Food Engineering [346]</option><option value="347">Printing Technology [347]</option><option value="348">Robotics and  Mechatronics [348]</option><option value="349">Electronics and Tele Communication Engineering [349]</option><option value="350">B.Sc in Food and Process Engineering [350]</option><option value="351">ECE (Electronics and Communication Engineering) [351]</option><option value="352">Urban and Regional/Rural Planning [352]</option><option value="353">Graphics Design [353]</option><option value="354">Automobile Engineering [354]</option><option value="355">Ceramic [355]</option><option value="356">Glass [356]</option><option value="391">Medicine &amp; Surgery [391]</option><option value="392">Dental Surgery [392]</option><option value="393">Equivalent of Medicine &amp; Surgery [393]</option><option value="394">Equivalent of Dental Surgery [394]</option><option value="401">B.Sc in Nursing/Bachelor of Nursing [401]</option><option value="402">B.Sc in Midwifery/Bachelor of Midwifery [402]</option><option value="501">Bachelor of Homeopathic Medicine &amp; Surgery (BHMS) [501]</option><option value="502">Bachelor of Ayurvedic Medicine &amp; Surgery (BAMS) [502]</option><option value="503">Bachelor of Unani Medicine &amp; Surgery (BUMS) [503]</option><option value="601">Archival Science [601]</option><option value="602">Geography and Environment Science [602]</option><option value="603">Geographic Information System [603]</option><option value="604">Commercial Art [604]</option><option value="605">Health Economics [605]</option><option value="606">Women and Gender Studies [606]</option><option value="607">Nutrition Science [607]</option><option value="608">Geochemistry [608]</option><option value="609">Museologi [609]</option><option value="610">Resource Management &amp; Entrepreneurship [610]</option><option value="611">Organic Chemistry [611]</option><option value="612">Al Quran &amp; Islamic Studies [612]</option><option value="613">Farmacology [613]</option><option value="999">Others [999]</option>
                                                                                                </select>
                                                                                                <input name="other_subject4" type="text" class="field-100" id="other_subject4" style="VISIBILITY: hidden"></td>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6">Passing Year</td>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6">
                                                                                                <select name="year4" class="field-dropdown-E" id="year4" disabled="">
                                                                                                    <option selected="selected" value="0">Select One</option>
                                                                                                    <option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option>
                                                                                                </select></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6">University/Institute</td>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6"><select name="institute4" class="field-dropdown-L" id="institute4" onchange="Show_UniTextBox(this.id,'other_institute4');" disabled="">
                                                                                                    <option value="0" selected="selected">Select One</option>
                                                                                                    <option value="111">Bangabandhu Sheikh Mujib Medical University</option><option value="112">Bangabandhu Sheikh Mujibur Rahman Agricultural University</option><option value="113">Bangladesh Agricultural University,Mymensingh</option><option value="114">Bangladesh Open University</option><option value="115">Bangladesh University of Engineering &amp; Technology</option><option value="116">Bangladesh University of Professionals</option><option value="117">Chittagong University of Engineering &amp; Technology</option><option value="118">Chittagong Veterinary and Animal Sciences University</option><option value="119">Comilla University</option><option value="120">Dhaka University</option><option value="121">Dhaka University of Engineering &amp; Technology</option><option value="122">Hajee Mohammad Danesh Science &amp; Technology University</option><option value="123">Islamic University</option><option value="124">Jagannath University</option><option value="125">Jahangirnagar University</option><option value="126">Jatiya Kabi Kazi Nazrul Islam University</option><option value="127">Jessore Science &amp; Technology University</option><option value="128">Khulna University</option><option value="129">Khulna University of Engineering and Technology</option><option value="130">Mawlana Bhashani Science &amp; Technology University</option><option value="131">National University</option><option value="132">Noakhali Science &amp; Technology University</option><option value="133">Pabna University of Science and Technology</option><option value="134">Patuakhali Science And Technology University</option><option value="135">Rajshahi University</option><option value="136">Rajshahi University of Engineering &amp; Technology</option><option value="137">Rangpur University</option><option value="138">Shahjalal University of Science &amp; Technology</option><option value="139">Sher-e-Bangla Agricultural University</option><option value="140">Sylhet Agricultural University</option><option value="141">University of Chittagong</option><option value="142">Sonargaon University</option><option value="143">Bangladesh University of Textiles</option><option value="144">Islamic Arabic University</option><option value="145">Rabindra University</option><option value="222">Ahsanullah University of Science and Technology</option><option value="223">America Bangladesh University</option><option value="224">American International University Bangladesh</option><option value="225">ASA University Bangladesh</option><option value="226">Asian University of Bangladesh</option><option value="227">Atish Dipankar University of Science &amp; Technology</option><option value="228">Bangladesh Islami University</option><option value="229">Bangladesh University</option><option value="230">Bangladesh University of Business &amp; Technology (BUBT)</option><option value="231">BGC Trust University Bangladesh, Chittagong</option><option value="232">BRAC University</option><option value="233">Central Women's University</option><option value="234">City University</option><option value="235">Daffodil International University</option><option value="237">Dhaka International University</option><option value="238">East Delta University , Chittagong</option><option value="239">East West University</option><option value="240">Eastern University</option><option value="241">Gono Bishwabidyalay</option><option value="242">Green University of Bangladesh</option><option value="243">IBAIS University</option><option value="244">Independent University, Bangladesh</option><option value="245">International Islamic University, Chittagong</option><option value="246">International University of Business Agriculture &amp; Technology</option><option value="247">Leading University, Sylhet</option><option value="248">Manarat International University</option><option value="249">Metropolitan University, Sylhet</option><option value="250">North South University</option><option value="251">Northern University Bangladesh</option><option value="252">Premier University, Chittagong</option><option value="253">Presidency University</option><option value="254">Prime University</option><option value="255">Primeasia University</option><option value="256">Queens University</option><option value="257">Royal University of Dhaka</option><option value="258">Shanto Mariam University of Creative Technology</option><option value="259">Southeast University</option><option value="260">Southern University of Bangladesh , Chittagong</option><option value="261">Stamford University, Bangladesh</option><option value="262">State University Of Bangladesh</option><option value="263">Sylhet International University, Sylhet</option><option value="264">The Millenium University</option><option value="265">The Peoples University of Bangladesh</option><option value="266">The University of Asia Pacific</option><option value="267">United International University</option><option value="268">University of Development Alternative</option><option value="269">University of Information Technology &amp; Sciences</option><option value="270">University of Liberal Arts Bangladesh</option><option value="271">University of Science &amp; Technology, Chittagong</option><option value="272">University of South Asia</option><option value="273">Uttara University</option><option value="274">Victoria University of Bangladesh</option><option value="275">World University of Bangladesh</option><option value="276">European University of Bangladesh</option><option value="277">Britannia University</option><option value="278">Canadian University of Bangladesh</option><option value="279">CCN University of Science &amp; Technology</option><option value="280">Central University of Science and Technology</option><option value="281">Chittagong Independent University</option><option value="282">City University</option><option value="283">Cox's Bazar International University</option><option value="284">Exim Bank Agricultural University, Bangladesh</option><option value="285">Fareast International University</option><option value="286">Feni University</option><option value="287">First Capital University of Bangladesh</option><option value="288">German University Bangladesh</option><option value="289">Global University Bangladesh</option><option value="290">Hamdard University Bangladesh</option><option value="291">International Standard University</option><option value="292">Ishakha International University, Bangladesh</option><option value="293">Khulna Khan Bahadur Ahsanullah University</option><option value="294">Khwaja Yunus Ali University</option><option value="295">Leading University</option><option value="296">Microland University of Science and Technology</option><option value="297">N.P.I University of Bangladesh</option><option value="298">North Bengal International University</option><option value="299">North East University Bangladesh</option><option value="300">North South University</option><option value="301">North Western University</option><option value="302">Northern University of Business &amp; Technology, Khulna</option><option value="303">Notre Dame University Bangladesh</option><option value="304">Port City International University</option><option value="305">Pundra University of Science &amp; Technology</option><option value="306">R.T.M Al-Kobir Technical University</option><option value="307">Rabindra Maitree University, Kushtia</option><option value="308">Rajshahi Science &amp; Technology University (RSTU), Natore</option><option value="309">Ranada Prasad Shaha University</option><option value="310">Rupayan A.K.M Shamsuzzoha University</option><option value="311">Shah Makhdum Management University, Rajshahi</option><option value="312">Sheikh Fazilatunnesa Mujib University</option><option value="313">Sonargaon University</option><option value="314">Tagore University of Creative Arts, Keranigonj, Bangladesh</option><option value="315">The International University of Scholars</option><option value="316">The Millennium University</option><option value="317">The University of Comilla</option><option value="318">Times University, Bangladesh</option><option value="319">Trust University, Barishal</option><option value="320">Ahsania Mission University of Science and Technology</option><option value="321">University of Brahmanbaria</option><option value="322">University of Creative Technology, Chittagong</option><option value="323">University of Global Village</option><option value="324">University of Skill Enrichment and Technology</option><option value="325">University of South Asia</option><option value="326">Uttara University</option><option value="327">Varendra University</option><option value="328">Z.H Sikder University of Science &amp; Technology</option><option value="329">Z.N.R.F. University of Management Sciences</option><option value="330">Bangladesh Army International University of Science &amp; Technology(BAIUST) ,Comilla</option><option value="331">Bangladesh Army University of Engineering and Technology (BAUET), Qadirabad</option><option value="332">Bangladesh Army University of Science and Technology(BAUST), Saidpur</option><option value="333">Asian University for Women</option><option value="334">Islamic University of Technology</option><option value="335">South Asian University</option><option value="336">BGMEA University of Fashion &amp; Technology(BUFT)</option><option value="337">Bandarban University</option><option value="401">Dhaka Medical College</option><option value="402">Sir Salimullah Medical College</option><option value="403">Mymensingh Medical College</option><option value="404">Chittagong Medical College</option><option value="405">Rajshahi Medical College</option><option value="406">MAG Osmani Medical College</option><option value="407">Sher-E-Bangla Medical College</option><option value="408">Rangpur Medical College</option><option value="409">Comilla Medical College</option><option value="410">Khulna Medical College</option><option value="411">Shaheed Ziaur Rahman Medical College</option><option value="412">Dinajpur Medical College</option><option value="413">Faridpur Medical College</option><option value="414">Shaheed Suhrawardy Medical College</option><option value="415">Pabna Medical College</option><option value="416">Noakhali Medical College</option><option value="417">Cox's Bazar Medical College</option><option value="418">Jessore Medical College</option><option value="419">Shaheed Nazrul Islam Medical College</option><option value="420">Kushtia Medical College</option><option value="421">Satkhira Medical College</option><option value="422">Sheikh Sayera Khatun Medical College, Gopalganj</option><option value="501">Feni Medical College,Feni</option><option value="502">Gono Bishwabidyalay, Savar, Dhaka</option><option value="503">Ad-din Womens Medical College, Dhaka</option><option value="504">Anwer Khan Modern Medical College, Dhaka</option><option value="505">Bangladesh Medical College</option><option value="506">Jalalabad Rageb-Rabeya Medical College,Sylhet</option><option value="507">BGC Trust Medical College, Chittagong</option><option value="508">Central Medical College, Comilla</option><option value="509">Chottagram Ma-O-Shishu Hospital Medical College</option><option value="510">Community Based Medical College (cbmc), Mymensingh</option><option value="511">Community Medical College, Dhaka</option><option value="512">Delta Medical College, Dhaka</option><option value="513">Dhaka National Medical College</option><option value="514">Durra Samad Rahman Red Crescent Women's Medical College, Sylhet</option><option value="515">Eastern Medical College, Comilla</option><option value="516">Enam Medical College, Savar, Dhaka</option><option value="517">Sylhet Women`s Medical College, Sylhet</option><option value="518">Green Life Medical College,Dhaka</option><option value="519">Holy Family Red Crescent Medical College, Dhaka</option><option value="520">Ibrahim Medical College, Dhaka</option><option value="521">Ibn Sina Medical College, Dhaka</option><option value="522">International Medical College, Gazipur</option><option value="523">Islami Bank Medical College, Rajshahi</option><option value="524">Jahurul Islam Medical College, Kishoregonj</option><option value="525">Jalalabad Ragib-Rabeya Medical College Sylhet</option><option value="526">Khawja Yunus Ali Medical College, Sirajganj</option><option value="527">Kumudini Medical College, Tangail</option><option value="528">Labaid Medical College[6] Dhanmondi, Dhaka</option><option value="529">Maulana Bhasani Medical College</option><option value="530">Medical College for Women and Hospital, Dhaka</option><option value="531">Nightingale Medical College, Dhaka</option><option value="532">North Bengal Medical College, Sirajganj</option><option value="533">North East Medical College, Sylhet</option><option value="534">Northern International Medical College, Dhaka</option><option value="535">Northern Private Medical College, Rangpur</option><option value="536">Popular Medical College &amp; Hospital, Dhaka</option><option value="537">Prime Medical College, Rangpur</option><option value="538">Rangpur Community Hospital Medical College</option><option value="539">Sahabuddin Medical College and Hospital</option><option value="540">Samaj Vittik Medical College, Mirzanagar, Savar</option><option value="541">Shahabuddin Medical College, Dhaka</option><option value="542">Z. H. Sikder Women`s Medical College</option><option value="543">Southern Medical College, Chittagong</option><option value="544">Tairunnessa Memorial Medical College, Gazipur</option><option value="545">TMSS Medical College,Bogra</option><option value="546">University Of Science and Technology Chittagong. IAMS</option><option value="547">Uttara Adhunik Medical College,Dhaka</option><option value="548">Bangabandhu Sheikh Mujibur  Rahman Science and Technology University</option><option value="549">Begum Rokeya University, Rangpur</option><option value="550">Bangabandhu Sheikh Mujibur Rahman Science &amp; Technology University</option><option value="551">Port City International University</option><option value="552">Sylhet Engineering College</option><option value="553">Mymensingh Engineering College</option><option value="554">Faridpur Engineering College</option><option value="555">Barisal Engineering College</option><option value="556">University of Global Village</option><option value="557">Bangabandhu Sheikh Mujibur Rahman Science &amp; Technology University, Pirojpur</option><option value="999">Others</option>
                                                                                                </select>
                                                                                                <input name="other_institute4" type="text" class="field-100" id="other_institute4" style="VISIBILITY: hidden"></td>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6">Course Duration</td>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6">
                                                                                                <select name="duration4" class="field-dropdown-E" id="duration4" disabled="">
                                                                                                    <option>Select One</option>
                                                                                                    <option value="1">01 Year</option>
                                                                                                    <option value="2">02 Years</option>
                                                                                                    <option value="3">03 Years</option>
                                                                                                    <option value="4">04 Years</option>
                                                                                                    <option value="5">05 Years</option>
                                                                                                </select></td>
                                                                                        </tr>
                                                                                        </tbody></table></td>
                                                                            </tr>

                                                                            <tr>
                                                                                <td align="center" valign="middle">&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><table width="100%" border="0" cellpadding="3" cellspacing="0" class="bdr-body">
                                                                                        <tbody><tr>
                                                                                            <td height="20" colspan="3" align="left" valign="middle" bgcolor="#89AED8" class="GrayBlue12">
                                                                                                Additional Qualification for Teachers' Training College <input name="tt_level" type="checkbox" id="tt_level" value="1" onclick="ttlfd();">
                                                                                                <span class="black11">if applicable</span></td>
                                                                                            <td align="left" valign="middle" bgcolor="#89AED8" class="subblack12">&nbsp;</td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td width="18%" align="left" valign="middle" bgcolor="#B3CBE6">Examination</td>
                                                                                            <td width="37%" align="left" valign="middle" bgcolor="#B3CBE6">
                                                                                                <select name="exam5" class="field-dropdown-L" id="exam5" disabled="">
                                                                                                    <option selected="selected" value="0">Select One</option>
                                                                                                    <option value="1">Diploma of Education</option>
                                                                                                    <option value="2">Bachelor of Education (B.Ed)</option>
                                                                                                    <option value="3">Master's of Education (M.Ed)</option>
                                                                                                    <option value="4">Diploma in Technical Education</option>
                                                                                                    <option value="5">B.Sc in Technical Education</option>
                                                                                                </select>
                                                                                            </td>
                                                                                            <td width="15%" align="left" valign="middle" bgcolor="#B3CBE6">Result</td>
                                                                                            <td width="30%" align="left" valign="middle" bgcolor="#B3CBE6"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="black12">
                                                                                                    <tbody><tr>
                                                                                                        <td width="60%" height="25" align="left" valign="middle">
                                                                                                            <select name="result5" class="field-dropdown-E" id="result5" disabled="">
                                                                                                                <option value="" selected="selected">Select One</option>
                                                                                                                <option value="1">Pass</option>
                                                                                                            </select></td>
                                                                                                        <td width="25%" height="25" align="left" valign="middle">&nbsp;</td>
                                                                                                        <td width="15%" align="left" valign="middle">&nbsp;</td>
                                                                                                    </tr>
                                                                                                    </tbody></table></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6">University/Institute</td>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6">
                                                                                                <input name="institute5" type="text" class="field-100" id="institute5" disabled="">
                                                                                            </td>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6">Passing Year</td>
                                                                                            <td align="left" valign="middle" bgcolor="#B3CBE6">
                                                                                                <select name="year5" class="field-dropdown-E" id="year5" disabled="">
                                                                                                    <option selected="selected" value="0">Select One</option>
                                                                                                    <option value="2002">2002</option><option value="2003">2003</option><option value="2004">2004</option><option value="2005">2005</option><option value="2006">2006</option><option value="2007">2007</option><option value="2008">2008</option><option value="2009">2009</option><option value="2010">2010</option><option value="2011">2011</option><option value="2012">2012</option><option value="2013">2013</option><option value="2014">2014</option><option value="2015">2015</option><option value="2016">2016</option><option value="2017">2017</option><option value="2018">2018</option><option value="2019">2019</option><option value="2020">2020</option><option value="2021">2021</option><option value="2022">2022</option>
                                                                                                </select></td>
                                                                                        </tr>
                                                                                        </tbody></table></td>
                                                                            </tr>


                                                                            <tr>
                                                                                <td height="30" align="center" valign="middle" class="black14"><input type="checkbox" name="info_yes" id="info_yes" onclick="agreesubmit(this)">
                                                                                    The above information is correct and I would like to go to the next step.</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center" valign="middle">
                                                                                    <input type="button" name="pi" id="button01" value="Previous" onclick="window.location.href='afpie.php?t=NZVW3QT7Z89Q5PYENRG1-435'">
                                                                                    <input type="submit" name="next" id="button01" value="Next" disabled=""></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height="40" align="center" valign="middle"><img name="im" src="images/blank.png"></td>
                                                                            </tr>
                                                                            </tbody></table></td>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody></table></td>
                                                <td class="main-right">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td><img src="images/main_07.png" width="19" height="19"></td>
                                                <td class="main-button">&nbsp;</td>
                                                <td><img src="images/main_09.png" width="20" height="19"></td>
                                            </tr>
                                            </tbody></table>
                                    </form></td>
                            </tr>
                            </tbody></table>
                        <!-- form close -->
                    </div>
                    <!-- /.card -->

                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@stop
@section('script')
    <script src="{{ asset('assets/custom-form/imageup.js') }}"></script>
    <script src="{{ asset('assets/custom-form/valid_step_04.js') }}"></script>
    <script src="{{ asset('assets/custom-form/form_submit.js') }}"></script>
    <script src="{{ asset('assets/custom-form/popup.js') }}"></script>
    <script>
        $(document).on('change', '.session', function () {
            var id= $(this).val();
            console.log(id);
            $.ajax({
                url: '{{url("get-ClassSectionBySession/")}}'+id,
                type: 'GET',
                success:function (data) {
                    console.log(data);
                    if (data.length>0){
                        var $select_class =  $('.class');
                        var html = '<option disabled selected> Select Class-Section</option>';
                        $.each(data, function (idx, item) {
                            html += '<option value="' + item.id + '" data-id="'+item.section_id+'" >' + item.name + ' [ Sec- '+item.section+' ]</option>';
                        });
                        $select_class.html(html);
                    }
                }
            });
        });

        $(document).on('change', '.class', function () {
            //var id = $(this).val();
            var section_id = $(this).attr("data-id");
            console.log(section_id);
            $('.section').html(section_id);
        });

        $(document).on('keyup','#rank', function () {
            alert();
            var academicYear = $('.year').val();
            $.ajax({
                url:"{{url('admin/load_student_id')}}",
                type:'GET',
                data:{academicYear:academicYear},
                success:function (data) {
                    console.log(data);
                    $('#studentID').val(data);

                }
            });
        });
    </script>
@stop
