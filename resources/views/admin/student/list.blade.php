@extends('layouts.fixed')

@section('title','Student List')
@section('style')
    <style>
        .form-group.required .control-label:after {
            content:"*";
            color:red;
        }
    </style>
@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Student</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">All Students</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- /.Search-panel -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card" style="margin: 10px;">
                        <!-- form start -->
                        {{ Form::open(['action'=>'Backend\StudentController@index','role'=>'form','method'=>'get']) }}
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col">
                                    <label for="">Student ID</label>
                                    <div class="input-group">
                                        {{ Form::text('studentId',null,['class'=>'form-control','placeholder'=>'Student ID']) }}
                                    </div>
                                </div>
                                <div class="col">
                                    <label for="">Name</label>
                                    <div class="input-group">
                                        {{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Name']) }}
                                    </div>
                                </div>
                                <div class="col">
                                    <label for="">Class</label>
                                    <div class="input-group">
                                        <select name="academic_class_id" class="form-control ">
                                            <option value=""> Select </option>
                                            @foreach($repository->academicClasses() as $class)
                                                <option value="{{ $class->id }}">
                                                    {{ $class->academicClasses->name ?? '' }}
                                                    {{ $class->section->name ?? '' }}
                                                    {{ $class->group->name ?? '' }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                {{--                                <div class="col">--}}
                                {{--                                    <label for="">Session</label>--}}
                                {{--                                    <div class="input-group">--}}
                                {{--                                        {{ Form::select('session_id',$repository->sessions(),null,['class'=>'form-control','placeholder'=>'Select Session']) }}--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                                {{--                                <div class="col">--}}
                                {{--                                    <label for="">Class</label>--}}
                                {{--                                    <div class="input-group">--}}
                                {{--                                        {{ Form::select('class_id',$repository->classes(),null,['class'=>'form-control','placeholder'=>'Select Class']) }}--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                                {{--                                <div class="col">--}}
                                {{--                                    <label for="">Section</label>--}}
                                {{--                                    <div class="input-group">--}}
                                {{--                                        {{ Form::select('section_id',$repository->sections(),null,['class'=>'form-control','placeholder'=>'Select Section']) }}--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                                {{--                                <div class="col">--}}
                                {{--                                    <label for="">Group</label>--}}
                                {{--                                    <div class="input-group">--}}
                                {{--                                        {{ Form::select('group_id',$repository->groups(),null,['class'=>'form-control','placeholder'=>'Select Group']) }}--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}

                                <div class="col-1" style="padding-top: 32px;">
                                    <div class="input-group">
                                        <button  style="padding: 6px 20px;" type="submit" class="btn btn-primary"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>
    <!-- /.Search-panel -->


    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card card-info">
                    <div class="card-header">
                        {{--                        <h3 class="card-title">Total Found : {{ $students->total() }}</h3>--}}
                        <div class="float-right">
                            <button type="button" class="btn btn-outline-warning text-light text-bold btn-sm" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" ><i class="fas fa-plus-circle"></i> Quick Add</button>
                            <a href="{{ route('student.examform') }}" class="btn btn-success btn-sm" ><i class="fas fa-plus-circle"></i> BE Form</a>
                            <a href="{{ route('student.add') }}" class="btn btn-secondary btn-sm" ><i class="fas fa-plus-circle"></i> New</a>
                            <a href="{{ route('csv') }}" target="_blank" class="btn btn-primary btn-sm"><i class="fas fa-cloud-download-alt"></i> CSV</a>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped table-sm">
                            <thead class="thead-dark">
                            <tr>
                                <th>Id</th>
                                <th>Student</th>
                                <th>Mobile</th>
                                <th>Father/Mother</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($students as $student)
                                <tr>
                                    <td>{{ $student->studentId }}</td>
                                    <td>{{ $student->name }}
                                        @if($student->status == 2)
                                            <span class="badge badge-danger">Drop Out</span>
                                        @endif
                                    </td>
                                    <td> {{ $student->mobile }}</td>
                                    <td>    {{ $student->father ? $student->father->f_name : ''}} ||<br>
                                        {{ $student->mother ? $student->mother->m_name : ''}}


                                    </td>
                                    <td><img src="{{ asset('storage/uploads/students/') }}/{{ $student->image }}" height="100" alt=""></td>
                                    <td>
                                        <a href="{{ action('Backend\StudentController@studentProfile',$student->id) }}" role="button" class="btn btn-success btn-sm"><i class="fas fa-eye"></i></a>
                                        <a href="{{ action('Backend\StudentController@edit',$student->id) }}" role="button" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                                        <a href="{{ action('Backend\StudentController@subjects',$student->id) }}" role="button" class="btn btn-info btn-sm"><i class="fas fa-book"></i></a>
                                        <a href="{{ action('Backend\StudentController@dropOut',$student->id) }}" role="button" class="btn btn-danger btn-sm" title="DROPOUT"><i class="fas fa-sign-out-alt"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-body">
                        {{ $students->appends(Request::except('page'))->links() }}
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <!-- ***/ Pop Up Model for button -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Student </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    {!! Form::open(['action'=>'Backend\StudentController@store', 'method'=>'post']) !!}

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label "> <i class="text-danger">*</i> Name (English): </label>
                        <input type="text" name="name" class="form-control" required id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label "><i class="text-danger">*</i>Mobile:</label>
                        <input type="text" name="mobile" class="form-control" id="recipient-name" required>
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label "><i class="text-danger">*</i>Academic Class:</label>
                        <select name="academic_class_id" class="form-control " required id="">
                            <option value="">select</option>
                            @foreach ($academicClass as $cls)
                                <option value="{{ $cls->id }}">
                                    {{ $cls->classes->name ?? '' }} {{ $cls->section->name ?? '' }} {{ $cls->group->name ?? '' }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label "><i class="text-danger">*</i>studentId:</label>
                        <input type="text" name="studentId" class="form-control" required id="studentId">
                    </div>
                    <input type="hidden" name="status" value="1">

                    <div  class="float-right">
                        <button type="submit" class="btn btn-success  btn-sm" > <i class="fas fa-plus-circle"></i> Add</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- ***/ Pop Up Model for button End-->
@stop

@section('plugin')
@stop
@section('script')

    <script>
        $(document).on('keyup','#rank', function () {
            var academicYear = $('#getAcademicYear').val();
            console.log('form', academicYear);

            $.ajax({
                url:"{{url('admin/load_student_id')}}",
                type:'GET',
                data:{academicYear:academicYear},
                success:function (data) {
                    console.log(data);
                    $('#studentID').val(data);
                }
            });
        });
    </script>
@stop
