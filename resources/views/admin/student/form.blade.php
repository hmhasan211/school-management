@section('style')
    <style>
        .fancy-forms .tab-content{
            background: #ffffff;
            /* color: #ffffff; */
            padding: 10px;
            /* box-shadow: 8px 12px 25px 2px rgba(0,0,0,0.3); */
        }

        .fancy-forms .nav-tabs .nav-item{
            /* width: 50%; */
            text-align: center;
        }

        .fancy-forms .nav-tabs .nav-link{

            border: 1px solid #047afc;
            background-color: #047afc;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            color: #ffffff;
            box-shadow: 2px 0px 7px 0px rgb(0 0 0 / 30%);
            border-left: 1px solid;
        }

        .fancy-forms .nav-tabs .nav-link.active{
            border-color: #fff;
            color: #047afc;
            background-color: #ffffff;
        }

        .fancy-forms .nav-tabs .nav-link:hover{
            border-color: #fff;
        }

        fancy-forms .nav-tabs .nav-link.active:hover{
            border-color: #047afc;
        }

        .fancyformcontainer{
            background: #e6c3b4;
            padding: .5rem 3rem !important;
            margin: 3rem !important;
        }

        .formsubmitbtn{
            background: #e47a4b;
            color: white;
            margin-bottom: 1.5rem !important;
        }

        .formsubmitbtn:hover,.formsubmitbtn:focus{
            color: #fff;
        }

        /* for image upload  */
        .drop-zone {
            max-width: 300px;
            height: 300px;
            padding: 25px;
            display: flex;
            align-items: center;
            justify-content: center;
            text-align: center;
            font-family: "Quicksand", sans-serif;
            font-weight: 500;
            font-size: 20px;
            cursor: pointer;
            color: #cccccc;
            border: 4px dashed #009578;
            border-radius: 10px;
        }

        .drop-zone--over {
            border-style: solid;
        }

        .drop-zone__input {
            display: none;
        }

        .drop-zone__thumb {
            width: 100%;
            height: 100%;
            border-radius: 10px;
            overflow: hidden;
            background-color: #cccccc;
            background-size: cover;
            position: relative;
        }

        .drop-zone__thumb::after {
            content: attr(data-label);
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
            padding: 5px 0;
            color: #ffffff;
            background: rgba(0, 0, 0, 0.75);
            font-size: 14px;
            text-align: center;
        }

        .cutom_shadow{
            box-shadow: rgb(100 100 111 / 26%) 0px 7px 29px 0px;
        }
        option.customOption {
            /* background: red; */
            color: #000;
            font-weight: 900;
        }

    </style>
@endsection
<!-- MultiStep Form -->
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-3 fancy-forms">
            <ul class="nav nav-tabs  mt-3" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="login" data-toggle="tab" href="#login_form" role="tab" aria-controls="login" aria-selected="true">Student Info</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="acadimic" data-toggle="tab" href="#acadimic_form" role="tab" aria-controls="acadimic" aria-selected="true">Academics Info</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" id="personal" data-toggle="tab" href="#personal_form" role="tab" aria-controls="personal" aria-selected="true">Personal Info</a>
                </li>
            </ul>

            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="login_form" role="tabpanel" aria-labelledby="login">
                    <h3 class="text-center p-2 text-bold">Student Information</h3>
                    @isset($father)
                        <input type="hidden" name="f_id" value="{{ $father->id }}">
                        <input type="hidden" name="m_id" value="{{ $mother->id }}">
                        <input type="hidden" name="g_id" value="{{ $guardian->id }}">
                    @endisset
                    <div class="row">
                        <div class="col-8">
                            <div class="row">
                                <div class="form-group col-6">
                                    {{Form::label('name','Name (English)',['class'=>'control-label'])}}<i class="text-danger">*</i>
                                    {{ Form::text('name', null, ['placeholder' => 'Student\'s  Name English..', 'class' => 'form-control' ]) }}
                                    @error('name')
                                    <b style="color: red">{{ $message }}</b>
                                    @enderror
                                </div>
                                <div class="form-group col-6">
                                    {{Form::label('name',' Name (Bangla)',['class'=>'control-label'])}}<i class="text-danger">*</i>
                                    {{ Form::text('name_bn', null, ['placeholder' => 'Student\'s  Name Bangla...', 'class' => 'form-control' ]) }}
                                    @error('name_bn')
                                    <b style="color: red">{{ $message }}</b>
                                    @enderror
                                </div>
                                <div class="form-group col-12">
                                    {{Form::label('name','Student\'s Birth Registration No',['class'=>'control-label'])}}<i class="text-danger">*</i>
                                    {{ Form::number('birth_certificate', null, ['placeholder' => 'Student\'s  Birth Reg no...', 'class' => 'form-control' ]) }}
                                    @error('birth_certificate')
                                    <b style="color: red">{{ $message }}</b>
                                    @enderror
                                </div>

                                <div class="form-group col-6">
                                    {{ Form::label('dob','Date of Birth',['class'=>'control-label']) }}<i class="text-danger">*</i>
                                    {{ Form::date('dob',null,['class' => 'form-control', 'placeholder'=>'Date of Birth']) }}
                                    @error('dob')
                                    <b style="color: red">{{ $message }}</b>
                                    @enderror
                                </div>

                                <div class="form-group col-6">
                                    {{ Form::label('gender','Gender',['class'=>'control-label']) }}<i class="text-danger text-bold">*</i>
                                    {{ Form::select('gender_id', $repository->genders(), null, ['class'=>'form-control','placeholder' => 'Select Gender...']) }}
                                    @error('gender_id')
                                    <b style="color: red">{{ $message }}</b>
                                    @enderror
                                </div>
                                {{-- for 2nd section --}}

                                <div class="form-group col-6">
                                    {{ Form::label('bloodGroup','Blood Group',['class'=>'control-label']) }}<i class="text-danger text-bold">*</i>
                                    {{ Form::select('blood_group_id', $repository->bloods(), null, ['placeholder' => 'Select Blood Group...','class'=>'form-control']) }}
                                    @error('blood_group_id')
                                    <b style="color: red">{{ $message }}</b>
                                    @enderror
                                </div>
                                <div class="form-group col-6">
                                    {{ Form::label('religion_id','Religion',['class'=>'control-label']) }}<i class="text-danger text-bold">*</i>
                                    {{ Form::select('religion_id', $repository->religions(), null, ['placeholder' => 'Select Blood Group...','class'=>'form-control']) }}
                                    @error('religion_id')
                                    <b style="color: red">{{ $message }}</b>
                                    @enderror
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        {{ Form::label('freedom_fighter','Freedom Fighter',['class'=>'control-label']) }}
                                        {{ Form::radio('freedom_fighter', 1, false, ['id'=>'active']) }}&nbsp;{{ Form::label('active','Yes') }}
                                        {{ Form::radio('freedom_fighter', 0, true , ['id'=>'inactive']) }}&nbsp;{{ Form::label('inactive','No') }}
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        {{ Form::label('is_tribal','Tribal',['class'=>'control-label']) }}
                                        {{ Form::radio('is_tribal', 1, false, ['id'=>'active']) }}&nbsp;{{ Form::label('active','Yes') }}
                                        {{ Form::radio('is_tribal', 0, true, ['id'=>'inactive']) }}&nbsp;{{ Form::label('inactive','No') }}
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        {{ Form::label('disability','Disability',['class'=>'control-label']) }}
                                        {{ Form::radio('disability', 1, false, ['id'=>'active']) }}&nbsp;{{ Form::label('active','Yes') }}
                                        {{ Form::radio('disability', 0, true, ['id'=>'inactive']) }}&nbsp;{{ Form::label('inactive','No') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="row">

                                <div class="col-12">
                                    <label for="">Student Picture</label>
                                    <div class="drop-zone">
                                        <span class="drop-zone__prompt">Drop file here or click to upload</span>
                                        {{-- <input type="file" name="myFile" class="drop-zone__input"> --}}
                                        {{ Form::file('pic',['class'=>'drop-zone__input', 'id'=>"file-input"]) }}
                                        <p></p>
                                        @error('pic')
                                        <b style="color: red">{{ $message }}</b>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 text-center mt-2">
                                    <div id="editImage" class="editImageShow">
                                        @if(\Route::current()->getName() != 'student.add')
                                            {!! Form::image('/storage/uploads/students/'.$student->image, 'Image Button',['class' => 'reset-now', 'width' => '200px', 'height' => '200px']) !!}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <h3 class=" m-3 text-center text-bold ">Address & Contact Information</h3>
                            <div class="row">
                                <div class="col-6">
                                    <div class="row">
                                        <div class="form-group col-12">
                                            <h5 class="bg-info text-center" >Present Address </h5>
                                            {{ Form::label('streetAddress','Address',['class'=>'control-label']) }}  <i class="text-danger text-bold">*</i>
                                            {{ Form::textarea('address',null,['class'=>'form-control','id'=>'preAdd', 'rows'=>1, 'placeholder'=>'Address']) }}
                                            @error('address')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>
                                        <div class="form-group col-12">
                                            {{ Form::label('area','Area / Town',['class'=>'control-label']) }}<i class="text-danger text-bold">*</i>
                                            {{ Form::text('area',null,['class'=>'form-control', 'id'=>'preArea', 'placeholder'=>'Area Town']) }}
                                            @error('area')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>
                                        <div class="form-group col-6">
                                            {{ Form::label('postCode','Post / Zip Code',['class'=>'control-label']) }}
                                            {{ Form::text('zip',null,['class'=>'form-control', 'id'=>'preZip', 'placeholder'=>'Post / Zip Code']) }}
                                            @error('zip')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>
                                        <div class="form-group col-6">
                                            {{ Form::label('city_id','City',['class'=>'control-label']) }}
                                            {{ Form::select('city_id',$repository->cities(), null, ['placeholder' => 'Select City', 'id'=>'preCity', 'class'=>'form-control']) }}
                                            @error('city_id')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>
                                        <div class="form-group col-6">
                                            {{ Form::label('country','Country',['class'=>'control-label']) }}
                                            {{ Form::select('country_id', $repository->countries(), null, ['placeholder' => 'Select Country...','id'=>'preCountry','class'=>'form-control']) }}
                                            @error('country_id')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>
                                        <div class="form-group col-6">
                                            {{ Form::label('nationality','Nationality',['class'=>'control-label']) }}
                                            {{ Form::text('nationality',null,['class'=>'form-control', 'id'=>'preNatism','placeholder'=>'Nationality']) }}
                                            @error('nationality')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                @php
                                    if (isset($student)){
                                        $p_address=json_decode($student->p_address);
                                        }
                                @endphp
                                <div class="col-6">
                                    <div class="row">
                                        <div class="form-group col-12">
                                            <h5 class="bg-info text-center" >Permanent Address </h5>
                                            <small class="float-right mr-4 text-danger">Same as present
                                                <input type="checkbox" id="fillParment" name="fillParment"/>     </small>
                                            {{ Form::label('streetAddress','Address',['class'=>'control-label']) }}  <i class="text-danger text-bold">*</i>
                                            {{ Form::textarea('p_address[address]', isset($p_address) ?  $p_address->address : null,['class'=>'form-control', 'id'=>'parAdd', 'rows'=>1, 'placeholder'=>'Address']) }}

                                        </div>
                                        <div class="form-group col-12">
                                            {{ Form::label('area','Area / Town',['class'=>'control-label']) }}<i class="text-danger text-bold">*</i>
                                            {{ Form::text('p_address[area]', isset($p_address) ?  $p_address->area : null,['class'=>'form-control', 'id'=>'parArea', 'placeholder'=>'Area Town']) }}

                                        </div>
                                        <div class="form-group col-6">
                                            {{ Form::label('postCode','Post / Zip Code',['class'=>'control-label']) }}
                                            {{ Form::text('p_address[zip]',isset($p_address) ?  $p_address->zip : null,['class'=>'form-control','id'=>'parZip','placeholder'=>'Post / Zip Code']) }}

                                        </div>
                                        <div class="form-group col-6">
                                            {{ Form::label('city_id','City',['class'=>'control-label']) }}
                                            {{ Form::select('p_address[city_id]',$repository->cities(), isset($p_address) ?  $p_address->city_id : null, ['placeholder' => 'Select City', 'id'=>'parCity','class'=>'form-control']) }}

                                        </div>
                                        <div class="form-group col-6">
                                            {{ Form::label('country','Country',['class'=>'control-label']) }}
                                            {{ Form::select('p_address[country_id]',$repository->countries(), isset($p_address) ?  $p_address->country_id : null, ['placeholder' => 'Select Country...','id'=>'parCountry', 'class'=>'form-control']) }}

                                        </div>
                                        <div class="form-group col-6">
                                            {{ Form::label('nationality','Nationality',['class'=>'control-label']) }}
                                            {{ Form::text('p_address[nationality]',isset($p_address) ?  $p_address->nationality : null,['class'=>'form-control','id'=>'parNatism', 'placeholder'=>'Nationality']) }}

                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 ">
                                    <div class="row">
                                        <div class="col-3 ">
                                            <div class="form-group">
                                                {{ Form::label('contactMobile','Contact No.',['class'=>'control-label']) }} <i class="text-danger text-bold">*</i>
                                                {{ Form::number('mobile',null,['class'=>'form-control', 'placeholder'=>'Contact Number']) }}
                                                @error('mobile')
                                                <b style="color: red">{{ $message }}</b>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-3  ">
                                            <div class="form-group">
                                                {{ Form::label('whatsApp','WhatsApp No.',['class'=>'control-label']) }}
                                                {{ Form::text('whatsApp',null,['class'=>'form-control', 'placeholder'=>'Enter whats app no']) }}
                                                @error('email')
                                                <b style="color: red">{{ $message }}</b>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-3 ">
                                            <div class="form-group">
                                                {{ Form::label('email','E-mail',['class'=>'control-label']) }}
                                                {{ Form::email('email',null,['class'=>'form-control', 'placeholder'=>'email@gmail.com']) }}
                                                @error('email')
                                                <b style="color: red">{{ $message }}</b>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-3 mt-5">
                                            <div class="form-group">
                                                {{ Form::label('status','Status',['class'=>'control-label']) }}
                                                {{ Form::radio('status', 0, false, ['id'=>'inactive']) }}&nbsp;{{ Form::label('inactive','Inactive') }}
                                                {{ Form::radio('status', 1, true, ['id'=>'active']) }}&nbsp;{{ Form::label('active','Active') }}
                                            </div>
                                        </div>


                                    </div>
                                </div>
{{--                                <div class="col-6 ">--}}
{{--                                    <div class="col-12 mt-4">--}}
{{--                                        <div class="form-group">--}}
{{--                                            {{ Form::label('status','Status',['class'=>'control-label']) }}--}}
{{--                                            {{ Form::radio('status', 0, false, ['id'=>'inactive']) }}&nbsp;{{ Form::label('inactive','Inactive') }}--}}
{{--                                            {{ Form::radio('status', 1, true, ['id'=>'active']) }}&nbsp;{{ Form::label('active','Active') }}--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </div>
                        </div>

                    </div>
                </div>

                <div class="tab-pane fade show" id="acadimic_form" role="tabpanel" aria-labelledby="acadimic">
                    <h3 class="text-center p-2">Academics Information</h3>
                    @isset($father)
                        <input type="hidden" name="f_id" value="{{ $father->id }}">
                        <input type="hidden" name="m_id" value="{{ $mother->id }}">
                        <input type="hidden" name="g_id" value="{{ $guardian->id }}">
                        <input type="hidden" name="sa_id" @if(isset($studentAcademic)) value="{{ $studentAcademic->id }}" @endif>
                    @endisset
                    <div class="row">
                        <div class="col-12">
                            <div class="row">

                                <div class="form-group col-4">
                                    <div class="form-group">
                                        <label for="">School</label>
                                        <select name="school_id"  class="form-control">
                                            <option value="">--Select School--</option>
                                            @foreach ($schools as $item)
                                                <option value="{{ $item->id }}" class="customOption"
                                                @isset($studentAcademic->school_id)
                                                    {{ $item->id == $studentAcademic->school_id ? 'selected' : '' }}
                                                        @endisset >
                                                    {{ $item->name }}
                                                </option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-4">
                                    <div class="form-group">
                                        <label for="">Academic Class</label> <i class="text-danger text-bold">*</i>
                                        <select class="form-control " name="academic_class_id" id="getAcademicYear">
                                            <option value="">--Select Academics Class--</option>
                                            @foreach ($academicClass as $item)
                                                <option value="{{ $item->id }}"
                                                @isset($studentAcademic->academic_class_id)
                                                    {{ $item->id == $studentAcademic->academic_class_id ? 'selected' : '' }}
                                                        @endisset
                                                >
                                                    {{ $item->classes->name ?? '' }}-{{ $item->section->name ?? '' }}
                                                    -{{ $item->group->name ?? '' }}-{{ $item->sessions->year }}
                                                </option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-4">
                                    <div class="form-group">
                                        <label for="">Shift</label>  <i class="text-danger text-bold">*</i>
                                        <select name="shift_id"  class="form-control">
                                            <option value="">--Select Shift--</option>
                                            @foreach ($shifts as $item)
                                                <option value="{{ $item->id }}" class="customOption"
                                                @isset($studentAcademic->shift_id)
                                                    {{ $item->id == $studentAcademic->shift_id ? 'selected' : '' }}
                                                        @endisset >
                                                    {{ $item->name }}
                                                </option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    {{ Form::label('rank','Rank',['class'=>'control-label']) }}
                                    {{ Form::text('rank',!empty($studentAcademic) ? $studentAcademic->rank : null,['placeholder'=>'Student Rank','class' => 'form-control','id'=>'rank']) }}
                                    @error('rank')
                                    <b style="color: red">{{ $message }}</b>
                                    @enderror
                                </div>
                                <div class="form-group col-6">
                                    {{Form::label('studentId','Student ID',['class'=>'control-label'])}} <i class="text-danger">*</i>
                                    {{ Form::text('studentId', null, ['placeholder' => 'Student ID...','class' => 'form-control','id'=>'studentID']) }}
                                </div>

                                {{-- <div class="form-group col-6">
                                    {{ Form::label('session_id', 'Academic Year',['class'=>'control-label' ]) }}
                                    {{ Form::select('session_id',$repository->sessions(), !empty($studentAcademic) ? $studentAcademic->session_id : null, ['placeholder' => 'Select Academic year...','class'=>'form-control year']) }}
                                    @error('session_id')
                                    <b style="color: red">{{ $message }}</b>
                                    @enderror
                                </div> --}}
                                {{-- <div class="form-group col-6">
                                    {{ Form::label('class_id','Class',['class'=>'control-label'])}}
                                    {{ Form::select('class_id', $repository->classes(), !empty($studentAcademic) ? $studentAcademic->class_id : null, ['placeholder' => 'Select Class Name...','class'=>'form-control class']) }}
                                    @error('class_id')
                                    <b style="color: red">{{ $message }}</b>
                                    @enderror
                                </div>
                                <div class="form-group col-6">
                                    {{ Form::label('section_id','Section',['class'=>'control-label']) }}
                                    {{ Form::select('section_id',$repository->sections(),!empty($studentAcademic) ? $studentAcademic->section_id : null,['class'=>'form-control','placeholder'=>'Select Section']) }}
                                    @error('section_id')
                                    <b style="color: red">{{ $message }}</b>
                                    @enderror
                                </div>

                                <div class="form-group col-6">
                                    {{ Form::label('group_id','Group',['class'=>'control-label']) }}
                                    {{ Form::select('group_id', $repository->groups(), !empty($studentAcademic) ? $studentAcademic->group_id : null, ['placeholder' => 'Select Section...','class'=>'form-control']) }}
                                    @error('group_id')
                                    <b style="color: red">{{ $message }}</b>
                                    @enderror
                                </div> --}}
                            </div>
                        </div>
                        @php
                        if (isset($studentAcademic)){
                            $ssc=json_decode($studentAcademic->ssc);
                            $hsc=json_decode($studentAcademic->hsc);
                            $jsc=json_decode($studentAcademic->hsc);
                            $pec=json_decode($studentAcademic->pec);
                            }
                        @endphp
                        <div class="col-12 mb-3">
                            <h3 class="text-center m-3 p-2">Previous Academic Information <small>(optional)</small></h3>
                            <div class="row">
                                <div class="col-6">
                                    <h5 class="bg-info text-center" >HSC </h5>
                                    <div class="form-group">
                                        <label for="inputAddress">Institute</label>
                                        <input type="text" class="form-control" name="hsc[inst]"   @if(isset($hsc)) value="{{$hsc->inst}}"  @endif  id="inputAddress" placeholder="Institute Name">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="reg">Reg. Number</label>
                                            <input type="number" name="hsc[reg]"  @if(isset($hsc)) value="{{$hsc->reg}}"  @endif class="form-control" id="reg" placeholder="Registration No">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="roll">Roll Number</label>
                                            <input type="number"  name="hsc[roll]"  @if(isset($hsc)) value="{{$hsc->roll}}"  @endif class="form-control" id="roll" placeholder="Roll No.">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="gpa">GPA</label>
                                            <input type="number" name="hsc[gpa]"  @if(isset($hsc)) value="{{$hsc->gpa}}"  @endif class="form-control" id="gpa" placeholder="GPA">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="year">Passing Year</label>
                                            <input type="number"  name="hsc[year]"  @if(isset($hsc)) value="{{$hsc->year}}"  @endif class="form-control" id="year" placeholder="Passing Year">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <label for="year">Group</label>
                                        <select name="hsc[group]"  class="form-control">
                                            <option value="">-Select-</option>
                                            @foreach ($groups as $item)
                                                <option value="{{ $item->id }}" class="customOption"
                                                @isset($hsc->group)
                                                    {{ $item->id == $hsc->group ? 'selected' : '' }}
                                                        @endisset >
                                                    {{ $item->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <h5  class="bg-info text-center" >SSC </h5>
                                    <div class="form-group">
                                        <label for="inputAddress">Institute</label>
                                        <input type="text" class="form-control" name="ssc[inst]"  @if(isset($ssc)) value="{{$ssc->inst}}"  @endif id="inputAddress" placeholder="Institute Name">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="reg">Reg. Number</label>
                                            <input type="number" name="ssc[reg]" @if(isset($ssc)) value="{{$ssc->reg}}"  @endif  class="form-control" id="reg" placeholder="Registration No">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="roll">Roll Number</label>
                                            <input type="number"  name="ssc[roll]" class="form-control" id="roll" placeholder="Roll No.">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="gpa">GPA</label>
                                            <input type="number" name="ssc[gpa]" @if(isset($ssc)) value="{{$ssc->gpa}}"  @endif  class="form-control" id="gpa" placeholder="GPA">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="year">Passing Year</label>
                                            <input type="number"  name="ssc[year]" @if(isset($ssc)) value="{{$ssc->year}}"  @endif  class="form-control" id="year" placeholder="Passing Year">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <label for="year">Group</label>
                                        <select name="ssc[group]"  class="form-control">
                                            <option value="">-Select-</option>
                                            @foreach ($groups as $item)
                                                <option value="{{ $item->id }}" class="customOption"
                                                @isset($ssc->group)
                                                    {{ $item->id == $ssc->group ? 'selected' : '' }}
                                                        @endisset >
                                                    {{ $item->name }}
                                                </option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-5">
                                <div class="col-6">
                                    <h5  class="bg-info text-center"  >JSC </h5>
                                    <div class="form-group">
                                        <label for="inputAddress">Institute</label>
                                        <input type="text" class="form-control" name="jsc[inst]" @if(isset($jsc)) value="{{$jsc->inst}}"  @endif  id="inputAddress" placeholder="Institute Name">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="reg">Reg. Number</label>
                                            <input type="number" name="jsc[reg]" @if(isset($jsc)) value="{{$jsc->reg}}"  @endif  class="form-control" id="reg" placeholder="Registration No">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="roll">Roll Number</label>
                                            <input type="number"  name="jsc[roll]" @if(isset($jsc)) value="{{$jsc->roll}}"  @endif  class="form-control" id="roll" placeholder="Roll No.">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="gpa">GPA</label>
                                            <input type="number" name="jsc[gpa]" @if(isset($jsc)) value="{{$jsc->gpa}}"  @endif  class="form-control" id="gpa" placeholder="GPA">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="year">Passing Year</label>
                                            <input type="number"  name="jsc[year]" @if(isset($jsc)) value="{{$jsc->year}}"  @endif  class="form-control" id="year" placeholder="Passing Year">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <h5  class="bg-info text-center" >PEC </h5>
                                    <div class="form-group">
                                        <label for="inputAddress">Institute</label>
                                        <input type="text" class="form-control" name="pec[inst]" @if(isset($pec)) value="{{$pec->inst}}"  @endif  id="inputAddress" placeholder="Institute Name">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="reg">Reg. Number</label>
                                            <input type="number" disabled name="pec[reg]" @if(isset($pec)) value="{{$pec->reg ?? ''}}"  @endif  class="form-control" id="reg" placeholder="Registration No">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="roll">Roll Number</label>
                                            <input type="number"  name="pec[roll]" @if(isset($pec)) value="{{$pec->roll}}"  @endif  class="form-control" id="roll" placeholder="Roll No.">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="gpa">GPA</label>
                                            <input type="number" name="pec[gpa]" @if(isset($pec)) value="{{$pec->gpa}}"  @endif  class="form-control" id="gpa" placeholder="GPA">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="year">Passing Year</label>
                                            <input type="number"  name="pec[year]" @if(isset($pec)) value="{{$pec->year}}"  @endif  class="form-control" id="year" placeholder="Passing Year">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="personal_form" role="tabpanel" aria-labelledby="personal">
                    <h3 class="text-center"><b>Personal Information</b></h3>
                    <div class="row">
                        <div class="col-6">
                            <div class="card">
                                <div class="card-header">
                                    <b>Father Info</b>
                                </div>
                                <div class="card-body cutom_shadow">
                                    <div class="row">

                                        <div class="form-group col-12">
                                            {{ Form::label('f_name',' Name (English)',['class'=>'control-label']) }}  <i class="text-danger text-bold">*</i>
                                            {{ Form::text('f_name', !empty($father) ? $father->f_name : null,['class'=>'form-control', 'placeholder'=>' Name']) }}
                                            @error('fname')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>

                                        <div class="form-group col-12">
                                            {{ Form::label('f_name_bn',' Name (Bangla)',['class'=>'control-label']) }}
                                            {{ Form::text('f_name_bn',!empty($father) ? $father->f_name_bn : null,['class'=>'form-control', 'placeholder'=>' Name Bangla']) }}
                                            @error('f_name_bn')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>

                                        <div class="form-group col-6">
                                            {{ Form::label('f_mobile',' Mobile',['class'=>'control-label']) }}
                                            {{ Form::number('f_mobile',!empty($father) ? $father->f_mobile : null,['class'=>'form-control', 'placeholder'=>' Mobile']) }}
                                            @error('f_mobile')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>

                                        <div class="form-group col-6">
                                            {{ Form::label('f_email',' Email',['class'=>'control-label']) }}
                                            {{ Form::email('f_email',!empty($father) ? $father->f_email : null,['class'=>'form-control', 'placeholder'=>' Email']) }}
                                            @error('f_email')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>

                                        <div class="form-group col-6">
                                            {{ Form::label('f_occupation',' Occupation',['class'=>'control-label']) }}
                                            {{ Form::text('f_occupation',!empty($father) ? $father->f_occupation : null,['class'=>'form-control', 'placeholder'=>' Occupation']) }}
                                            @error('f_occupation')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>

                                        <div class="form-group col-6">
                                            {{ Form::label('f_nid',' NID',['class'=>'control-label']) }}
                                            {{ Form::number('f_nid',!empty($father) ? $father->f_nid : null,['class'=>'form-control', 'placeholder'=>' NID']) }}
                                            @error('f_nid')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>

                                        <div class="form-group col-12">
                                            {{ Form::label('f_wrk_place','Working Place',['class'=>'control-label']) }}
                                            {{ Form::text('f_wrk_place',!empty($father) ? $father->f_wrk_place : null,['class'=>'form-control', 'placeholder'=>' Add working Place']) }}
                                        </div>

                                        <div class="input-group  col-8">
                                            <div class="custom-file">
                                                <input type="file" name="f_image" class="custom-file-input" id="inputGroupFile01">
                                                <label class="custom-file-label" for="inputGroupFile01">Choose
                                                    file</label>
                                            </div>
                                        </div>
                                        <div class="input-group col-4">
                                            <img id="imgPreview" src="#" width="100px" height="50px" alt="pic" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="card">
                                <div class="card-header">
                                    <b>Mother Info</b>
                                </div>
                                <div class="card-body cutom_shadow">
                                    <div class="row">
                                        <div class="form-group col-12">
                                            {{ Form::label('m_name',' Name (English)',['class'=>'control-label']) }}  <i class="text-danger text-bold">*</i>
                                            {{ Form::text('m_name',!empty($mother) ? $mother->m_name : null,['class'=>'form-control', 'placeholder'=>' Name']) }}
                                            @error('m_name')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>
                                        <div class="form-group col-12">
                                            {{ Form::label('m_name_bn',' Name (Bangla)',['class'=>'control-label']) }}
                                            {{ Form::text('m_name_bn',!empty($mother) ? $mother->m_name_bn : null,['class'=>'form-control', 'placeholder'=>' Name Bangla']) }}
                                            @error('m_name_bn')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>
                                        <div class="form-group col-6">
                                            {{ Form::label('m_mobile',' Mobile',['class'=>'control-label']) }}
                                            {{ Form::number('m_mobile',!empty($mother) ? $mother->m_mobile : null,['class'=>'form-control', 'placeholder'=>' Mobile']) }}
                                            @error('m_mobile')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>
                                        <div class="form-group col-6">
                                            {{ Form::label('m_email',' Email',['class'=>'control-label']) }}
                                            {{ Form::email('m_email',!empty($mother) ? $mother->m_email : null,['class'=>'form-control', 'placeholder'=>' Email']) }}
                                            @error('m_email')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>

{{--                                        <div class="form-group col-6">--}}
{{--                                            {{ Form::label('m_dob',' Date of Birth',['class'=>'control-label']) }}--}}
{{--                                            {{ Form::date('m_dob',!empty($mother) ? $mother->m_dob : null,['class' => 'form-control', 'placeholder'=>' Date of Birth']) }}--}}
{{--                                            @error('m_dob')--}}
{{--                                            <b style="color: red">{{ $message }}</b>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}
                                        <div class="form-group col-6">
                                            {{ Form::label('m_occupation',' Occupation',['class'=>'control-label']) }}
                                            {{ Form::text('m_occupation',!empty($mother) ? $mother->m_occupation : null,['class'=>'form-control', 'placeholder'=>' Occupation']) }}
                                            @error('m_occupation')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>

                                        <div class="form-group col-6">
                                            {{ Form::label('m_nid',' NID',['class'=>'control-label']) }}
                                            {{ Form::number('m_nid',!empty($mother) ? $mother->m_nid : null,['class'=>'form-control', 'placeholder'=>' NID']) }}
                                            @error('m_nid')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>
                                        <div class="form-group col-12">
                                            {{ Form::label('workign_place','Working Place',['class'=>'control-label']) }}
                                            {{ Form::text('m_wrk_place',!empty($mother) ? $mother->m_wrk_place : null,['class'=>'form-control', 'placeholder'=>' Add working Place']) }}
                                        </div>
{{--                                        <div class="form-group col-6">--}}
{{--                                            {{ Form::label('m_birth_certificate',' Birth Certificate',['class'=>'control-label']) }}--}}
{{--                                            {{ Form::number('m_birth_certificate',!empty($mother) ? $mother->m_birth_certificate : null,['class'=>'form-control', 'placeholder'=>' Birth Certificate']) }}--}}
{{--                                            @error('m_birth_certificate')--}}
{{--                                            <b style="color: red">{{ $message }}</b>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}

                                        <div class="input-group col-8">

                                            <div class="custom-file">
                                                <input type="file" name="m_image" class="custom-file-input" id="inputGroupFile02">
                                                <label class="custom-file-label" for="inputGroupFile02">Choose
                                                    file</label>
                                            </div>
                                        </div>
                                        <div class="input-group col-4">
                                            <img id="imgPreview2" src="#" height="50px" width="100px" alt="pic" />
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card mt-4 mb-2">
                                <div class="card-header">
                                    <b>Guardian Info</b>
                                </div>
                                <div class="card-body cutom_shadow">
                                    <div class="row">
                                        <div class="form-group col-6">
                                            {{ Form::label('g_name',' Name (English)',['class'=>'control-label']) }}
                                            {{ Form::text('g_name',!empty($guardian) ? $guardian->g_name : null,['class'=>'form-control', 'placeholder'=>' Name']) }}
                                            @error('g_name')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>
                                        <div class="form-group col-6">
                                            {{ Form::label('g_name_bn',' Name (Bangla)',['class'=>'control-label']) }}
                                            {{ Form::text('g_name_bn',!empty($guardian) ? $guardian->g_name_bn : null,['class'=>'form-control', 'placeholder'=>' Name Bangla']) }}
                                            @error('g_name_bn')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>
                                        <div class="form-group col-4">
                                            {{ Form::label('g_mobile',' Mobile',['class'=>'control-label']) }}  <i class="text-danger text-bold">*</i>
                                            {{ Form::number('g_mobile',!empty($guardian) ? $guardian->g_mobile : null,['class'=>'form-control', 'placeholder'=>' Mobile']) }}
                                            @error('g_mobile')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>
                                        <div class="form-group col-4">
                                            {{ Form::label('g_email',' Email',['class'=>'control-label']) }}
                                            {{ Form::email('g_email',!empty($guardian) ? $guardian->g_email : null,['class'=>'form-control', 'placeholder'=>' Email']) }}
                                            @error('g_email')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>

{{--                                        <div class="form-group col-6">--}}
{{--                                            {{ Form::label('g_dob',' Date of Birth',['class'=>'control-label']) }}--}}
{{--                                            {{ Form::date('g_dob',!empty($guardian) ? $guardian->g_dob : null,['class' => 'form-control', 'placeholder'=>' Date of Birth']) }}--}}
{{--                                            @error('g_dob')--}}
{{--                                            <b style="color: red">{{ $message }}</b>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}
                                        <div class="form-group col-4">
                                            {{ Form::label('g_occupation',' Occupation',['class'=>'control-label']) }}
                                            {{ Form::text('g_occupation',!empty($guardian) ? $guardian->g_occupation : null,['class'=>'form-control', 'placeholder'=>' Occupation']) }}
                                            @error('g_occupation')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>

                                        <div class="form-group col-5">
                                            {{ Form::label('g_nid',' NID',['class'=>'control-label']) }}
                                            {{ Form::number('g_nid',!empty($guardian) ? $guardian->g_nid : null,['class'=>'form-control', 'placeholder'=>' NID']) }}
                                            @error('g_nid')
                                            <b style="color: red">{{ $message }}</b>
                                            @enderror
                                        </div>
                                        <div class="input-group col-5 mt-4">
                                            <div class="custom-file mt-2">
                                                <input type="file" name="g_image" class="custom-file-input" id="inputGroupFile04">
                                                <label class="custom-file-label" for="inputGroupFile04">Choose
                                                    file</label>
                                            </div>
                                        </div>
                                        <div class="input-group col-2 mt-4">
                                            <img id="imgPreview4" src="#" height="50px" width="100px" alt="pic" />
                                        </div>

{{--                                        <div class="form-group col-6">--}}
{{--                                            {{ Form::label('g_birth_certificate',' Birth Certificate',['class'=>'control-label']) }}--}}
{{--                                            {{ Form::number('g_birth_certificate',!empty($guardian) ? $guardian->g_birth_certificate : null,['class'=>'form-control', 'placeholder'=>' Birth Certificate']) }}--}}
{{--                                            @error('g_birth_certificate')--}}
{{--                                            <b style="color: red">{{ $message }}</b>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12">
                    <div class="form-group">
                        {!! Form::submit('Submit', ['class' => 'form-control, btn btn-success btn-block']) !!}
                    </div>
                </div>
            </div>
            <!-- 	</div> -->
        </div>
    </div>
</div>
<!-- /.MultiStep Form -->
@section('script')
    <script>
        $(document).on('keyup','#rank', function () {
            var academicYear = $('#getAcademicYear').val();
            console.log('form', academicYear);

            $.ajax({
                url:"{{url('admin/load_student_id')}}",
                type:'GET',
                data:{academicYear:academicYear},
                success:function (data) {
                    console.log(data);
                    $('#studentID').val(data);
                }
            });
        });


        document.querySelectorAll(".drop-zone__input").forEach((inputElement) => {
            const dropZoneElement = inputElement.closest(".drop-zone");

            dropZoneElement.addEventListener("click", (e) => {
                inputElement.click();
            });

            inputElement.addEventListener("change", (e) => {
                if (inputElement.files.length) {
                    updateThumbnail(dropZoneElement, inputElement.files[0]);
                }
            });

            dropZoneElement.addEventListener("dragover", (e) => {
                e.preventDefault();
                dropZoneElement.classList.add("drop-zone--over");
            });

            ["dragleave", "dragend"].forEach((type) => {
                dropZoneElement.addEventListener(type, (e) => {
                    dropZoneElement.classList.remove("drop-zone--over");
                });
            });

            dropZoneElement.addEventListener("drop", (e) => {
                e.preventDefault();

                if (e.dataTransfer.files.length) {
                    inputElement.files = e.dataTransfer.files;
                    updateThumbnail(dropZoneElement, e.dataTransfer.files[0]);
                }

                dropZoneElement.classList.remove("drop-zone--over");
            });
        });

        /**
         * Updates the thumbnail on a drop zone element.
         *
         * @param {HTMLElement} dropZoneElement
         * @param {File} file
         */
        function updateThumbnail(dropZoneElement, file) {
            let thumbnailElement = dropZoneElement.querySelector(".drop-zone__thumb");

            // First time - remove the prompt
            if (dropZoneElement.querySelector(".drop-zone__prompt")) {
                dropZoneElement.querySelector(".drop-zone__prompt").remove();
            }

            // First time - there is no thumbnail element, so lets create it
            if (!thumbnailElement) {
                thumbnailElement = document.createElement("div");
                thumbnailElement.classList.add("drop-zone__thumb");
                dropZoneElement.appendChild(thumbnailElement);
            }

            let x = document.getElementById("editImage");
            if (file.name != null) {
                // console.log(x);
                x.style.display = "none";
            }


            console.log(file.name);

            thumbnailElement.dataset.label = file.name;

            // Show thumbnail for image files
            if (file.type.startsWith("image/")) {
                const reader = new FileReader();

                reader.readAsDataURL(file);
                reader.onload = () => {
                    thumbnailElement.style.backgroundImage = `url('${reader.result}')`;
                };
            } else {
                thumbnailElement.style.backgroundImage = null;
            }
        }

    </script>

    <script type="text/javascript">
        $(function () {
            $("#fillParment").on("click",function () {
                if (this.checked) {
                    $("#parAdd").val($("#preAdd").val());
                    $("#parArea").val($("#preArea").val());
                    $("#parZip").val($("#preZip").val());
                    $("#parCity").val($("#preCity").val());
                    $("#parCountry").val($("#preCountry").val());
                    $("#parNatism").val($("#preNatism").val());
                }else {
                    $("#parAdd").val('');
                    $("#parArea").val('');
                    $("#parZip").val('');
                    $("#parCity").val('');
                    $("#parCountry").val('');
                    $("#parNatism").val('');
                }
            });
        });
    </script>
    <script>
        $(document).ready(()=>{
            $('#inputGroupFile01').change(function(){
                const file = this.files[0];
                console.log(file);
                if (file){
                    let reader = new FileReader();
                    reader.onload = function(event){
                        console.log(event.target.result);
                        $('#imgPreview').attr('src', event.target.result);
                    }
                    reader.readAsDataURL(file);
                }
            });

            $('#inputGroupFile02').change(function(){
                const file = this.files[0];
                console.log(file);
                if (file){
                    let reader = new FileReader();
                    reader.onload = function(event){
                        console.log(event.target.result);
                        $('#imgPreview2').attr('src', event.target.result);
                    }
                    reader.readAsDataURL(file);
                }
            });

            $('#inputGroupFile03').change(function(){
                const file = this.files[0];
                console.log(file);
                if (file){
                    let reader = new FileReader();
                    reader.onload = function(event){
                        console.log(event.target.result);
                        $('#imgPreview3').attr('src', event.target.result);
                    }
                    reader.readAsDataURL(file);
                }
            });

            $('#inputGroupFile04').change(function(){
                const file = this.files[0];
                console.log(file);
                if (file){
                    let reader = new FileReader();
                    reader.onload = function(event){
                        console.log(event.target.result);
                        $('#imgPreview4').attr('src', event.target.result);
                    }
                    reader.readAsDataURL(file);
                }
            });

        });
    </script>
@endsection
