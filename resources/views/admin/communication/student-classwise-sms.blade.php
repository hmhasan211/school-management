@extends('layouts.fixed')

@section('title', 'Class-wise SMS')
@section('style')
    <style>
        @media print {
            .no_print {
                display: none;
            }
        }

        /*span.showDiscountMsg {*/
        /*    background: #90abb1;*/
        /*    padding: 2px 5px 2px 4px;*/
        /*    border-radius: 16px;*/
        /*    font-size: 11px;*/
        /*    font-weight: 900;*/
        /*    cursor: pointer;*/
        /*}*/

    </style>
@stop

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Class-wise SMS') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{ __('Communication') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('class-wise sms') }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- /.Search-panel -->
    <section class="content no_print ">
        <div class="container-fluid">
            <div class="col-lg-12 col-sm-8 col-md-8 col-xs-12  ">
                <div class="card card-primary card-outline">
                    <div class="card-body" style="padding-bottom:0">
                        <form method="get" action="{{ route('student-sms.classwise') }}">
                            <div class="form-row" style="margin-left: 33%;" >

                                <label class="mr-1 mt-1"> Academic Class </label>
                                <div class="form-group col-md-3">
                                    <select name="ac_class_id[]" id="" class=" form-control select2" multiple="multiple"  required>
                                        <option value="">Select Class</option>

                                        @foreach ($classes as $cls)
                                            <option value="{{ $cls->id }}"  {{ request()->ac_class_id == $cls->id ? 'selected="selected"' : '' }}>
                                                {{ $cls->classes->name ?? '' }} {{ $cls->section->name ?? '' }} {{ $cls->group->name ?? '' }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col-md-1 ">
                                    <button type="submit" class="btn btn-info btn-sm "><i
                                                class="fa fa-check"></i>
                                    </button>
                                </div>


                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>{{-- end --}}
    </section>

    @if (isset($students))

        <section class="content mt-4">
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('student-sms.send') }}" method='post'>
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="form-row">
                                                    <div class="col-md-5">

{{--                                                        <label for="">{{ __('Numbers')}}--}}
{{--                                                            <small>{{ __('add multiple numbers with "+" sign')}}</small></label>--}}
{{--                                                        <div class="input-group">--}}
{{--                                                            <textarea class="form-control descriptionLen" rows="5"--}}
{{--                                                                      placeholder="01XXXXXXXXX+01XXXXXXXXX+01XXXXXXXXX"--}}
{{--                                                                      name="numbers" cols="50" id="textarea"></textarea>--}}
{{--                                                        </div>--}}

                                                        @if(isset($students))
                                                            @foreach ( $students as $stu)
                                                                <input type="hidden" value="{{ $stu->student->mobile}}"
                                                                       name="numbers[]"/>
                                                            @endforeach
                                                        @endif


                                                        <p> Total Student : <strong class="badge badge-secondary"> {{ count($students) }} </strong>
                                                            <input type="hidden" value="{{count($students)}}"
                                                                   name="total_number"/>
                                                        </p>
                                                        <label for="">{{ __('SMS Description')}}</label>
                                                        <div class="input-group">
                                                            <textarea  wrap="off" class="form-control descriptionLen" rows="5"
                                                                      placeholder="type sms here.." name="message"
                                                                      cols="50" id="textarea"></textarea>
                                                        </div>
                                                        <p></p>
                                                        <div class="input-group">
                                                            <button class="btn btn-primary" type="submit"> SEND <i
                                                                        class="fa fa-paper-plane"></i> &nbsp; </button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--list--}}
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    @endif

@stop

@section('plugin')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@stop
@section('script')
    <script type="text/javascript">
            $('.select2').select2({})
    </script>
@stop
