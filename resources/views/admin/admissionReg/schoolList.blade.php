@extends('layouts.fixed')

@section('title','Schools')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Schools</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Regitration Mng</a></li>
                        <li class="breadcrumb-item active">school</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header" style="border-bottom: none !important;">
                            <div class="row">
                                <div class="col-md-12">
                                    <div style="float: left;">
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"  style="margin-top: 10px; margin-left: 10px;"> <i class="fas fa-plus-circle"></i> New</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>School Name</th>
                                    <th>Phone</th>
                                    <th>Address</th>
                                    <th>Email</th>
                                    <th>Head of Inst.</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php($i = 0)
                                @forelse($schools as $key=>$school)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $school->name ?? '' }}</td>

                                        <td> {{ $school->phone ?? '' }} </td>
                                        <td>{{ $school->address ?? '' }}</td>
                                        <td>{{ $school->email ?? '' }}</td>
                                        <td>{{ $school->p_name ?? '-' }}</td>
                                        <td>
                                            {{ Form::model($school,['action'=>['Backend\SchoolController@destroy',$school->id],'method'=>'delete','onsubmit'=>'return confirmDelete()']) }}
                                            <a type="button" value='{{$school->id}}' title="SMS" class="btn btn-info btn-sm sms" data-toggle="modal" data-target="#smsModal" data-whatever="@mdo"> <i class="fa fa-paper-plane"></i> </a>
                                            <a type="button" class="btn btn-warning btn-sm edit" value='{{$school->id}}' title="Edit"> <i class="fas fa-edit"></i></a>
                                            {{ Form::submit('X',['class'=>'btn btn-danger btn-sm']) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @empty
                                    <td colspan="10" class="text-danger text-bold text-center">  No Data Found!! 😒</td>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ***/ Pop Up Model for button -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add School</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    {!! Form::open(['action'=>'Backend\SchoolController@store', 'method'=>'post']) !!}

                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label" style="font-weight: 500; text-align: right"> Name*</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'School Name']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label" style="font-weight: 500; text-align: right">Phone</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {!! Form::text('phone', null, ['class'=>'form-control', 'placeholder'=>'Phone']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label" style="font-weight: 500; text-align: right">Address</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {!! Form::text('address', null, ['class'=>'form-control', 'placeholder'=>'Address']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label" style="font-weight: 500; text-align: right">E-mail</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {!! Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'E-mail']) !!}
                            </div>
                        </div>
                    </div>


                    <div style="float: right">
                        <button type="submit" class="btn btn-success  btn-sm" > <i class="fas fa-plus-circle"></i> Add</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- ***/ Pop Up Model for button End-->

    <!-- ***/ Pop Up Model for button -->
    <div class="modal fade" id="smsModal" tabindex="-1" role="dialog" aria-labelledby="smsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Send SmS </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    {!! Form::open(['action'=>'Backend\SchoolController@sendSms', 'method'=>'post']) !!}

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="input-group">
                                {!! Form::text('number', null, ['class'=>'form-control number', 'placeholder'=>'Receiver Number']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="input-group">
                                {!! Form::textarea('message', null, ['class'=>'form-control message', 'placeholder'=>'Write Message Here']) !!}
                            </div>
                        </div>
                    </div>

                    <div>
                        <button type="submit" class="btn btn-success btn-block  btn-sm" > Send</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- ***/ Pop Up Model for button End-->

    <!-- ***/ Pop Up Model for Edit school -->
    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update School</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['action'=>'Backend\SchoolController@update', 'method'=>'post']) !!}
                    {!! Form::hidden('id', null, ['id'=>'id']) !!}
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label" style="font-weight: 500; text-align: right"> Name*</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {!! Form::text('name', null, ['class'=>'form-control name', 'placeholder'=>'School Name']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label" style="font-weight: 500; text-align: right">Phone</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {!! Form::text('phone', null, ['class'=>'form-control phone', 'placeholder'=>'Phone']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label" style="font-weight: 500; text-align: right">Address</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {!! Form::text('address', null, ['class'=>'form-control address', 'placeholder'=>'Address']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-sm-3 col-form-label" style="font-weight: 500; text-align: right">E-mail</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                                {!! Form::text('email', null, ['class'=>'form-control email', 'placeholder'=>'E-mail']) !!}
                            </div>
                        </div>
                    </div>


                    <div style="float: right">
                        <button type="submit" class="btn btn-warning btn-sm">
                            <i class="fas fa-plus-circle"></i> Update
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- ***/ Pop Up Model for button End-->
@stop

@section('script')
    <script>
        $(document).on('click','.sms',function () {
            var id = $(this).attr('value');

            $.ajax({
                method:"get",
                url:"{{ route('school.sms')}}",
                data:{id:id,"_token":"{{ csrf_token() }}"},
                dataType:"json",
                success:function(response){
                    console.log(response);
                    $(".number").val(response.phone);
                },
                error:function(err){
                    console.log(err);
                }
            });
        })
    </script>
    <script>
        $(document).on('click', '.edit', function () {
            $("#edit").modal("show");
            var id = $(this).attr('value');

            $.ajax({
                method:"get",
                url:"{{ url('school-edit')}}",
                data:{id:id,"_token":"{{ csrf_token() }}"},
                dataType:"json",
                success:function(response){
                    console.log(response);
                    $("#id").val(response.id);
                    $(".name").val(response.name);
                    $(".phone").val(response.phone);
                    $(".address").val(response.address);
                    $(".email").val(response.email);

                },
                error:function(err){
                    console.log(err);
                }
            });
        });

        function confirmDelete(){
            let x = confirm('Are you sure you want to delete this School?');
            return !!x;
        }
    </script>
@stop
