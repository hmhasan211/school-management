@extends('layouts.fixed')

@section('title', 'Custom Search')

@section('style')
    <style>
        @media print {
            .no_print {
                display: none;
            }
        }
    </style>
@stop

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Student Custom Search') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{ __('Addmission') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('Custom Search') }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- /.Search-panel -->
    <section class="content no_print ">
        <div class="container-fluid">
            <div class="col-lg-12 col-sm-8 col-md-12 col-xs-12 ">
                <div class="card card-primary card-outline">
                    <div class="card-body">
                        <form method="get" action="{{ route('student-custom.report') }}">
                            <div class="form-row">
                                <div class="form-group col">
                                    <label>Group </label>
                                    <select class="custom-select " name="group_id" id="inputGroupSelect01">
                                        <option value="">-Select-</option>
                                        @foreach ($groups as $grp)
                                            <option value="{{ $grp->id }}" {{ request()->group_id == $grp->id ? 'selected':'' }}>
                                                {{ $grp->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col">
                                    <label>Section </label>
                                    <select class="custom-select " name="section_id" id="inputGroupSelect01">
                                        <option value="">-Select-</option>
                                        @foreach ($section as $value)
                                            <option value="{{ $value->id }}" {{ request()->section_id == $value->id ? 'selected':'' }}>
                                                {{ $value->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col">
                                    <label>Religion </label>
                                    <select class="custom-select" name="religion_id" id="inputGroupSelect01">
                                        <option value="">-Select-</option>
                                        @foreach ($religions as $rlg)
                                            <option value="{{ $rlg->id }}" {{ request()->religion_id == $rlg->id ? 'selected':'' }}>
                                                {{ $rlg->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col">
                                    <label>Age </label>
                                    <input type="text" name="age" class="form-control" placeholder="Age">
                                </div>

                                <div class="form-group ml-2 mt-4">

                                    <button title="Search" type="submit" class="btn btn-info btn-md"><i
                                                class="fa fa-search"></i>&nbsp
                                    </button>
                                </div>

                                <div class="form-group ml-2 mt-4">
                                    <a title="Reset Search Data" href="{{ route('student-custom.report') }}"
                                       class="btn btn-danger btn-md"><i
                                                class="fa fa-undo"></i>&nbsp
                                    </a>
                                </div>

                                <div class="form-group ml-2 mt-4">
                                    <button title="Print" class="btn btn-warning btn-md "
                                            onclick="window.print(); return false;"><i
                                                class="fa fa-print"></i>&nbsp
                                    </button>
                                </div>
                                <div class="form-group ml-2 mt-4">
                                    <button class="btn btn-info btn-md" type="button" data-toggle="collapse"
                                            data-target="#collapseExample" aria-expanded="false"
                                            aria-controls="collapseExample">
                                        More Query
                                    </button>
                                </div>
                                <div class="form-group ml-2 mt-4">
                                    <button class="btn btn-info btn-md" type="button"
                                            data-toggle="collapse"
                                            data-target="#collapsefield" aria-expanded="false"
                                            aria-controls="collapseExample">
                                        Select Fields
                                    </button>
                                </div>
                            </div>
                            <div class="form-row ">
                                <div class="collapse" id="collapseExample">
                                    <div class="card card-body">
                                        <div class="form-row">
                                            <div class="form-group form-check">
                                                <input type="checkbox" @isset( request()->is_tribal) {{  request()->is_tribal ? 'checked':'' }}@endisset name="is_tribal"  class="form-check-input"
                                                       id="is_tribal">
                                                <label class="form-check-label" for="is_tribal">Tribal</label>
                                            </div>

                                            <div class="form-group form-check  ml-4">
                                                <input type="checkbox"  @isset( request()->is_freedomfighter) {{  request()->is_freedomfighter ? 'checked':'' }}@endisset name="is_freedomfighter" class="form-check-input"
                                                       id="is_freedomfighter">
                                                <label class="form-check-label" for="is_freedomfighter">Freedom
                                                    Fighter</label>
                                            </div>

                                            <div class="form-group form-check  ml-4">
                                                <input type="checkbox"  @isset( request()->disability) {{  request()->disability ? 'checked':'' }}@endisset name="disability" class="form-check-input"
                                                       id="disability">
                                                <label class="form-check-label"  for="disability">Disability</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row  ml-2">
                                    <div class="collapse" id="collapsefield">
                                        <div class="card card-body">

                                            <div class="form-row">
                                                <div class="form-group form-check ">
                                                    <input type="checkbox" checked name="field[]" @isset($reqField) {{ in_array("name",$reqField) ? 'checked':'' }}@endisset value="name" class="form-check-input"
                                                           id="mobnameile">
                                                    <label class="form-check-label" for="name">Name</label>
                                                </div>
                                                <div class="form-group form-check ml-3">
                                                    <input type="checkbox"  name="field[]" @isset($reqField) {{ in_array("studentId",$reqField) ? 'checked':'' }}@endisset value="studentId" class="form-check-input"
                                                           id="stu_id">
                                                    <label class="form-check-label" for="stu_id">StudentId</label>
                                                </div>
                                                <div class="form-group form-check ml-3">
                                                    <input type="checkbox" name="field[]" @isset($reqField) {{ in_array("mobile",$reqField) ? 'checked':'' }}@endisset value="mobile" class="form-check-input"
                                                           id="mobile">
                                                    <label class="form-check-label" for="mobile">Mobile</label>
                                                </div>
                                                <div class="form-group form-check ml-3">
                                                    <input type="checkbox" name="field[]" @isset($reqField) {{ in_array("address",$reqField) ? 'checked':'' }}@endisset value="address" class="form-check-input"
                                                           id="address">
                                                    <label class="form-check-label" for="address">Address</label>
                                                </div>
                                                <div class="form-group form-check ml-3">
                                                    <input type="checkbox" name="field[]" @isset($reqField) {{ in_array("email",$reqField) ? 'checked':'' }}@endisset value="email" class="form-check-input"
                                                           id="email">
                                                    <label class="form-check-label" for="email">Email</label>
                                                </div>
                                                <div class="form-group form-check ml-3">
                                                    <input type="checkbox" name="field[]" @isset($reqField) {{ in_array("area",$reqField) ? 'checked':'' }}@endisset value="area" class="form-check-input"
                                                           id="area">
                                                    <label class="form-check-label" for="area">Area</label>
                                                </div>
                                                <div class="form-group form-check ml-3">
                                                    <input type="checkbox"  id="f_name" name="field[]" @isset($reqField) {{ in_array("f_name",$reqField) ? 'checked':'' }}@endisset value="f_name" class="form-check-input" id="area">
                                                    <label class="form-check-label" for="f_name">Father</label>
                                                </div>
                                                <div class="form-group form-check ml-3">
                                                    <input type="checkbox" name="field[]" @isset($reqField) {{ in_array("m_name",$reqField) ? 'checked':'' }}@endisset value="m_name" class="form-check-input"
                                                           id="m_name">
                                                    <label class="form-check-label" for="m_name">Mother</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.card -->
            </div>
            {{-- start --}}
        </div>{{-- end --}}
    </section>

    @if (isset($students))
        <section class="content mt-4">
            <div class="container-fluid">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center mb-4">
                                <h3>Student Custom Search </h3>
                            </div>
                            <table class="table table-bordered  table-sm">
                                <thead>
                                <tr>
                                    <th>Sl.</th>

                                    @if(isset($reqField))
                                        @foreach($reqField as $item)
                                            <th>  {{ $item }}</th>
                                        @endforeach
                                    @endif
                                </tr>
                                </thead>
                                <tbody>

                                @forelse($students as $key =>$student)
                                    <tr>
                                        <td >{{ $key + 1 }}</td>

                                        @if(in_array("name",$reqField))
                                            <td id="mbl" >{{ $student->student->name ?? '' }}</td>
                                        @endif

                                        @if(in_array("studentId",$reqField))
                                            <td id="mbl" >{{ $student->student->studentId ?? '' }}</td>
                                        @endif

                                        @if(in_array("mobile",$reqField))
                                            <td id="mbl" >{{ $student->student->mobile ?? '' }}</td>
                                        @endif

                                        @if(in_array("address",$reqField))
                                            <td id="mbl" >{{ $student->student->address ?? '' }}</td>
                                        @endif

                                        @if(in_array("email",$reqField))
                                            <td id="mbl" >{{ $student->student->email ?? '' }}</td>
                                        @endif

                                        @if(in_array("area",$reqField))
                                            <td id="mbl" >{{ $student->student->area ?? '' }}</td>
                                        @endif

                                        @if(in_array("f_name",$reqField))
                                            <td id="mbl" >{{ $student->student->father->f_name ?? '' }}</td>
                                        @endif
                                        @if(in_array("m_name",$reqField) )
                                            <td id="mbl" >{{ $student->student->mother->m_name ?? '' }}</td>
                                        @endif
                                    </tr>
                                @empty
                                    <td colspan="8" class="text-center text-danger text-bold"> No Data Found! 😒</td>
                                @endforelse

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

@stop

@section('plugin')

@stop
@section('script')

@stop
