@extends('layouts.fixed')

@section('title', 'Reg|Roll|Gpa Setup')
@section('style')
    <style>
        body {
            --table-width: 100%; /* Or any value, this will change dinamically */
        }
        tbody {
            display:block;
            max-height:350px;
            overflow-y:auto;
        }
        thead, tbody tr {
            display:table;
            width: var(--table-width);
            table-layout:fixed;
        }
        .invalid{background:#FFD6D5}
    </style>
@stop

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Setup Roll & Reg') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{ __('Sttudent') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('Setup Reg') }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- /.Search-panel -->
    <section class="content no_print ">
        <div class="container-fluid">
            {{-- start --}}
            <div class="col-lg-12 col-sm-8 col-md-8 col-xs-12  ">
                <div class="card card-primary card-outline">
                    <div class="card-body" style="padding-bottom:0">
                        <form method="get" action="{{ route('reg.setup') }}">
                            <div class="form-row">

{{--                                <label class="mr-1 mt-1"> Academic Class </label>--}}
                                <div class="form-group col">
                                    <select name="ac_class_id" id="" class=" form-control select2 " required>
                                        <option value="">Select Academic Class</option>
                                        @foreach ($classes as $cls)
                                            <option value="{{ $cls->id }}" {{ request()->ac_class_id == $cls->id ? 'selected':''  }} >
                                                {{ $cls->classes->name ?? '' }} {{ $cls->section->name ?? '' }} {{ $cls->group->name ?? '' }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group col">
                                    <select name="exam_name" id="" class=" form-control" required>
                                        <option value="">Select Exam</option>
                                            <option value="hsc" {{ request()->exam_name == "hsc" ? 'selected':''  }}> HSC </option>
                                            <option value="ssc" {{ request()->exam_name == "ssc" ? 'selected':''  }}> SSC </option>
                                            <option value="jsc" {{ request()->exam_name == "jsc" ? 'selected':''  }}> JSC </option>
                                            <option value="pec" {{ request()->exam_name == "pec" ? 'selected':''  }}> PEC </option>
                                    </select>
                                </div>

                                <div class="form-group row col-md-2 ml-1 ">
                                    <button type="submit" class="btn btn-info btn-md "><i
                                                class="fa fa-search"></i>
                                    </button>&nbsp;
                                    <button class="btn btn-warning btn-md "
                                            onclick="window.print(); return false;"><i
                                                class="fa fa-print"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.card -->
            </div>

        </div>{{-- end --}}
    </section>

    @if (isset($students))
        @if( $students->count() > 0)
            <section class="content mt-4">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <form action="{{route("reg-setup.update")}}" method="get">
                        <div class="card">
                            <card class="card-header">
                                <button type="submit" id="subButton"  class="btn btn-warning btn-sm float-right "> Update </button>
{{--                                <button type="button"  id="changeMood" class="btn btn-danger btn-sm "> Edit Mood </button>--}}
                                <button type="button"  id="disableRoll" class="btn btn-danger btn-sm "> Change Roll </button>
                                <button type="button"  id="disableReg" class="btn btn-danger btn-sm "> Change Reg </button>
                                <button type="button"  id="disableGpa" class="btn btn-danger btn-sm "> Change Gpa </button>
                            </card>
                            <div class="card-body">


                                <table class="table table-striped table-sm">
                                    <thead>
                                    <tr>
                                        <th>Sl.</th>
                                        <th>StudentId</th>
                                        <th>Full Name</th>
                                        <th>Exam</th>
                                        <th>Roll</th>
                                        <th>Reg</th>
                                        <th>Gpa</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($students as $key=>$stu)
                                        @php
                                            $hsc =  json_decode( $stu->hsc);
                                            $ssc =  json_decode( $stu->ssc);
                                            $jsc =  json_decode( $stu->jsc);
                                            $pec =  json_decode( $stu->pec);
                                        @endphp

                                        <tr>
                                            <td> {{ $key+1 }}</td>
                                            <td>{{ $stu->student->studentId ?? ''}}</td>
                                            <td>{{ $stu->student->name ?? '' }}</td>
                                            <td> {{ request()->exam_name }}</td>

                                            <input type="hidden" name="id[]" value="{{ $stu->id }}">

                                            @if(request()->exam_name == 'hsc' )
                                                <td><input type="number" name="hsc[roll][]"  class="form-control rollNo " value="{{$hsc->roll ?? '' }}"> </td>
                                                <td><input type="number" name="hsc[reg][]" class="form-control  regNo" value="{{$hsc->reg ?? '' }}" ></td>
                                                <td><input type="number" step="any"  name="hsc[gpa][]"  class="form-control gpa" value="{{$hsc->gpa ?? '' }}"> </td>
                                            @endif

                                            @if(request()->exam_name == 'ssc' )
                                                <td> <input type="number" name="ssc[roll][]"  class="form-control  rollNo" value="{{$ssc->roll ?? '' }}"> </td>
                                                <td><input type="number"  name="ssc[reg][]"  class="form-control regNo " value="{{$ssc->reg ?? '' }}"> </td>
                                                <td><input type="number" step="any"  name="ssc[gpa][]"  class="form-control gpa" value="{{$ssc->gpa ?? '' }}"> </td>
                                            @endif
                                            @if(request()->exam_name == 'jsc' )
                                                <td><input type="text"  name="jsc[roll][]" class="form-control  rollNo" value="{{$jsc->roll ?? '' }}"> </td>
                                                <td><input type="text"  name="jsc[reg][]" class="form-control  regNo" value="{{$jsc->reg  ?? ''}}"> </td>
                                                <td><input type="number" step="any"  name="jsc[gpa][]"  class="form-control gpa" value="{{$jsc->gpa ?? '' }}"> </td>
                                            @endif
                                            @if(request()->exam_name == 'pec' )
                                                <td><input type="text"  name="pec[roll][]" class="form-control  rollNo" value="{{$pec->roll  ?? ''}}"> </td>
                                                <td><input type="text" disabled name="pec[reg][]" class="form-control  regNo" value="{{$pec->reg  ?? ''}}"> </td>
                                                <td><input type="number" step="any"  name="pec[gpa][]"  class="form-control gpa " value="{{$pec->gpa ?? '' }}"> </td>
                                            @endif

                                        </tr>

                                    @empty
                                        <h6> Sorry! No data found!</h6>
                                    @endforelse
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        </form>
                    </div>
                </div>

            </section>
        @else
{{--            <h5 class="text-bold text-danger text-center"> Sorry!! Somethig went wrong!</h5>--}}
        @endif
    @endif
@stop

@section('plugin')
@stop
@section('script')
    <script>
        $("input").prop('readonly',true);

        $(function () {
            // $("#changeMood").click(function () {
            //      $("input, #subButton").prop('readonly', (i, v) => !v);
            // });

            $("#disableRoll").click(function () {
                 $(".rollNo, #subButton").prop('readonly', (i, v) => !v);
            });

            $("#disableReg").click(function () {
                 $(".regNo, #subButton").prop('readonly', (i, v) => !v);
            });

            $("#disableGpa").click(function () {
                 $(".gpa , #subButton").prop('readonly', (i, v) => !v);
            });

            //change input color
            checkInput();

            $('input').on('input change',checkInput)

            function checkInput(){
                $('input').each(function(){
                    if($(this).val() == ''){
                        $(this).addClass('invalid')}else{
                        $(this).removeClass('invalid')}
                })
            }

            //gpa validation
            $(".gpa").keyup(function() {
                if ($(this).val() > 5) {
                    alert("To order quantity greater than 40 please use the contact form.");
                    $(this).val('');
                    $(this).focus();
                }
            });
        })

    </script>

@stop
