@extends('layouts.fixed')

@section('title', 'Token ')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Token Generate') }}</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{ __('Addmission') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('Token Generate') }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- /.Search-panel -->
    <section class="content no_print ">
        <div class="container-fluid">
            {{-- start --}}
            <div class="col-lg-12 col-sm-8 col col-xs-12">
                <div class="card card-primary card-outline">
                    <div class="card-body" >
                        <form method="get" action="{{ route('token.generate') }}">
                            <div class="form-row">

                                <label class="mr-1 mt-1"> Academic Class </label>
                                <div class="form-group col">
                                    <select name="ac_class_id" id="" class=" form-control select2" required>
                                        <option value="">Select Class</option>

                                        @foreach ($classes as $cls)
                                            <option value="{{ $cls->id }}">
                                                {{ $cls->classes->name ?? '' }} {{ $cls->section->name ?? '' }} {{ $cls->group->name ?? '' }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group row col ml-1 ">
                                    <button type="submit" class="btn btn-info btn-md "><i
                                                class="fa fa-check"></i>
                                    </button>&nbsp;
                                    <button class="btn btn-warning btn-md "
                                            onclick="window.print(); return false;"><i
                                                class="fa fa-print"></i>
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
                <!-- /.card -->
            </div>

        </div>{{-- end --}}
    </section>

    @if (isset($students))
        @if( $students->count() > 0)
            <section class="content mt-4">
                <div class="container-fluid">
                    <div class="col-md-12">
                        <div class="card" >
                            <div class="card-body">
                                <div class="text-center">
                                </div>
                                <table class="table " >
                                    <tbody>
                                    @foreach($students->chunk(3) as  $data)
                                        <div class="row">
                                            @foreach($data as $stu)
                                            <div class="col-md-4 mb-3">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <h5 class="card-title">{{ $stu->student->name ?? '' }}</h5>
                                                       StudentId: <span class="card-text">{{  $stu->student->studentId ?? ''}}</span>  ,
                                                      Class:  <span class="card-text">{{ $students[0]->academicClass->academicClasses->name  ?? '' }}
                                                            {{ $students[0]->academicClass->section->name  ?? '' }}
                                                            {{ $students[0]->academicClass->group->name  ?? '' }}
                                                        </span> <br>
                                                        Mobile:<span class="card-text">{{  $stu->student->mobile ?? ''}}</span><br>
                                                        Address:<span class="card-text">{{  $stu->student->address ?? '' }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        @else
            <h5 class="text-bold text-danger text-center"> Sorry!! Somethig went wrong!</h5>
        @endif
    @endif

@stop

@section('plugin')
@stop
@section('script')

@stop
