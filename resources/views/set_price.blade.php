@extends('layouts.fixed')

@section('title','Attendance | Monthly Report')

@section('style')

@stop

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>{{ __('Pricing') }} </h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">{{ __('Home') }}</a></li>
                        <li class="breadcrumb-item active">{{ __('Attendance Monthly Report') }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="card card-default">
                <div class="card-body">
                    <form method="get" action="{{ route('get.price') }}">
                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Date</label>
                                <input type="date" name="date" class="form-control" id="inputEmail4" placeholder="Email">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Greeting</label>
                                <input type="text" name="greeting" class="form-control" id="inputEmail4" placeholder="Email">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="inputEmail4">Mobile</label>
                                <input type="text" name="mobile" class="form-control" id="inputEmail4" placeholder="Email">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputEmail4"></label>
                                <input type="text" name="basic" class="form-control" id="inputEmail4" placeholder="Email">
                            </div>
                        <button type="submit" class="btn btn-primary">OK</button>
                    </form>
                </div>
            </div>
        </div>
    </section>


@stop


